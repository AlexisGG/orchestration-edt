-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : mysqldb:3306
-- Généré le : mar. 19 oct. 2021 à 17:39
-- Version du serveur : 8.0.26
-- Version de PHP : 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `orchestration_db`
--

-- --------------------------------------------------------

--
-- Structure de la table `class`
--

CREATE TABLE `class` (
  `id` int NOT NULL,
  `name` varchar(256) DEFAULT NULL,
  `element_id` int DEFAULT NULL,
  `part_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `class`
--

INSERT INTO `class` (`id`, `name`, `element_id`, `part_id`) VALUES
(1, 'Base de données 2-CM1', 25, 1),
(2, 'Base de données 2-TD1', 26, 2),
(3, 'Base de données 2-TD2', 27, 2),
(4, 'Base de données 2-TP1', 28, 3),
(5, 'Base de données 2-TP2', 29, 3),
(6, 'Base de données 2-TP3', 30, 3),
(7, 'Base de données 2-TPN1', 31, 4),
(8, 'Base de données 2-TPN2', 32, 4),
(9, 'Base de données 2-TPN3', 33, 4),
(10, 'Programmation Fonctionnelle-CM1', 34, 9),
(11, 'Programmation Fonctionnelle-TD1', 35, 10),
(12, 'Programmation Fonctionnelle-TD2', 36, 10),
(13, 'Programmation Fonctionnelle-TP1', 37, 11),
(14, 'Programmation Fonctionnelle-TP2', 38, 11),
(15, 'Programmation Fonctionnelle-TP3', 39, 11),
(16, 'Programmation Fonctionnelle-PR1', 40, 12),
(17, 'Développement-Web-CM1', 41, 16),
(18, 'Développement-Web-TP1', 42, 17),
(19, 'Développement-Web-TP2', 43, 17),
(20, 'Développement-Web-TP3', 44, 17),
(21, 'Développement-Web-PR1', 45, 18);

-- --------------------------------------------------------

--
-- Structure de la table `class_parent`
--

CREATE TABLE `class_parent` (
  `class_id` int NOT NULL,
  `parent_class_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `course`
--

CREATE TABLE `course` (
  `id` int NOT NULL,
  `name` varchar(256) DEFAULT NULL,
  `element_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `course`
--

INSERT INTO `course` (`id`, `name`, `element_id`) VALUES
(1, 'Base de données 2', 12),
(2, 'Programmation Logique et Fonctionnelle', 13),
(3, 'OPTIONS', 14),
(4, 'Développement-Web', 15),
(5, 'CT', 16);

-- --------------------------------------------------------

--
-- Structure de la table `courseXcourse_bundle`
--

CREATE TABLE `courseXcourse_bundle` (
  `course_id` int NOT NULL,
  `course_bundle_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `courseXcourse_bundle`
--

INSERT INTO `courseXcourse_bundle` (`course_id`, `course_bundle_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1);

-- --------------------------------------------------------

--
-- Structure de la table `course_bundle`
--

CREATE TABLE `course_bundle` (
  `id` int NOT NULL,
  `tag` varchar(256) DEFAULT NULL,
  `nr_weeks` int DEFAULT NULL,
  `days_per_week` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `course_bundle`
--

INSERT INTO `course_bundle` (`id`, `tag`, `nr_weeks`, `days_per_week`) VALUES
(1, 'L3-INFORMATIQUE', 12, 5);

-- --------------------------------------------------------

--
-- Structure de la table `course_bundleXcourse_bundle`
--

CREATE TABLE `course_bundleXcourse_bundle` (
  `course_bundle_id` int NOT NULL,
  `course_bundle_id_parent` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `element`
--

CREATE TABLE `element` (
  `id` int NOT NULL,
  `element_type` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `element`
--

INSERT INTO `element` (`id`, `element_type`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 2),
(13, 2),
(14, 2),
(15, 2),
(16, 2),
(17, 3),
(18, 3),
(19, 3),
(20, 3),
(21, 3),
(22, 3),
(23, 3),
(24, 3),
(25, 4),
(26, 4),
(27, 4),
(28, 4),
(29, 4),
(30, 4),
(31, 4),
(32, 4),
(33, 4),
(34, 4),
(35, 4),
(36, 4),
(37, 4),
(38, 4),
(39, 4),
(40, 4),
(41, 4),
(42, 4),
(43, 4),
(44, 4),
(45, 4),
(46, 5),
(47, 5),
(48, 5),
(49, 5),
(50, 6),
(51, 6),
(52, 6),
(53, 6),
(54, 6),
(55, 6),
(56, 6),
(57, 6),
(58, 6),
(59, 6),
(60, 6);

-- --------------------------------------------------------

--
-- Structure de la table `elementXlabel`
--

CREATE TABLE `elementXlabel` (
  `element_id` int NOT NULL,
  `label_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `element_type`
--

CREATE TABLE `element_type` (
  `id` int NOT NULL,
  `name` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `element_type`
--

INSERT INTO `element_type` (`id`, `name`) VALUES
(1, 'PART'),
(2, 'COURSE'),
(3, 'ROOM'),
(4, 'CLASS'),
(5, 'GROUP'),
(6, 'PERSON'),
(7, 'EQUIPMENT');

-- --------------------------------------------------------

--
-- Structure de la table `equipment`
--

CREATE TABLE `equipment` (
  `id` int NOT NULL,
  `name` varchar(256) DEFAULT NULL,
  `element_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `filter`
--

CREATE TABLE `filter` (
  `id` int NOT NULL,
  `scope_id` int DEFAULT NULL,
  `including` tinyint DEFAULT '1',
  `label` tinyint DEFAULT '0',
  `element_type_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `filter_element`
--

CREATE TABLE `filter_element` (
  `element_id` int NOT NULL,
  `filter_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `filter_label`
--

CREATE TABLE `filter_label` (
  `filter_id` int NOT NULL,
  `label_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `formation`
--

CREATE TABLE `formation` (
  `course_bundle_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `formation`
--

INSERT INTO `formation` (`course_bundle_id`) VALUES
(1);

-- --------------------------------------------------------

--
-- Structure de la table `group`
--

CREATE TABLE `group` (
  `id` int NOT NULL,
  `name` varchar(256) DEFAULT NULL,
  `element_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `group`
--

INSERT INTO `group` (`id`, `name`, `element_id`) VALUES
(1, '1-td1-tp1-pq', 46),
(2, '2-td1-tp3-ps', 47),
(3, '3-td2-tp2-is', 48),
(4, '4-td2-tp2-qs', 49);

-- --------------------------------------------------------

--
-- Structure de la table `groupXclass`
--

CREATE TABLE `groupXclass` (
  `class_id` int NOT NULL,
  `group_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `groupXclass`
--

INSERT INTO `groupXclass` (`class_id`, `group_id`) VALUES
(1, 1),
(2, 1),
(4, 1),
(7, 1),
(10, 1),
(11, 1),
(13, 1),
(16, 1),
(17, 1),
(18, 1),
(21, 1),
(1, 2),
(2, 2),
(6, 2),
(9, 2),
(10, 2),
(11, 2),
(15, 2),
(16, 2),
(17, 2),
(20, 2),
(21, 2),
(1, 3),
(3, 3),
(5, 3),
(8, 3),
(10, 3),
(12, 3),
(14, 3),
(16, 3),
(17, 3),
(19, 3),
(21, 3),
(1, 4),
(3, 4),
(5, 4),
(8, 4),
(10, 4),
(12, 4),
(14, 4),
(16, 4),
(17, 4),
(19, 4),
(21, 4);

-- --------------------------------------------------------

--
-- Structure de la table `group_registration`
--

CREATE TABLE `group_registration` (
  `group_id` int NOT NULL,
  `part_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `label`
--

CREATE TABLE `label` (
  `id` int NOT NULL,
  `name` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `part`
--

CREATE TABLE `part` (
  `id` int NOT NULL,
  `nr_sessions` int DEFAULT NULL,
  `nr_teachers` int DEFAULT NULL,
  `max_headcount` int DEFAULT NULL,
  `session_length` int DEFAULT NULL,
  `single_room` tinyint DEFAULT NULL,
  `starting_slots` text,
  `course_id` int DEFAULT NULL,
  `element_id` int DEFAULT NULL,
  `part_type_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `part`
--

INSERT INTO `part` (`id`, `nr_sessions`, `nr_teachers`, `max_headcount`, `session_length`, `single_room`, `starting_slots`, `course_id`, `element_id`, `part_type_id`) VALUES
(1, 8, 1, NULL, 1, 1, NULL, 1, 1, 1),
(2, 9, 1, NULL, 1, 1, NULL, 1, 2, 3),
(3, 7, 1, NULL, 2, 1, NULL, 1, 3, 2),
(4, 1, 2, NULL, 2, 0, NULL, 1, 4, 5),
(9, 7, 1, NULL, 1, 1, NULL, 2, 5, 1),
(10, 4, 1, NULL, 1, 1, NULL, 2, 6, 3),
(11, 4, 1, NULL, 2, 1, NULL, 2, 7, 2),
(12, 8, NULL, NULL, 1, 1, NULL, 2, 8, 6),
(16, 12, 1, NULL, 1, 1, NULL, 4, 9, 1),
(17, 8, 1, NULL, 2, 1, NULL, 4, 10, 2),
(18, 8, NULL, NULL, 3, 0, NULL, 4, 11, 6);

-- --------------------------------------------------------

--
-- Structure de la table `part_type`
--

CREATE TABLE `part_type` (
  `id` int NOT NULL,
  `name` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `part_type`
--

INSERT INTO `part_type` (`id`, `name`) VALUES
(1, 'CM'),
(2, 'TP'),
(3, 'TD'),
(4, 'CT'),
(5, 'TPN'),
(6, 'PR');

-- --------------------------------------------------------

--
-- Structure de la table `person`
--

CREATE TABLE `person` (
  `id` int NOT NULL,
  `teacher` tinyint DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `element_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `person`
--

INSERT INTO `person` (`id`, `teacher`, `name`, `element_id`) VALUES
(1, 1, 'AIT EL MEKKI Touria', 50),
(2, 1, 'GARCIA Laurent', 51),
(3, 1, 'RICHER Jean-Michel', 52),
(4, 1, 'LEGEAY Marc', 53),
(5, 1, 'LEFEVRE Claire', 54),
(6, 1, 'DIEGUEZ Martin', 55),
(7, 1, 'STEPHAN Igor', 56),
(8, 1, 'DEVRED Caroline', 57),
(9, 1, 'GOUDET Olivier', 58),
(10, 1, 'LESAINT David', 59),
(11, 1, 'JAMIN Antoine', 60);

-- --------------------------------------------------------

--
-- Structure de la table `room`
--

CREATE TABLE `room` (
  `id` int NOT NULL,
  `name` varchar(256) DEFAULT NULL,
  `capacity` int DEFAULT NULL,
  `element_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `room`
--

INSERT INTO `room` (`id`, `name`, `capacity`, `element_id`) VALUES
(1, 'AMPHI-A', 90, 17),
(2, 'AMPHI-B', 90, 18),
(3, 'H001', 20, 19),
(4, 'H002', 42, 20),
(5, 'H003', 38, 21),
(6, 'L201', 46, 22),
(7, 'L202', 46, 23),
(8, 'L206', 28, 24);

-- --------------------------------------------------------

--
-- Structure de la table `roomXsession`
--

CREATE TABLE `roomXsession` (
  `session` int DEFAULT NULL,
  `class_id` int NOT NULL,
  `room_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `room_allocation`
--

CREATE TABLE `room_allocation` (
  `mandatory` tinyint DEFAULT NULL,
  `part_id` int NOT NULL,
  `room_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `room_allocation`
--

INSERT INTO `room_allocation` (`mandatory`, `part_id`, `room_id`) VALUES
(0, 1, 1),
(0, 1, 2),
(0, 2, 6),
(0, 2, 7),
(0, 2, 8),
(0, 3, 3),
(0, 3, 4),
(0, 3, 5),
(0, 4, 3),
(1, 4, 4),
(0, 4, 5),
(0, 9, 1),
(0, 9, 2),
(0, 10, 6),
(0, 10, 7),
(0, 10, 8),
(0, 11, 3),
(0, 11, 4),
(0, 11, 5),
(0, 12, 3),
(0, 12, 4),
(0, 12, 5),
(0, 16, 1),
(0, 16, 2),
(0, 17, 3),
(0, 17, 4),
(0, 17, 5),
(1, 18, 3),
(1, 18, 4),
(0, 18, 5);

-- --------------------------------------------------------

--
-- Structure de la table `rule`
--

CREATE TABLE `rule` (
  `id` int NOT NULL,
  `constraint_name` varchar(256) DEFAULT NULL,
  `constraint_type` text,
  `trigger_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `rule_parameter`
--

CREATE TABLE `rule_parameter` (
  `id` int NOT NULL,
  `name` varchar(256) DEFAULT NULL,
  `type` text,
  `value` text,
  `rule_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `scope`
--

CREATE TABLE `scope` (
  `id` int NOT NULL,
  `mask` varchar(256) DEFAULT NULL,
  `scope_order` int DEFAULT NULL,
  `group_by` int DEFAULT NULL,
  `rule_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `service`
--

CREATE TABLE `service` (
  `nr_sessions` int DEFAULT NULL,
  `part_id` int NOT NULL,
  `teacher_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `service`
--

INSERT INTO `service` (`nr_sessions`, `part_id`, `teacher_id`) VALUES
(8, 1, 1),
(18, 2, 1),
(14, 3, 1),
(7, 3, 2),
(1, 4, 1),
(1, 4, 2),
(1, 4, 3),
(1, 4, 4),
(7, 9, 5),
(14, 10, 5),
(4, 11, 4),
(4, 11, 5),
(4, 11, 6),
(12, 16, 10),
(12, 17, 10),
(12, 17, 11);

-- --------------------------------------------------------

--
-- Structure de la table `slotXsession`
--

CREATE TABLE `slotXsession` (
  `session` int DEFAULT NULL,
  `slot` int DEFAULT NULL,
  `class_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `teacherXsession`
--

CREATE TABLE `teacherXsession` (
  `session` int DEFAULT NULL,
  `class_id` int NOT NULL,
  `teacher_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `trigger`
--

CREATE TABLE `trigger` (
  `id` int NOT NULL,
  `text` text NOT NULL,
  `course_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `userRights`
--

CREATE TABLE `userRights` (
  `courseId` int NOT NULL,
  `userId` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `userRights`
--

INSERT INTO `userRights` (`courseId`, `userId`) VALUES
(1, 2),
(1, 3),
(2, 5),
(2, 7),
(4, 10),
(4, 11);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(64) NOT NULL,
  `isAdmin` tinyint(1) DEFAULT '0',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `isAdmin`, `createdAt`, `updatedAt`) VALUES
(1, 'admin', '$2b$10$EKMb1Fw5ltAg9VjJs5zSguxoAiewoqy4eXi9uAdWKDGOoMTmCZNqi', 1, '2021-10-09 20:48:41', '2021-10-09 20:48:41'),
(2, 'l.garcia', '$2b$10$0ww1YwiNuDl3FmPWhyvGY./Z7gv.1VBkRuuelXSvpxbOXsrLbPV0u', 0, '2021-10-09 20:53:01', '2021-10-09 20:53:01'),
(3, 'j.richer', '$2b$10$ZcPGOebs6OmJ.tNkkEwLk.zq7FXTJijx8K9FMlqyJ1WkUnYggEOi6', 0, '2021-10-09 20:53:20', '2021-10-09 20:53:20'),
(4, 'm.legeay', '$2b$10$HurNnkVeVs3mO7IMLjdyoewNiVL/zidDX2rslBQ5ItpO5BlUaFhSy', 0, '2021-10-09 20:53:29', '2021-10-09 20:53:29'),
(5, 'c.lefevre', '$2b$10$NX/ECUKBdUNhLVa7cpw7UepU6aUuaLt33wJyinQqyooDFQ9oMZMiS', 0, '2021-10-09 20:53:49', '2021-10-09 20:53:49'),
(6, 'm.dieguez', '$2b$10$9sGxcLGSTdd/2wscTyLTRO5JVZdQzPI9RAHRPQBpCkslXR2//S09C', 0, '2021-10-09 20:54:02', '2021-10-09 20:54:02'),
(7, 'i.stephan', '$2b$10$eucec.WQN.eiO.J1qJGOWuiYDy040LgNVW4MhSKeiv72LtQuofvH6', 0, '2021-10-09 20:54:15', '2021-10-09 20:54:15'),
(8, 'c.devred', '$2b$10$I5hz4OtV1xuVmol0fBTi1eNZWefP5agco.HoFnMYc..Vb1O16g54K', 0, '2021-10-09 20:54:23', '2021-10-09 20:54:23'),
(9, 'o.goudet', '$2b$10$DKRRZAIukIsAT5hTuaCTD.GxavTwAW2sjTsL927/CFNy6gi6vzbY6', 0, '2021-10-09 20:54:31', '2021-10-09 20:54:31'),
(10, 'd.lesaint', '$2b$10$3plPVTS.VxGuTVGo8xEn.eyR4vkWrISHLRRjug9f8pNSzZVmO3PKW', 0, '2021-10-09 20:54:42', '2021-10-09 20:54:42'),
(11, 'a.jamin', '$2b$10$p9xtTfTkraCv5iaD3drMWOYKufnP4E84zfLYMeOeXrPnTWUre9Omm', 0, '2021-10-09 20:54:53', '2021-10-09 20:54:53');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`id`),
  ADD KEY `element_id` (`element_id`),
  ADD KEY `part_id` (`part_id`);

--
-- Index pour la table `class_parent`
--
ALTER TABLE `class_parent`
  ADD PRIMARY KEY (`class_id`,`parent_class_id`),
  ADD KEY `parent_class_id` (`parent_class_id`);

--
-- Index pour la table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`),
  ADD KEY `element_id` (`element_id`);

--
-- Index pour la table `courseXcourse_bundle`
--
ALTER TABLE `courseXcourse_bundle`
  ADD PRIMARY KEY (`course_id`,`course_bundle_id`),
  ADD KEY `course_bundle_id` (`course_bundle_id`);

--
-- Index pour la table `course_bundle`
--
ALTER TABLE `course_bundle`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `course_bundleXcourse_bundle`
--
ALTER TABLE `course_bundleXcourse_bundle`
  ADD PRIMARY KEY (`course_bundle_id`,`course_bundle_id_parent`),
  ADD KEY `course_bundle_id_parent` (`course_bundle_id_parent`);

--
-- Index pour la table `element`
--
ALTER TABLE `element`
  ADD PRIMARY KEY (`id`),
  ADD KEY `element_type` (`element_type`);

--
-- Index pour la table `elementXlabel`
--
ALTER TABLE `elementXlabel`
  ADD PRIMARY KEY (`element_id`,`label_id`),
  ADD KEY `label_id` (`label_id`);

--
-- Index pour la table `element_type`
--
ALTER TABLE `element_type`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `equipment`
--
ALTER TABLE `equipment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `element_id` (`element_id`);

--
-- Index pour la table `filter`
--
ALTER TABLE `filter`
  ADD PRIMARY KEY (`id`),
  ADD KEY `element_type_id` (`element_type_id`);

--
-- Index pour la table `filter_element`
--
ALTER TABLE `filter_element`
  ADD PRIMARY KEY (`element_id`,`filter_id`),
  ADD KEY `filter_id` (`filter_id`);

--
-- Index pour la table `filter_label`
--
ALTER TABLE `filter_label`
  ADD PRIMARY KEY (`filter_id`,`label_id`),
  ADD KEY `label_id` (`label_id`);

--
-- Index pour la table `formation`
--
ALTER TABLE `formation`
  ADD KEY `course_bundle_id` (`course_bundle_id`);

--
-- Index pour la table `group`
--
ALTER TABLE `group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `element_id` (`element_id`);

--
-- Index pour la table `groupXclass`
--
ALTER TABLE `groupXclass`
  ADD PRIMARY KEY (`class_id`,`group_id`),
  ADD KEY `group_id` (`group_id`);

--
-- Index pour la table `group_registration`
--
ALTER TABLE `group_registration`
  ADD PRIMARY KEY (`group_id`,`part_id`),
  ADD KEY `part_id` (`part_id`);

--
-- Index pour la table `label`
--
ALTER TABLE `label`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `part`
--
ALTER TABLE `part`
  ADD PRIMARY KEY (`id`),
  ADD KEY `course_id` (`course_id`),
  ADD KEY `element_id` (`element_id`),
  ADD KEY `part_type_id` (`part_type_id`);

--
-- Index pour la table `part_type`
--
ALTER TABLE `part_type`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`id`),
  ADD KEY `element_id` (`element_id`);

--
-- Index pour la table `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`id`),
  ADD KEY `element_id` (`element_id`);

--
-- Index pour la table `roomXsession`
--
ALTER TABLE `roomXsession`
  ADD PRIMARY KEY (`class_id`,`room_id`),
  ADD KEY `room_id` (`room_id`);

--
-- Index pour la table `room_allocation`
--
ALTER TABLE `room_allocation`
  ADD PRIMARY KEY (`part_id`,`room_id`),
  ADD KEY `room_id` (`room_id`);

--
-- Index pour la table `rule`
--
ALTER TABLE `rule`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rule_trigger_id_foreign_idx` (`trigger_id`);

--
-- Index pour la table `rule_parameter`
--
ALTER TABLE `rule_parameter`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rule_id` (`rule_id`);

--
-- Index pour la table `scope`
--
ALTER TABLE `scope`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_by` (`group_by`),
  ADD KEY `rule_id` (`rule_id`);

--
-- Index pour la table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`part_id`,`teacher_id`),
  ADD KEY `teacher_id` (`teacher_id`);

--
-- Index pour la table `slotXsession`
--
ALTER TABLE `slotXsession`
  ADD KEY `class_id` (`class_id`);

--
-- Index pour la table `teacherXsession`
--
ALTER TABLE `teacherXsession`
  ADD PRIMARY KEY (`class_id`,`teacher_id`),
  ADD KEY `teacher_id` (`teacher_id`);

--
-- Index pour la table `trigger`
--
ALTER TABLE `trigger`
  ADD PRIMARY KEY (`id`),
  ADD KEY `course_id` (`course_id`);

--
-- Index pour la table `userRights`
--
ALTER TABLE `userRights`
  ADD PRIMARY KEY (`courseId`,`userId`),
  ADD KEY `userId` (`userId`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `username_2` (`username`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `class`
--
ALTER TABLE `class`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT pour la table `course`
--
ALTER TABLE `course`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `course_bundle`
--
ALTER TABLE `course_bundle`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `element`
--
ALTER TABLE `element`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT pour la table `element_type`
--
ALTER TABLE `element_type`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `equipment`
--
ALTER TABLE `equipment`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `filter`
--
ALTER TABLE `filter`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `group`
--
ALTER TABLE `group`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `label`
--
ALTER TABLE `label`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `part`
--
ALTER TABLE `part`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT pour la table `part_type`
--
ALTER TABLE `part_type`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `person`
--
ALTER TABLE `person`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `room`
--
ALTER TABLE `room`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `rule`
--
ALTER TABLE `rule`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `rule_parameter`
--
ALTER TABLE `rule_parameter`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `scope`
--
ALTER TABLE `scope`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `trigger`
--
ALTER TABLE `trigger`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `class`
--
ALTER TABLE `class`
  ADD CONSTRAINT `class_ibfk_3` FOREIGN KEY (`element_id`) REFERENCES `element` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `class_ibfk_4` FOREIGN KEY (`part_id`) REFERENCES `part` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `class_parent`
--
ALTER TABLE `class_parent`
  ADD CONSTRAINT `class_parent_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `class` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `class_parent_ibfk_2` FOREIGN KEY (`parent_class_id`) REFERENCES `class` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `course`
--
ALTER TABLE `course`
  ADD CONSTRAINT `course_ibfk_1` FOREIGN KEY (`element_id`) REFERENCES `element` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `courseXcourse_bundle`
--
ALTER TABLE `courseXcourse_bundle`
  ADD CONSTRAINT `courseXcourse_bundle_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `courseXcourse_bundle_ibfk_2` FOREIGN KEY (`course_bundle_id`) REFERENCES `course_bundle` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `course_bundleXcourse_bundle`
--
ALTER TABLE `course_bundleXcourse_bundle`
  ADD CONSTRAINT `course_bundleXcourse_bundle_ibfk_1` FOREIGN KEY (`course_bundle_id`) REFERENCES `course_bundle` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `course_bundleXcourse_bundle_ibfk_2` FOREIGN KEY (`course_bundle_id_parent`) REFERENCES `course_bundle` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `element`
--
ALTER TABLE `element`
  ADD CONSTRAINT `element_ibfk_1` FOREIGN KEY (`element_type`) REFERENCES `element_type` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `elementXlabel`
--
ALTER TABLE `elementXlabel`
  ADD CONSTRAINT `elementXlabel_ibfk_1` FOREIGN KEY (`element_id`) REFERENCES `element` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `elementXlabel_ibfk_2` FOREIGN KEY (`label_id`) REFERENCES `label` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `equipment`
--
ALTER TABLE `equipment`
  ADD CONSTRAINT `equipment_ibfk_1` FOREIGN KEY (`element_id`) REFERENCES `element` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `filter`
--
ALTER TABLE `filter`
  ADD CONSTRAINT `filter_ibfk_1` FOREIGN KEY (`element_type_id`) REFERENCES `element_type` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `filter_element`
--
ALTER TABLE `filter_element`
  ADD CONSTRAINT `filter_element_ibfk_1` FOREIGN KEY (`element_id`) REFERENCES `element` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `filter_element_ibfk_2` FOREIGN KEY (`filter_id`) REFERENCES `filter` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `filter_label`
--
ALTER TABLE `filter_label`
  ADD CONSTRAINT `filter_label_ibfk_1` FOREIGN KEY (`filter_id`) REFERENCES `filter` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `filter_label_ibfk_2` FOREIGN KEY (`label_id`) REFERENCES `label` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `formation`
--
ALTER TABLE `formation`
  ADD CONSTRAINT `formation_ibfk_1` FOREIGN KEY (`course_bundle_id`) REFERENCES `course_bundle` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `group`
--
ALTER TABLE `group`
  ADD CONSTRAINT `group_ibfk_1` FOREIGN KEY (`element_id`) REFERENCES `element` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `groupXclass`
--
ALTER TABLE `groupXclass`
  ADD CONSTRAINT `groupXclass_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `class` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `groupXclass_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `group_registration`
--
ALTER TABLE `group_registration`
  ADD CONSTRAINT `group_registration_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `group_registration_ibfk_2` FOREIGN KEY (`part_id`) REFERENCES `part` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `part`
--
ALTER TABLE `part`
  ADD CONSTRAINT `part_ibfk_7` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `part_ibfk_8` FOREIGN KEY (`element_id`) REFERENCES `element` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `part_ibfk_9` FOREIGN KEY (`part_type_id`) REFERENCES `part_type` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `person`
--
ALTER TABLE `person`
  ADD CONSTRAINT `person_ibfk_1` FOREIGN KEY (`element_id`) REFERENCES `element` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `room`
--
ALTER TABLE `room`
  ADD CONSTRAINT `room_ibfk_1` FOREIGN KEY (`element_id`) REFERENCES `element` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `roomXsession`
--
ALTER TABLE `roomXsession`
  ADD CONSTRAINT `roomXsession_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `class` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `roomXsession_ibfk_2` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `room_allocation`
--
ALTER TABLE `room_allocation`
  ADD CONSTRAINT `room_allocation_ibfk_1` FOREIGN KEY (`part_id`) REFERENCES `part` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `room_allocation_ibfk_2` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `rule`
--
ALTER TABLE `rule`
  ADD CONSTRAINT `rule_trigger_id_foreign_idx` FOREIGN KEY (`trigger_id`) REFERENCES `trigger` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `rule_parameter`
--
ALTER TABLE `rule_parameter`
  ADD CONSTRAINT `rule_parameter_ibfk_1` FOREIGN KEY (`rule_id`) REFERENCES `rule` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `scope`
--
ALTER TABLE `scope`
  ADD CONSTRAINT `scope_ibfk_5` FOREIGN KEY (`group_by`) REFERENCES `element_type` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `scope_ibfk_6` FOREIGN KEY (`rule_id`) REFERENCES `rule` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `service`
--
ALTER TABLE `service`
  ADD CONSTRAINT `service_ibfk_1` FOREIGN KEY (`part_id`) REFERENCES `part` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `service_ibfk_2` FOREIGN KEY (`teacher_id`) REFERENCES `person` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `slotXsession`
--
ALTER TABLE `slotXsession`
  ADD CONSTRAINT `slotXsession_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `class` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `teacherXsession`
--
ALTER TABLE `teacherXsession`
  ADD CONSTRAINT `teacherXsession_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `class` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `teacherXsession_ibfk_2` FOREIGN KEY (`teacher_id`) REFERENCES `person` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `trigger`
--
ALTER TABLE `trigger`
  ADD CONSTRAINT `trigger_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `userRights`
--
ALTER TABLE `userRights`
  ADD CONSTRAINT `userRights_ibfk_1` FOREIGN KEY (`courseId`) REFERENCES `course` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `userRights_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
