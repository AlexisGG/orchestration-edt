// Import the dependencies for testing
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../../index');

// Configure chai
chai.use(chaiHttp);
chai.should();

// Import model
const { user: UserModel , course : CourseModel , waitForSync, sequelize } = require('../../../db/sequelize');

describe('RIGHTS - DELETE /rights/{user_id}/{course_id}', function() {

	let user,admin;
	let userT,adminT;

	before( function(done) {
		// Mise en attente pour préparation de la BDD
		waitForSync.then( function () { done() } );
	});
	
	before( async function() {
		// Création d'utilisateurs globaux
		user = await UserModel.create({ username: "alpha", password: "alpha", id:1, isAdmin: false});
		admin = await UserModel.create({ username: "beta", password: "beta", id:2, isAdmin: true});
		userT = user.generateToken();
		adminT = admin.generateToken();
	});

    after(async function() {
        // Suppression des données de test
        await sequelize.sync({ force: true });
    });

	it('should return 401 (not connected)', async function() {		
        const res = await chai.request(app)
        	.delete('/rights/999/999')
			.send({});

		res.should.have.status(401);
	});

	it('should return 403 (connected but not as admin)', async function() {

		const res = await chai.request(app)
        	.delete('/rights/999/999')
        	.set({ "Authorization": `Bearer ${userT}` });

		res.should.have.status(403);
	});

	it('should return 404 (user does not exist)', async function() {

		const res = await chai.request(app)
        	.delete('/rights/999/999')
        	.set({ "Authorization": `Bearer ${adminT}` });

		res.should.have.status(404);
		res.body.field.should.equal('user');
	});

	it('should return 404 (course does not exist)', async function() {

		const res = await chai.request(app)
        	.delete('/rights/1/999')
        	.set({ "Authorization": `Bearer ${adminT}` });

		res.should.have.status(404);
		res.body.field.should.equal('course');
	});

	it('should return 409 (both course and user exist but not already linked)', async function() {
		const course = await CourseModel.create({ id:1 });

		const res = await chai.request(app)
        	.delete('/rights/1/1')
        	.set({ "Authorization": `Bearer ${adminT}` });

		res.should.have.status(409);
	});

	it('should return 200 and delete link ', async function() {
		const course = await CourseModel.create({ id:7 });

		await admin.addCourse(course);

		const res = await chai.request(app)
			.delete('/rights/2/7')
			.set({ "Authorization": `Bearer ${adminT}`});
		
		res.should.have.status(200);

		// DataBase check:
		chai.assert.equal( await admin.hasCourse(course) , false , 'User should no longer be link to the course');
	})

	it('should return 404 (missing parameters)', async function() {	

		const res = await chai.request(app)
			.delete('/rights/')
			.set({ "Authorization": `Bearer ${adminT}`});
		
		res.should.have.status(404);
	})

	it('should return 404 (missing parameters)', async function() {

		const res = await chai.request(app)
			.delete('/rights/42')
			.set({ "Authorization": `Bearer ${adminT}`});
		
		res.should.have.status(404);

	})	
});