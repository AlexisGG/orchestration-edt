// Import the dependencies for testing
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../../index');

// Configure chai
chai.use(chaiHttp);
chai.should();
chai.use(require('chai-things'));

// Import model
const { user: UserModel , course : CourseModel , userRights: UserRightsModel, waitForSync, sequelize } = require('../../../db/sequelize');

describe('RIGHTS - POST /rights', function() {

    // Utlisateurs
    var garcia, legeay, goudet, admin;
    // Course
    var bdd, reseau, prog;


    before(async function(){
        await waitForSync
		
        // Création des utilisateurs
        admin = await UserModel.create({ username: "admin", password: "password", isAdmin: true});
        garcia = await UserModel.create({ username: "garcia", password: "password"});
        legeay = await UserModel.create({ username: "legeay", password: "password" });
        goudet = await UserModel.create({ username:"goudet", password: "password"});


        // Création des course
        bdd = await CourseModel.create({name:"bdd" });
        reseau = await CourseModel.create({ name:"reseau" });
        prog = await CourseModel.create({ name:"prog" });

        //lier un utilisateur à un cours

        await garcia.addCourse(reseau);
    })

    after(async function() {
        // Suppression des données de test
        await sequelize.sync({ force: true });
    });

    it('should return status code 401 (not being connected)', async function() {
		
        const res = await chai
			.request(app)
            .post('/rights')
        	.send({});
            res.should.have.status(401);
    })

    it('should return status code 403 (if not connected but not admin)', async function() {
		const token =  garcia.generateToken();
        const res = await chai
			.request(app)
            .post('/rights')
        	.send({'user_id':1,'course_id':1})
            .set({ "Authorization": `Bearer ${token}`});
            res.should.have.status(403);
    })
    it('should return status code 404 (if user_id not found)', async function() {
		const token =  admin.generateToken();
        const res = await chai
			.request(app)
            .post('/rights')
        	.send({'user_id':12,'course_id':1})
            .set({ "Authorization": `Bearer ${token}`});
            res.should.have.status(404);
    })
    it('should return status code 406 (if user_id is empty)', async function() {
		const token =  admin.generateToken();
        const res = await chai
			.request(app)
            .post('/rights')
        	.send({'course_id':1})
            .set({ "Authorization": `Bearer ${token}`});
        res.should.have.status(406);
        res.body.field.should.equal('user_id');  
          
    })

    it('should return status code 406 (if course_id is empty)', async function() {
		const token =  admin.generateToken();
        const res = await chai
			.request(app)
            .post('/rights')
        	.send({'user_id':1})
            .set({ "Authorization": `Bearer ${token}`});
        res.should.have.status(406);
        res.body.field.should.equal('course_id');  
    })
    it('should return status code 404 (if course_id not found)', async function() {
		const token =  admin.generateToken();
        const res = await chai
			.request(app)
            .post('/rights')
        	.send({'user_id':2,'course_id':23})
            .set({ "Authorization": `Bearer ${token}`});
            res.should.have.status(404);
    })
    it('should return status code 404 (if both course_id and user_id not found)', async function() {
		const token =  admin.generateToken();
        const res = await chai
			.request(app)
            .post('/rights')
        	.send({'user_id':23,'course_id':23})
            .set({ "Authorization": `Bearer ${token}`});
            res.should.have.status(404);
    })

    it('should return status code 409 (if both course_id and user_id are already linked)', async function() {
		const token =  admin.generateToken();
        const res = await chai
			.request(app)
            .post('/rights')
        	.send({'user_id':2,'course_id':2})
            .set({ "Authorization": `Bearer ${token}`});
            res.should.have.status(409);
    })

    it('should return status code 200 (if success)', async function() {
		const token =  admin.generateToken();
        const res = await chai
			.request(app)
            .post('/rights')
        	.send({'user_id':bdd.id,'course_id':admin.id})
            .set({ "Authorization": `Bearer ${token}`});
                
        const userRight = await UserRightsModel.findOne({
                    where: { userId :1,courseId:1}
        });

        userRight.should.to.not.be.null;
       
        res.should.have.status(200);      
    })
})