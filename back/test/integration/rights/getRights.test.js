// Import the dependencies for testing
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../../index');

// Configure chai
chai.use(chaiHttp);
chai.should();
chai.use(require('chai-things'));

// Import model
const { user: UserModel , course : CourseModel , course_bundle : CourseBundleModel, formation : FormationModel, waitForSync, sequelize } = require('../../../db/sequelize');

describe('RIGHTS - GET /rights', function() {

    // Utlisateurs
    var garcia, legeay, goudet, admin;
    // Course
    var bdd, reseau, prog, python, web;
    // Course bundle
    var L3, M1, Options;

    before(async function() {
        // Creation des données requises pour faire les test
        await waitForSync
		
        // Création des utilisateurs
        admin = await UserModel.create({ username: "admin", password: "password", isAdmin: true});
        garcia = await UserModel.create({ username: "garcia", password: "password"});
        legeay = await UserModel.create({ username: "legeay", password: "password" });
        goudet = await UserModel.create({ username:"goudet", password: "password"});

        // Création des course bundle
        L3 = await CourseBundleModel.create({tag:"L3"});
        M1 = await CourseBundleModel.create({tag:"M1"});
        Options = await CourseBundleModel.create({tag:"Options"});

        // Création des formations
        await FormationModel.create({course_bundle_id: L3.id});
        await FormationModel.create({course_bundle_id: M1.id});

        // Création des course
        bdd = await CourseModel.create({name:"bdd" });
        reseau = await CourseModel.create({ name:"reseau" });
        prog = await CourseModel.create({ name:"prog" });
        python = await CourseModel.create({ name:"python" });
        web = await CourseModel.create({ name:"web" });

        // Course <-> CourseBundle
        await bdd.addCourseBundle(L3);
        await reseau.addCourseBundle(L3);
        await prog.addCourseBundle(M1);
        await python.addCourseBundle(Options);
        await web.addCourseBundle(Options);

        // CourseBundle <-> Parent
        await Options.addParent(L3);

        // User <-> Course
        await garcia.addCourse(bdd);
        await legeay.addCourses([reseau,python, web]);
        await goudet.addCourses([reseau, prog]);
    });
            
    after(async function() {
        // Suppression des données de test
        await sequelize.sync({ force: true });
    });

    it('should return status 403 with if user not admin', async function() {
		const token = goudet.generateToken();
        const res = await chai.request(app)
        .get('/rights')
        .set({ "Authorization": `Bearer ${token}`});
        res.should.have.status(403);
    });

    it('should return status code 401 (not being connected)', async function() {
		const res = await chai
			.request(app)
            .get('/rights')
        	.send({});

		res.should.have.status(401);
	});

    it('should return status 200 and all rights', async function() {
		const token = admin.generateToken();
        const res = await chai.request(app)
        .get('/rights')
        .set({ "Authorization": `Bearer ${token}`});

        res.should.have.status(200);
        res.body.length.should.be.equal(6);
        // Garcia -> BDD
        res.body.should.include.something.that.deep.equals({
            user:{
                id: garcia.id,
                username: garcia.username
            },
            course:{
                id: bdd.id,
                name: bdd.name
            },
            formation:{
                id: L3.id,
                name: L3.tag
            }
        });

        // Legeay -> Reseau
        res.body.should.include.something.that.deep.equals({
            user:{
                id: legeay.id,
                username: legeay.username
            },
            course:{
                id: reseau.id,
                name: reseau.name
            },
            formation:{
                id: L3.id,
                name: L3.tag
            }
        });

        // Legeay -> Python
        res.body.should.include.something.that.deep.equals({
            user:{
                id: legeay.id,
                username: legeay.username
            },
            course:{
                id: python.id,
                name: python.name
            },
            formation:{
                id: L3.id,
                name: L3.tag
            }
        });

        // Legeay -> Web
        res.body.should.include.something.that.deep.equals({
            user:{
                id: legeay.id,
                username: legeay.username
            },
            course:{
                id: web.id,
                name: web.name
            },
            formation:{
                id: L3.id,
                name: L3.tag
            }
        });

        // Goudet -> Reseau
        res.body.should.include.something.that.deep.equals({
            user:{
                id: goudet.id,
                username: goudet.username
            },
            course:{
                id: reseau.id,
                name: reseau.name
            },
            formation:{
                id: L3.id,
                name: L3.tag
            }
        });

        // Goudet -> Prog
        res.body.should.include.something.that.deep.equals({
            user:{
                id: goudet.id,
                username: goudet.username
            },
            course:{
                id: prog.id,
                name: prog.name
            },
            formation:{
                id: M1.id,
                name: M1.tag
            }
        });
    });

    it('should return status 200 and rights with user id', async function() {
		const token = admin.generateToken();
        const res = await chai.request(app)
        .get(`/rights?user_id=${goudet.id}`)
        .set({ "Authorization": `Bearer ${token}`});

        res.should.have.status(200);
        res.body.length.should.be.equal(2);

        // Goudet -> Reseau
        res.body.should.include.something.that.deep.equals({
            user:{
                id: goudet.id,
                username: goudet.username
            },
            course:{
                id: reseau.id,
                name: reseau.name
            },
            formation:{
                id: L3.id,
                name: L3.tag
            }
        });

        // Goudet -> Reseau
        res.body.should.include.something.that.deep.equals({
            user:{
                id: goudet.id,
                username: goudet.username
            },
            course:{
                id: prog.id,
                name: prog.name
            },
            formation:{
                id: M1.id,
                name: M1.tag
            }
        });
    });

    it('should return status 200 and rights with course id', async function() {
		const token = admin.generateToken();
        const res = await chai.request(app)
        .get(`/rights?course_id=${reseau.id}`)
        .set({ "Authorization": `Bearer ${token}`});

        res.should.have.status(200);
        res.body.length.should.be.equal(2);

        // Legeay -> Reseau
        res.body.should.include.something.that.deep.equals({
            user:{
                id: legeay.id,
                username: legeay.username
            },
            course:{
                id: reseau.id,
                name: reseau.name
            },
            formation:{
                id: L3.id,
                name: L3.tag
            }
        });

        // Goudet -> Reseau
        res.body.should.include.something.that.deep.equals({
            user:{
                id: goudet.id,
                username: goudet.username
            },
            course:{
                id: reseau.id,
                name: reseau.name
            },
            formation:{
                id: L3.id,
                name: L3.tag
            }
        });
    });

    it('should return status 200 and rights with formation id', async function() {
		const token = admin.generateToken();
        const res = await chai.request(app)
        .get(`/rights?formation_id=${M1.id}`)
        .set({ "Authorization": `Bearer ${token}`});

        res.should.have.status(200);
        res.body.length.should.be.equal(1);

        // Goudet -> Reseau
        res.body.should.include.something.that.deep.equals({
            user:{
                id: goudet.id,
                username: goudet.username
            },
            course:{
                id: prog.id,
                name: prog.name
            },
            formation:{
                id: M1.id,
                name: M1.tag
            }
        });
    });

    it('should return status 200 and rights with course_id and user_id', async function() {
		const token = admin.generateToken();
        const res = await chai.request(app)
        .get(`/rights?course_id=${reseau.id}&user_id=${legeay.id}`)
        .set({ "Authorization": `Bearer ${token}`});

        res.should.have.status(200);
        res.body.length.should.be.equal(1);

        // Legeay -> Reseau
        res.body.should.include.something.that.deep.equals({
            user:{
                id: legeay.id,
                username: legeay.username
            },
            course:{
                id: reseau.id,
                name: reseau.name
            },
            formation:{
                id: L3.id,
                name: L3.tag
            }
        });
    });
});
