// Import the dependencies for testing
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../../index');

// Configure chai
chai.use(chaiHttp);
chai.should();

// Import model
const { user: UserModel , waitForSync, sequelize } = require('../../../db/sequelize');

describe('USER - GET /users/search', function() {

	// Donnée global :
	let userT, adminT;

	// Attente chargement BDD :
	before( async () => await waitForSync );

	// A la fin purge des données :
	after( async () => await sequelize.sync({ force: true }) );

	before( async function() {
		// Création des comptes utilisateurs pour la connexion
		const [user, admin] = await UserModel.bulkCreate([
			{ username: "tom", password: "akfoqmfenp" },
			{ username: "charles", password: "davieqlgos", isAdmin: true }
		]);
		userT = user.generateToken();
		adminT = admin.generateToken();

		// Créations des comptes utilisateurs pour la recherche
		await UserModel.bulkCreate( [
			{ username: "Eins", password: "davieqlgos", id:41 },
			{ username: "zwei", password: "davieqlgos", id:42 },
			{ username: "drEi", password: "davieqlgos", id:43 },
			{ username: "VIER", password: "davieqlgos", id:44 },
			{ username: "fünf", password: "davieqlgos", id:45 },
			{ username: "unos", password: "davieqlgos", id:14 },
			{ username: "dos", password: "davieqlgos", id:15 },
			{ username: "tres", password: "davieqlgos", id:16 },
			{ username: "quatros", password: "davieqlgos", id:17 },
			{ username: "cinquos", password: "davieqlgos", id:18 },
			{ username: "alpha", password: "davieqlgos", id:3},
			{ username: "beta", password: "davieqlgos", id:4}
		] );
	});

	it('should return status code 401 (not being connected)', async function() {
		const res = await chai
			.request(app)
        	.get('/users/search')
        	.send({});

		res.should.have.status(401);
	});

	it('should return status code 403 (connected but not authorized)', async function() {
		const res = await chai
			.request(app)
        	.get('/users/search')
			.set({ "Authorization": `Bearer ${userT}` })
        	.send({});

		res.should.have.status(403);
	});

	it('should return status code 200 but empty user list (connected as admin but user does not exist)', async function() {
		const res = await chai
			.request(app)
        	.get('/users/search?name=idonotexist')
			.set({ "Authorization": `Bearer ${adminT}` });

		res.should.have.status(200);
		res.body.length.should.equal(0);
	});

	it('should return status code 200 with user data', async function() {
		const res = await chai
			.request(app)
        	.get(`/users/search?name=beta`)
			.set({ "Authorization": `Bearer ${adminT}` });

		res.should.have.status(200);
		chai.assert.deepEqual(res.body, 
			[
				{
					id: 4,
					username: 'beta'
				}
			]
		);
	});

	it('should return status code 200 with multiple user data', async function() {
		const res = await chai
			.request(app)
        	.get(`/users/search?name=os`)
			.set({ "Authorization": `Bearer ${adminT}` });

		res.should.have.status(200);
		chai.assert.deepEqual(res.body, 
			[
				{ id: 14, username: 'unos' },
				{ id: 15, username: 'dos' },
				{ id: 17, username: 'quatros' },
				{ id: 18, username: 'cinquos' }
			]
		);
	});

	it('should return status code 200 with multiple user data (case sensitivity)', async function() {
		const res = await chai
			.request(app)
        	.get(`/users/search?name=eI`)
			.set({ "Authorization": `Bearer ${adminT}` });

		res.should.have.status(200);
		chai.assert.deepEqual(res.body, 
			[
				{ id: 41, username: 'Eins' },
				{ id: 42, username: 'zwei' },
				{ id: 43, username: 'drEi' }
			]
		);

	});
});