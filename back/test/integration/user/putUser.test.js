// Import the dependencies for testing
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../../index');

// Configure chai
chai.use(chaiHttp);
chai.should();

// Import model
const { user: UserModel , waitForSync, sequelize } = require('../../../db/sequelize');

describe('USER - PUT /users', function() {

	let jeannesToken;

	// Attente chargement BDD :
	before( async () => await waitForSync );

	// A la fin purge des données :
	after( async () => await sequelize.sync({ force: true }) );

	before( async function() {
		// Créations des comptes utilisateurs
		let jeannesAccount = await UserModel.bulkCreate( [... new Array(5)].map( (_,i) => ( { username: `jeanne${i}` , password: 'password' , id : i+1 } ) ) );

		jeannesToken = jeannesAccount.map( jeanne => jeanne.generateToken() );
	});

    it('should return status 401 (not being connected)', async function() {
        const res = await chai.request(app)
			.put('/users')
			.send();
        
		// Vérification de la réponse : 
        res.should.have.status(401);
    });

    it('should return status 200 (change username only)', async function() {
        const res = await chai.request(app)
        	.put('/users')
        	.set({ "Authorization": `Bearer ${jeannesToken[0]}` })
			.send({
				username: "jean"
			});

		// Vérification de la réponse : 
        res.should.have.status(200);
        res.body.username.should.be.equal("jean");

		// Vérification de la BDD :
		const user = await UserModel.findByPk(1);
		user.username.should.be.equal('jean');
		chai.assert.equal(user.validatePassword('password'), true, 'Password should have same bcrypt output');
    });

    it('should return status 200 (change password only)', async function() {
        const res = await chai.request(app)
        	.put('/users')
        	.set({ "Authorization": `Bearer ${jeannesToken[1]}` })
			.send({
				password: "motdepasse"
			});

		// Vérification de la réponse : 
        res.should.have.status(200);
        res.body.username.should.be.equal("jeanne1");

		// Vérification de la BDD :
		const user = await UserModel.findByPk(2);
		user.username.should.be.equal('jeanne1');
		chai.assert(user.validatePassword('motdepasse'), true, 'Password should have same bcrypt output');
    });

    it('should return status 200 (change both password and username)', async function() {
        const res = await chai.request(app)
			.put('/users')
			.set({ "Authorization": `Bearer ${jeannesToken[2]}` })
			.send({
				username: "jean2",
				password: "motdepasse"
			});

		// Vérification de la réponse : 
        res.should.have.status(200);
        res.body.username.should.be.equal("jean2");

		// Vérification de la BDD :
		const user = await UserModel.findByPk(3);
		user.username.should.be.equal('jean2');
		chai.assert.equal(user.validatePassword('motdepasse'), true, 'Password should have same bcrypt output');
    });

    it('should return status 200 (change nothing)', async function() {
        const res = await chai.request(app)
			.put('/users')
			.set({ "Authorization": `Bearer ${jeannesToken[3]}` })
			.send({});

		// Vérification de la réponse : 
        res.should.have.status(200);
        res.body.username.should.be.equal("jeanne3");

		// Vérification de la BDD :
		const user = await UserModel.findByPk(4);
		user.username.should.be.equal('jeanne3');
		chai.assert(user.validatePassword('password'), true, 'Password should have same bcrypt output');
    });

    it('should return status 409 (change to something who already exist)', async function() {
        const res = await chai.request(app)
			.put('/users')
			.set({ "Authorization": `Bearer ${jeannesToken[4]}` })
			.send({
				username: "jean"
			});

		// Vérification de la réponse : 
        res.should.have.status(409);
    });
});