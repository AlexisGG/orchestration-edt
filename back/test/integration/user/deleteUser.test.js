// Import the dependencies for testing
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../../index');

// Configure chai
chai.use(chaiHttp);
chai.should();

// Import model
const { user: UserModel , waitForSync, sequelize } = require('../../../db/sequelize');

describe('USER - DELETE /users', function() {

    var jeanne;

    before(function(done) {
        // Creation des données requises pour faire les test
        waitForSync.then( () => {
            UserModel.create({ username: "jeanne", password: "password", id: 4}).then(user => {
                jeanne = user;
                done();
            })
        });
    });
    
    after(async function() {
        // Suppression des données de test
        await sequelize.sync({ force: true });
    });

    it('should return status 401 with no token', async function() {
        const res = await chai.request(app)
        .delete('/users');
        
        res.should.have.status(401);
        const user = await UserModel.findOne({ where : {
			username : "jeanne"
		}});
        user.username.should.be.equal('jeanne');
        user.id.should.be.equal(4);
        chai.assert(user.validatePassword('password'), true, 'Password is the same');


    });

    it('should return status 200 and delete user from database', async function() {
        const token = jeanne.generateToken();
        const res = await chai.request(app)
        .delete('/users')
        .set({ "Authorization": `Bearer ${token}` });

        res.should.have.status(200);
        
		// Vérification de la BDD :
		const user = await UserModel.findOne({ where : {
			username : "jeanne"
		}});
		chai.assert(user, null, 'user should be null because he has been deleted');
    });


});