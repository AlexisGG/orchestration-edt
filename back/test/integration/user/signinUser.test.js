// Import the dependencies for testing
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../../index');

// Configure chai
chai.use(chaiHttp);
chai.use(require('chai-datetime'))
chai.should();

const expect = chai.expect;

// Import model
const { user: UserModel, waitForSync, sequelize } = require('../../../db/sequelize');

describe('USER - POST /users/register', function() {

    before(function(done) {
        // Creation des données requises pour faire les test
        waitForSync.then( () => {
            UserModel.create({ username: "albert", password: "password" }).then(res => {
                done();
            })
        });
    });
  
    after(async function() {
        // Suppression des données de test
        await sequelize.sync({ force: true });
    });

    it('should return status 200 and user data', async function() {
        const res = await chai.request(app)
        .post('/users/register')
        .send({
            "username":"jean",
            "password":"password"
        });
        
        // Vérification de la réponse
        res.should.have.status(200);
        res.body.username.should.be.equal("jean");
        res.body.isAdmin.should.be.false;

        // Vérification en base de données
        const user = await UserModel.findByPk(res.body.id);
        user.username.should.be.equal(res.body.username);
        user.isAdmin.should.be.false;
        user.createdAt.should.be.closeToTime(new Date(res.body.createdAt), 1);
        user.updatedAt.should.be.closeToTime(new Date(res.body.updatedAt), 1);
    });

    it('should return status 400 (username too short)', async function() {
        const res = await chai.request(app)
        .post('/users/register')
        .send({
            "username":"j",
            "password":"password"
        });

        // Vérification de la réponse
        res.should.have.status(400);

        // Vérification en base de données
        const user = await UserModel.findOne({where : {username : "j"}});
        expect(user).to.be.null;
    });

    it('should return status 409 (username already taken)', async function() {
        const res = await chai.request(app)
        .post('/users/register')
        .send({
            "username":"albert",
            "password":"password2"
        });
        
        // Vérification de la réponse
        res.should.have.status(409);
    });

    it('should return status 406 (missing parameter : username)', async function() {
        const res = await chai.request(app)
        .post('/users/register')
        .send({
            "password":"password2"
        });
        
        // Vérification de la réponse
        res.should.have.status(406);
		res.body.field.should.equal('username');
    });

    it('should return status 406 (missing parameter : password)', async function() {
        const res = await chai.request(app)
        .post('/users/register')
        .send({
            "username":"nemarcherapas"
        });
        
        // Vérification de la réponse
        res.should.have.status(406);
		res.body.field.should.equal('password');
    });

});