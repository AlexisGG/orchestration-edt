// Import the dependencies for testing
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../../index');

// Configure chai
chai.use(chaiHttp);
chai.should();

// Import model
const { user: UserModel , waitForSync, sequelize } = require('../../../db/sequelize');

describe('USER - GET /users/disconnect', function() {

    var pierre, paul, jacque;

    before(function(done) {
        // Creation des données requises pour faire les test
        waitForSync
			.then( () => UserModel.bulkCreate([
                { username: "pierre", password: "password" },
                { username: "paul", password: "password" },
                { username: "jacque", password: "password" }
            ]))
			.then( () => {
                return UserModel.findAll()
            })
			.then( (users) => {
                pierre = users[0];
                paul = users[1];
                jacque = users[2];
                done();
            });
    });
    
    after(async function() {
        // Suppression des données de test
        await sequelize.sync({ force: true });
    });

    it('should return status 401', async function() {
        const res = await chai.request(app)
        .get('/users/disconnect');
        
        res.should.have.status(401);
    });

    it('should return 200', async function() {
        const token = pierre.generateToken();
        const res = await chai.request(app)
        .get('/users/disconnect')
        .set({ "Authorization": `Bearer ${token}` });

        res.should.have.status(200);
		
    });

    it('should return 200 and be disconnected (same endpoint test)', async function() {
        const token = paul.generateToken();
        const res = await chai.request(app)
        .get('/users/disconnect')
        .set({ "Authorization": `Bearer ${token}` });

        res.should.have.status(200);

		const failed_req = await chai.request(app)
		.get('/users/disconnect')
        .set({ "Authorization": `Bearer ${token}` })

		failed_req.should.have.status(401);
    });

    it('should return 200 and be disconnected (different endpoint test)', async function() {
        const token = jacque.generateToken();
        const res = await chai.request(app)
        .get('/users/disconnect')
        .set({ "Authorization": `Bearer ${token}` });

        res.should.have.status(200);

		const failed_req = await chai.request(app)
		.get('/users')
        .set({ "Authorization": `Bearer ${token}` })

		failed_req.should.have.status(401);
    });
});