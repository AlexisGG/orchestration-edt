// Import the dependencies for testing
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../../index');

// Configure chai
chai.use(chaiHttp);
chai.should();

// Import model
const { user: UserModel, waitForSync, sequelize } = require('../../../db/sequelize');

describe('USER - POST /users/login', function() {

    before(function(done) {
        // Creation des données requises pour faire les test
        waitForSync.then( () => {
            UserModel.create({ username: "jeanne", password: "password" }).then(res => {
                done();
            })
        });
    });
    
    after(async function() {
        // Suppression des données de test
        await sequelize.sync({ force: true });
    });

    it('should return status 200, user data and token', async function() {
        const res = await chai.request(app)
        .post('/users/login')
        .send({
            "username":"jeanne",
            "password":"password"
        })

        // Vérification de la réponse
        res.should.have.status(200);
        res.body.username.should.be.equal("jeanne");
        res.body.isAdmin.should.be.false;
    });

    it('should return error 400', async function() {
        const res = await chai.request(app)
        .post('/users/login')
        .send({
            "username":"jeanne",
            "password":"password_failed"
        });

        res.should.have.status(400);
    });

    it('should return status 406 (missing parameter : username)', async function() {
        const res = await chai.request(app)
        .post('/users/login')
        .send({
            "password":"password2"
        });
        
        // Vérification de la réponse
        res.should.have.status(406);
		res.body.field.should.equal('username');
    });

    it('should return status 406 (missing parameter : password)', async function() {
        const res = await chai.request(app)
        .post('/users/login')
        .send({
            "username":"nemarcherapas"
        });
        
        // Vérification de la réponse
        res.should.have.status(406);
		res.body.field.should.equal('password');
    });
});