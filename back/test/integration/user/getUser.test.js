// Import the dependencies for testing
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../../index');;

// Configure chai
chai.use(chaiHttp);
chai.should();

// Import model
const { user: UserModel , waitForSync, sequelize } = require('../../../db/sequelize');

describe('USER - GET /users', function() {

    var jeanne;

    before(function(done) {
        // Creation des données requises pour faire les test
        waitForSync.then( () => {
            UserModel.create({ username: "jeanne", password: "password" }).then(user => {
                jeanne = user;
                done();
            })
        });
    });
    
    after(async function() {
        // Suppression des données de test
        await sequelize.sync({ force: true });
    });

    it('should return status 401 with no token', async function() {
        const res = await chai.request(app)
        .get('/users');
        
        res.should.have.status(401);
    });

    it('should return status 200 and user data', async function() {
        const token = jeanne.generateToken();
        const res = await chai.request(app)
        .get('/users')
        .set({ "Authorization": `Bearer ${token}` });
        
        res.should.have.status(200);
        res.body.username.should.be.equal("jeanne");
        res.body.isAdmin.should.be.false;
    }); 
});