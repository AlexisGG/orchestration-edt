// Import the dependencies for testing
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../../index');

// Configure chai
chai.use(chaiHttp);
chai.should();
chai.use(require('chai-things'));

// Import model
const { 
	user : UserModel,
    trigger : TriggerModel,
    course: CourseModel,
    rule : RuleModel,
    rule_parameter : ParameterModel,
    scope : ScopeModel,
    element_type : ElementTypeModel,
    filter : FilterModel,
    label : LabelModel,
    element : ElementModel,
    
	waitForSync, sequelize } = require('../../../db/sequelize');

describe('TRIGGER - GET /trigger/:trigger_id', function() {

	let alphaT,betaT,gamaT;

	before( function(done) {
		// Mise en attente pour préparation de la BDD
		waitForSync.then( function () { done() } );
	});

	// Création des jeu de données pour les test:
	before( async function() {
		// Création des comptes utilisateurs
		const [ alpha, beta, gama ] = await UserModel.bulkCreate([
			{ username: "alpha", password: "alpha", isAdmin: true },
			{ username: "beta", password: "beta" , isAdmin: false},
			{ username: "gama", password: "gama" , isAdmin: false}
		]);

		// Création des Course, Part, Trigger, Rule, Scope
		const [ Ca, Cb , Cc ] = await CourseModel.bulkCreate([
			{id:101,name:"algo1"},
			{id:102,name:"web1"},
			{id:103,name:"assembleur"}
		]);

		const [ Ta, Tb, Tc, Td ] = await TriggerModel.bulkCreate([
			{text:"Ich bin ein kartofle",id:1,file:"pref.json", request : "REQ1"},
			{text:"Soy una papa",id:2,file:"pref.json", request : "REQ1"},
			{text:"Je suis une patate",id:3,file:"pref.json", request : "REQ1"},
			{text:"I'm a potato",id:4,file:"pref.json", request : "REQ1"}
		]);

		const [ Ra, Rb, Rc] = await RuleModel.bulkCreate([
			{id:1},
			{id:2},
			{id:3}
		]);

		const [ Sa, Sb, Sc , Sd] = await ScopeModel.bulkCreate([
			({mask:'1', scope_order:1, id:1}),
			({mask:'1-3', scope_order:2, id:2}),
			({mask:'1,3-5', scope_order:3, id:3}),
			({mask:'1-9', scope_order:4, id:4})
		]);

    const [Pa, Pb, Pc, Pd] = await ParameterModel.bulkCreate([
      ({id:1, name:'param 1',type:'type 1',value:'value 1'}),
      ({id:2, name:'param 2',type:'type 2',value:'value 2'}),
      ({id:3, name:'param 3',type:'type 3',value:'value 3'}),
      ({id:4, name:'param 4',type:'type 4',value:'value 4'})
    ]);

    const [El1,El2,El3,El4] = await ElementModel.bulkCreate([
      ({id:1}),
      ({id:2}),
      ({id:3}),
      ({id:4}),
    ]);

    const [Et1,Et2,Et3,Et4] = await ElementTypeModel.bulkCreate([
      ({id:1,name:'part'}),
      ({id:2,name:'course'}),
      ({id:3,name:'room'}),
      ({id:4,name:'class'})
    ]);

    const [Fm1,Fm2,Fm3,Fm4] = await FilterModel.bulkCreate([
      ({id:1}), 
      ({id:2}), 
      ({id:3}), 
      ({id:4})
    ]);

    const [Lb1,Lb2,Lb3,Lb4] = await LabelModel.bulkCreate([
      ({id:1,name:'label 1'}),
      ({id:2,name:'label 2'}),
      ({id:3,name:'label 3'}),
      ({id:4,name:'label 4'})
    ]);

		// Liaisons des éléments :
		await Promise.all([
			Ca.addTrigger(Ta),
			Ca.addTrigger(Tb),
			Ca.addTrigger(Tc),
			Cb.addTrigger(Td),
			Tb.addRule(Ra),
			Ta.addRule(Rb),
			Tb.addRule(Rc),
			Ra.addScope(Sa),
			Ra.addScope(Sb),
			Rb.addScope(Sc),
			Rc.addScope(Sd),
          Ra.addParameter(Pa),
          Rb.addParameter(Pb),
          Rc.addParameter(Pc),
          Ra.addParameter(Pd),
          Et1.addScope(Sa),
          Et2.addScope(Sb),
          Et3.addScope(Sc),
          Sa.addFilter(Fm1),
          Sa.addFilter(Fm2),
          Sb.addFilter(Fm3),
          Sc.addFilter(Fm4),
          Fm1.addLabel(Lb1),
          Fm2.addLabel(Lb2),
          Fm3.addLabel(Lb3),
          Fm4.addLabel(Lb4),
          Et1.addFilter(Fm1),
          Et2.addFilter(Fm2),
          Et3.addFilter(Fm3),
          Fm1.addElement(El1),
          Fm1.addElement(El2),
          Fm3.addElement(El3),
          Fm4.addElement(El4)    
		]);

		// Droit sur cours
		await Promise.all([
			gama.addCourse( Ca ),
			gama.addCourse( Cb ),
			gama.addCourse( Cc )
		]);

		alphaT = alpha.generateToken();
		betaT = beta.generateToken();
		gamaT = gama.generateToken();
	});

  after(async function() {
      // Suppression des données de test
      await sequelize.sync({ force: true });
  });

  it('should return status 401 if user not connected', async function() {
      const res = await chai.request(app)
      .get('/trigger/1')
      .send();
      res.should.have.status(401);
  });

  it('should return status 403 if user connected but has not rights', async function() {
      const res = await chai.request(app)
      .get('/trigger/1')
      .set({ "Authorization": `Bearer ${betaT}`})

      res.should.have.status(403);
  });

  it('should return status 200 if user is Admin', async function() {
    const res = await chai.request(app)
    .get('/trigger/1')
    .set({ "Authorization": `Bearer ${alphaT}`})

    
    res.should.have.status(200);
  
    res.body.id.should.be.equal(1);
    res.body.text.should.be.equal('Ich bin ein kartofle');
    res.body.file.should.be.equal('pref.json');
    res.body.request.should.be.equal('REQ1');
    res.body.course.id.should.be.equal(101);
    res.body.course.name.should.be.equal('algo1');
    res.body.rules.should.include.something.that.deep.equals({ 
      id:2,
      constraint_name: null,
      constraint_type: null,
      parameter: [{
        id: 2,
        name: "param 2",
        type: "type 2",
        value: "value 2"
      }],
      scopes : [{
        filters: [{
            "elements": [{
              filter_element: {
                element_id: 4,
                filter_id: 4
              },
              id: 4,
            }],
            id: 4,
            including: 1,
            label: 0,
            labels: [
              {
                "filter_label": {
                  "filter_id": 4,
                  "label_id": 4
                },
                id: 4,
                name: "label 4"
              }
            ],
            type: null
        }],
        groupBy: {
          "id": 3,
          name: "room"
        },
        id: 3,
        mask: "1,3-5",
        scope_order: 3
      }]
    });
  });

  it('should return status 200 if user has rights', async function() {
    const res = await chai.request(app)
    .get('/trigger/1')
    .set({ "Authorization": `Bearer ${gamaT}`})

    res.should.have.status(200);
  
    res.body.id.should.be.equal(1);
    res.body.text.should.be.equal('Ich bin ein kartofle');
    res.body.file.should.be.equal('pref.json');
    res.body.request.should.be.equal('REQ1');
    res.body.course.id.should.be.equal(101);
    res.body.course.name.should.be.equal('algo1');
    res.body.rules.should.include.something.that.deep.equals({ 
      id:2,
      constraint_name: null,
      constraint_type: null,
      parameter: [{
        id: 2,
        name: "param 2",
        type: "type 2",
        value: "value 2"
      }],
      scopes : [{
        filters: [{
            "elements": [{
              filter_element: {
                element_id: 4,
                filter_id: 4
              },
              id: 4,
            }],
            id: 4,
            including: 1,
            label: 0,
            labels: [
              {
                "filter_label": {
                  "filter_id": 4,
                  "label_id": 4
                },
                id: 4,
                name: "label 4"
              }
            ],
            type: null
        }],
        groupBy: {
          "id": 3,
          name: "room"
        },
        id: 3,
        mask: "1,3-5",
        scope_order: 3
      }]
    });   
  });
})