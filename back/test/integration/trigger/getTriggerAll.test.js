// Import the dependencies for testing
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../../index');

// Configure chai
chai.use(chaiHttp);
chai.should();
chai.use(require('chai-things'));

// Import model
const { 
	user: UserModel ,
	course: CourseModel,
	trigger: TriggerModel,
	rule: RuleModel,
	waitForSync, sequelize } = require('../../../db/sequelize');

describe('TRIGGER - GET /trigger/all/{course_id}', function() {

	let alphaT,betaT,gamaT;

	before( function(done) {
		// Mise en attente pour préparation de la BDD
		waitForSync.then( function () { done() } );
	});

	// Création des jeu de données pour les test:
	before( async function() {
		// Création des comptes utilisateurs
		const [ alpha, beta, gama ] = await UserModel.bulkCreate([
			{ username: "alpha", password: "alpha", isAdmin: true },
			{ username: "beta", password: "beta" , isAdmin: false},
			{ username: "gama", password: "gama" , isAdmin: false}
		]);

		// Création des Course, Part, Trigger, Rule, Scope
		const [ Ca, Cb , Cc ] = await CourseModel.bulkCreate([
			{id:101,name:"algo1"},
			{id:102,name:"web1"},
			{id:103,name:"assembleur"}
		]);

		const [ Ta, Tb, Tc, Td ] = await TriggerModel.bulkCreate([
			{text:"Ich bin ein kartofle",id:1,file:"pref.json", request : "REQ1"},
			{text:"Soy una papa",id:2,file:"pref.json", request : "REQ1"},
			{text:"Je suis une patate",id:3,file:"pref.json", request : "REQ1"},
			{text:"I'm a potato",id:4,file:"pref.json", request : "REQ1"}
		]);

		const [ Ra, Rb, Rc] = await RuleModel.bulkCreate([
			{id:1},
			{id:2},
			{id:3}
		]);

		// Liaisons des éléments :
		await Promise.all([
			Ca.addTrigger(Ta),
			Ca.addTrigger(Tb),
			Ca.addTrigger(Tc),
			Cb.addTrigger(Td),
			Ta.addRule(Ra),
			Ta.addRule(Rb),
			Tb.addRule(Rc),
			
		]);

		// Droit sur cours
		await Promise.all([
			gama.addCourse( Ca ),
			gama.addCourse( Cb ),
			gama.addCourse( Cc )
		]);
        alphaT = alpha.generateToken();
		betaT = beta.generateToken();
		gamaT = gama.generateToken();

	});
    after(async function() {
        // Suppression des données de test
        await sequelize.sync({ force: true });
    });

    it('should return status 401 (not connected)', async function() {
		
        const res = await chai.request(app)
        .get('/trigger/all/101')
        .send({});
        res.should.have.status(401);
    });
    it('should return status 403 (has not rights)', async function() {
		
        const res = await chai.request(app)
        .get('/trigger/all/102')
        .set({ "Authorization": `Bearer ${betaT}`});        

        res.should.have.status(403);
    });
    

    it('should return status 404 (admin but not found)', async function() {
		
        const res = await chai.request(app)
        .get('/trigger/all')
        .set({ "Authorization": `Bearer ${alphaT}`});        

        res.should.have.status(404);
    });
    it('should return status 403 (has not rights)', async function() {
		
        const res = await chai.request(app)
        .get('/trigger/all/102')
        .set({ "Authorization": `Bearer ${betaT}`});        

        res.should.have.status(403);
    });

	it('should return status 200 (if admin)', async function() {
		
        const res = await chai.request(app)
        .get('/trigger/all/102')
        .set({ "Authorization": `Bearer ${alphaT}`})
		.send();   
		
		res.body.should.include.something.that.deep.equals({ 
			id: 4, 
			text: "I'm a potato",
			has_been_changed : 0,
			file : 'pref.json',
    		request : 'REQ1',
			course_id: 102, 
			rules_id: [] }
		)
		
    });

    it('should return status 200 (has rights)', async function() {
		
        const res = await chai.request(app)
        .get('/trigger/all/101')
        .set({ "Authorization": `Bearer ${gamaT}`});
		res.should.have.status(200);

		res.body.should.include.something.that.deep.equals({
			id: 1,
			text: 'Ich bin ein kartofle',
			file : 'pref.json',
    		request : 'REQ1',
			has_been_changed : 0,
			course_id: 101,
			rules_id: [ 1, 2 ]
		  },
		  { id: 2, text: 'Soy una papa', has_been_changed : 0 ,course_id: 101, rules_id: [ 3 ] },
		  { id: 3, text: 'Je suis une patate', has_been_changed : 0, course_id: 101, rules_id: [] }
		)
    });
})