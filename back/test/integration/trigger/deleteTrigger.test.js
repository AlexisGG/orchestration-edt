// Import the dependencies for testing
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../../index');

// Configure chai
chai.use(chaiHttp);
chai.should();

// Import model
const { 
	user: UserModel ,
	course: CourseModel,
	trigger: TriggerModel,
	rule: RuleModel,
	scope: ScopeModel,
	waitForSync, sequelize } = require('../../../db/sequelize');

describe('TRIGGER - DELETE /trigger/{trigger_id}', function() {

	let alphaT,betaT,gamaT;

	before( function(done) {
		// Mise en attente pour préparation de la BDD
		waitForSync.then( function () { done() } );
	});

	// Création des jeu de données pour les test:
	before( async function() {
		// Création des comptes utilisateurs
		const [ alpha, beta, gama ] = await UserModel.bulkCreate([
			{ username: "alpha", password: "alpha", isAdmin: true },
			{ username: "beta", password: "beta" , isAdmin: false},
			{ username: "gama", password: "gama" , isAdmin: false}
		]);

		// Création des Course, Part, Trigger, Rule, Scope
		const [ Ca, Cb , Cc ] = await CourseModel.bulkCreate([
			{id:101,name:"algo1"},
			{id:102,name:"web1"},
			{id:103,name:"assembleur"}
		]);

		const [ Ta, Tb, Tc, Td ] = await TriggerModel.bulkCreate([
			{text:"Ich bin ein kartofle",id:1,file:"pref.json", request : "REQ1"},
			{text:"Soy una papa",id:2,file:"pref.json", request : "REQ1"},
			{text:"Je suis une patate",id:3,file:"pref.json", request : "REQ1"},
			{text:"I'm a potato",id:4,file:"pref.json", request : "REQ1"}
		]);

		const [ Ra, Rb, Rc] = await RuleModel.bulkCreate([
			{id:1},
			{id:2},
			{id:3}
		]);

		const [ Sa, Sb, Sc , Sd] = await ScopeModel.bulkCreate([
			({mask:'1', scope_order:1, id:1}),
			({mask:'1-3', scope_order:2, id:2}),
			({mask:'1,3-5', scope_order:3, id:3}),
			({mask:'1-9', scope_order:4, id:4})
		]);

		try { Ca.addCourseBundle() } catch(e) { console.error(e) }

		// Liaisons des éléments :
		await Promise.all([
			Ca.addTrigger(Ta),
			Ca.addTrigger(Tb),
			Ca.addTrigger(Tc),
			Cb.addTrigger(Td),
			Ta.addRule(Ra),
			Ta.addRule(Rb),
			Tb.addRule(Rc),
			Ra.addScope(Sa),
			Ra.addScope(Sb),
			Rb.addScope(Sc),
			Rc.addScope(Sd)
		]);

		// Droit sur cour
		await Promise.all([
			gama.addCourse( Ca ),
			gama.addCourse( Cb ),
			gama.addCourse( Cc )
		]);

		alphaT = alpha.generateToken();
		betaT = beta.generateToken();
		gamaT = gama.generateToken();
	});

    after(async function() {
        // Suppression des données de test
        await sequelize.sync({ force: true });
    });

	it('should return 401 (not connected)', async function() {		
        const res = await chai.request(app)
        	.delete('/trigger/1')
			.send();

		res.should.have.status(401);
	});

	it('should return 403 (connected but refused on this course)', async function() {
		const res = await chai.request(app)
        	.delete('/trigger/1')
        	.set({ "Authorization": `Bearer ${betaT}` })
			.send();

		res.should.have.status(403);
		// Vérification BDD :
		// - Trigger :
		chai.assert.notEqual( (await TriggerModel.findOne({where : {id:1}})) , null );
		chai.assert.notEqual( (await TriggerModel.findOne({where : {id:2}})) , null );
		chai.assert.notEqual( (await TriggerModel.findOne({where : {id:3}})) , null );
		chai.assert.notEqual( (await TriggerModel.findOne({where : {id:4}})) , null );
		// - Rule & Scope :
		chai.assert.notEqual( (await RuleModel.findOne({where : {id:1}})) , null );
		chai.assert.notEqual( (await RuleModel.findOne({where : {id:2}})) , null );
		chai.assert.notEqual( (await RuleModel.findOne({where : {id:3}})) , null );
		chai.assert.notEqual( (await ScopeModel.findOne({where : {id:1}})) , null );
		chai.assert.notEqual( (await ScopeModel.findOne({where : {id:2}})) , null );
		chai.assert.notEqual( (await ScopeModel.findOne({where : {id:3}})) , null );
		chai.assert.notEqual( (await ScopeModel.findOne({where : {id:4}})) , null );
	});

	it('should return 404 (trigger does not exist)', async function() {
		const res = await chai.request(app)
        	.delete('/trigger/999')
        	.set({ "Authorization": `Bearer ${alphaT}` })
			.send();

		res.should.have.status(404);
		// Vérification BDD :
		// - Trigger :
		chai.assert.notEqual( (await TriggerModel.findOne({where : {id:1}})) , null );
		chai.assert.notEqual( (await TriggerModel.findOne({where : {id:2}})) , null );
		chai.assert.notEqual( (await TriggerModel.findOne({where : {id:3}})) , null );
		chai.assert.notEqual( (await TriggerModel.findOne({where : {id:4}})) , null );
		// - Rule & Scope :
		chai.assert.notEqual( (await RuleModel.findOne({where : {id:1}})) , null );
		chai.assert.notEqual( (await RuleModel.findOne({where : {id:2}})) , null );
		chai.assert.notEqual( (await RuleModel.findOne({where : {id:3}})) , null );
		chai.assert.notEqual( (await ScopeModel.findOne({where : {id:1}})) , null );
		chai.assert.notEqual( (await ScopeModel.findOne({where : {id:2}})) , null );
		chai.assert.notEqual( (await ScopeModel.findOne({where : {id:3}})) , null );
		chai.assert.notEqual( (await ScopeModel.findOne({where : {id:4}})) , null );
	});

	it('should return 200 and remove trigger (without rule linked | as admin )', async function() {
		const res = await chai.request(app)
        	.delete('/trigger/3')
        	.set({ "Authorization": `Bearer ${alphaT}` })
			.send();

		res.should.have.status(200);
		// Vérification BDD :
		// - Trigger :
		chai.assert.notEqual( (await TriggerModel.findOne({where : {id:1}})) , null );
		chai.assert.notEqual( (await TriggerModel.findOne({where : {id:2}})) , null );
		chai.assert.equal( (await TriggerModel.findOne({where : {id:3}})) , null );
		chai.assert.notEqual( (await TriggerModel.findOne({where : {id:4}})) , null );
		// - Rule & Scope :
		chai.assert.notEqual( (await RuleModel.findOne({where : {id:1}})) , null );
		chai.assert.notEqual( (await RuleModel.findOne({where : {id:2}})) , null );
		chai.assert.notEqual( (await RuleModel.findOne({where : {id:3}})) , null );
		chai.assert.notEqual( (await ScopeModel.findOne({where : {id:1}})) , null );
		chai.assert.notEqual( (await ScopeModel.findOne({where : {id:2}})) , null );
		chai.assert.notEqual( (await ScopeModel.findOne({where : {id:3}})) , null );
		chai.assert.notEqual( (await ScopeModel.findOne({where : {id:4}})) , null );
	});

	it('should return 200 and remove trigger (without rule linked | as user )', async function() {
		const res = await chai.request(app)
        	.delete('/trigger/4')
        	.set({ "Authorization": `Bearer ${gamaT}` })
			.send();

		res.should.have.status(200);
		// Vérification BDD :
		// - Trigger :
		chai.assert.notEqual( (await TriggerModel.findOne({where : {id:1}})) , null );
		chai.assert.notEqual( (await TriggerModel.findOne({where : {id:2}})) , null );
		chai.assert.equal( (await TriggerModel.findOne({where : {id:3}})) , null );
		chai.assert.equal( (await TriggerModel.findOne({where : {id:4}})) , null );
		// - Rule & Scope :
		chai.assert.notEqual( (await RuleModel.findOne({where : {id:1}})) , null );
		chai.assert.notEqual( (await RuleModel.findOne({where : {id:2}})) , null );
		chai.assert.notEqual( (await RuleModel.findOne({where : {id:3}})) , null );
		chai.assert.notEqual( (await ScopeModel.findOne({where : {id:1}})) , null );
		chai.assert.notEqual( (await ScopeModel.findOne({where : {id:2}})) , null );
		chai.assert.notEqual( (await ScopeModel.findOne({where : {id:3}})) , null );
		chai.assert.notEqual( (await ScopeModel.findOne({where : {id:4}})) , null );
	});

	it('should return 200 and remove trigger (with rule linked | as admin )', async function() {
		const res = await chai.request(app)
        	.delete('/trigger/1')
        	.set({ "Authorization": `Bearer ${alphaT}` })
			.send();

		res.should.have.status(200);
		// Vérification BDD :
		// - Trigger :
		chai.assert.equal( (await TriggerModel.findOne({where : {id:1}})) , null );
		chai.assert.notEqual( (await TriggerModel.findOne({where : {id:2}})) , null );
		chai.assert.equal( (await TriggerModel.findOne({where : {id:3}})) , null );
		chai.assert.equal( (await TriggerModel.findOne({where : {id:4}})) , null );
		// - Rule & Scope :
		chai.assert.equal( (await RuleModel.findOne({where : {id:1}})) , null );
		chai.assert.equal( (await RuleModel.findOne({where : {id:2}})) , null );
		chai.assert.notEqual( (await RuleModel.findOne({where : {id:3}})) , null );
		chai.assert.equal( (await ScopeModel.findOne({where : {id:1}})) , null );
		chai.assert.equal( (await ScopeModel.findOne({where : {id:2}})) , null );
		chai.assert.equal( (await ScopeModel.findOne({where : {id:3}})) , null );
		chai.assert.notEqual( (await ScopeModel.findOne({where : {id:4}})) , null );
	});

	it('should return 200 and remove trigger (with rule linked | as user )', async function() {
		const res = await chai.request(app)
        	.delete('/trigger/2')
        	.set({ "Authorization": `Bearer ${gamaT}` })
			.send();

		res.should.have.status(200);
		// Vérification BDD :
		// - Trigger :
		chai.assert.equal( (await TriggerModel.findOne({where : {id:1}})) , null );
		chai.assert.equal( (await TriggerModel.findOne({where : {id:2}})) , null );
		chai.assert.equal( (await TriggerModel.findOne({where : {id:3}})) , null );
		chai.assert.equal( (await TriggerModel.findOne({where : {id:4}})) , null );
		// - Rule & Scope :
		chai.assert.equal( (await RuleModel.findOne({where : {id:1}})) , null );
		chai.assert.equal( (await RuleModel.findOne({where : {id:2}})) , null );
		chai.assert.equal( (await RuleModel.findOne({where : {id:3}})) , null );
		chai.assert.equal( (await ScopeModel.findOne({where : {id:1}})) , null );
		chai.assert.equal( (await ScopeModel.findOne({where : {id:2}})) , null );
		chai.assert.equal( (await ScopeModel.findOne({where : {id:3}})) , null );
		chai.assert.equal( (await ScopeModel.findOne({where : {id:4}})) , null );
	});	
});