// Import the dependencies for testing
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../../index');

// Configure chai
chai.use(chaiHttp);
chai.should();
chai.use(require('chai-things'));

// Import model
const { 
	user: UserModel ,
	course : CourseModel ,
	element : ElementModel,
	element_type: ElementTypeModel,
	label: LabelModel,
	trigger: TriggerModel,
	rule: RuleModel,
	rule_parameter: RuleParameterModel,
	scope: ScopeModel,
	filter: FilterModel,
	  waitForSync,  sequelize } = require('../../../db/sequelize');

describe('TRIGGER - POST /trigger', function() {

	let adminT, useraT, userbT;

	const filterTrigger = ({text,has_been_changed}) => ({text,has_been_changed});
	const filterRule = ({constraint_name,constraint_type,trigger_id}) => ({constraint_name,constraint_type,trigger_id});
	const filterRuleParameter = ({name,type,value,rule_id}) => ({name,type,value,rule_id});
	const filterScope = ({mask,scope_order,group_by,rule_id}) => ({mask,scope_order,group_by,rule_id});
	const filterFilter = ({including,label,element_type_id,scope_id}) => ({including,label,element_type_id,scope_id});

	before( /* Attente de la BDD */ async () => await waitForSync );

    after( /* Suprresion des données de test */ async () => await sequelize.sync({ force: true }) );

    before( async function(){
		
        // Création :
		// - User :
		const [ admin, usera, userb ] = await UserModel.bulkCreate([
			{ username: "admin", password: "password", isAdmin: true},
			{ username: "usera", password: "password" },
			{ username: "userb", password: "password" }
		]);

		adminT = admin.generateToken();
		useraT = usera.generateToken();
		userbT = userb.generateToken();

		// - Course :
		const C = await CourseModel.create({ name: "Test", id:1 });

		// - ElementType :
		await ElementTypeModel.bulkCreate([
			{ name: 'PART' , id: 101 },
			{ name: 'TRIGGER' , id: 102 },
			{ name: 'SCOPE' , id: 103 },
		]);

		// - Element :
		await ElementModel.bulkCreate([
			{ id: 201, type: 102},
			{ id: 202, type: 102}
		]);

		// - Label :
		await LabelModel.bulkCreate([
			{ id: 301, name: "aaa"},
			{ id: 302, name: "bbb"}
		]);

		// Liaison Course <=> User (UserRight):
		await C.addUser( usera );

    });

    it('should return status code 401 (not being connected)', async function() {
        const res = await chai
			.request(app)
            .post('/trigger/')
        	.send( { course_id:1, text:"abcdef",file:"pref.json", request : "REQ1"} );
        
		res.should.have.status(401);
    });

    it('should return status code 403 (connected but neither as admin or right granted user))', async function() {
        const res = await chai
			.request(app)
            .post('/trigger/')
            .set({ "Authorization": `Bearer ${userbT}`})
        	.send( { course_id:1, text:"abcdef", rules: [{name:"a",type:"a"}],file:"pref.json", request : "REQ1"} );
        
		res.should.have.status(403);
    });

	it('should return status 406 (missing mandatory field : course_id )', async function() {
        const res = await chai
			.request(app)
            .post('/trigger/')
            .set({ "Authorization": `Bearer ${useraT}`})
        	.send( { text:"abcdef", file:"pref.json", request : "REQ1"} );

        res.should.have.status(406);
		res.body.field.should.equal('course_id');
	});

	it('should return status 406 (missing mandatory field : text )', async function() {
        const res = await chai
			.request(app)
            .post('/trigger/')
            .set({ "Authorization": `Bearer ${useraT}`})
        	.send( { course_id:1,file:"pref.json", request : "REQ1"} );

        res.should.have.status(406);
		res.body.field.should.equal('text');
	});

	it('should return status 406 (missing mandatory field : rules )', async function() {
	     const res = await chai
			.request(app)
	        .post('/trigger/')
	        .set({ "Authorization": `Bearer ${useraT}`})
	        .send( { course_id: 1 , text:"abcdef",file:"pref.json", request : "REQ1"} );

	     res.should.have.status(406);
		 res.body.field.should.equal('rules');
	});

	it('should return status 406 (field rules must have at least 1 field )', async function() {
		const res = await chai
			.request(app)
		    .post('/trigger/')
		    .set({ "Authorization": `Bearer ${useraT}`})
		    .send( { course_id: 1 , text:"abcdef", rules: [],file:"pref.json", request : "REQ1"} );

		res.should.have.status(406);
		res.body.field.should.equal('rules');
	});

	it('should return status 200 and create data (simple)', async function() {
        const res = await chai
			.request(app)
            .post('/trigger/')
            .set({ "Authorization": `Bearer ${useraT}`})
        	.send({ course_id:1, text:"simple", rules : [{name:"a",type:"a"}],file:"pref.json", request : "REQ1"});

        res.should.have.status(200);

		// DB Check : Should create 1 simple data
		const triggers = await TriggerModel.findAll();
		triggers.length.should.equal(1);
		triggers.map(filterTrigger).should.deep.equal([{ text: 'simple', has_been_changed: 0 }]);
		( await RuleModel.findAll() ).length.should.equal(1);
		( await RuleParameterModel.findAll() ).length.should.equal(0);
		( await ScopeModel.findAll() ).length.should.equal(0);
		( await FilterModel.findAll() ).length.should.equal(0);
		// Link
		const course = await CourseModel.findOne( {where : { id: 1 } } );
		(await course.hasTrigger( triggers[0] ) ).should.equal(true, 'Trigger should be link to the course');
	});

	it('should return status 200 and create data (medium)', async function() {
        const res = await chai
			.request(app)
            .post('/trigger/')
            .set({ "Authorization": `Bearer ${adminT}`})
        	.send({ course_id:1, text:"medium", rules:[{name:"a",type:"a"},{name:"b",type:"b"}],file:"pref.json", request : "REQ1"});

        res.should.have.status(200);

		// DB Check : Should create 1 medium data (and the simple data created before)
		const triggers = await TriggerModel.findAll();
		triggers.length.should.equal(2);
		triggers.map(filterTrigger).should.deep.equal([
			{ text: 'simple', has_been_changed: 0 },
			{ text: 'medium', has_been_changed: 0 }
		]);
		const rules = await RuleModel.findAll();
		rules.length.should.equal(3);
		rules.map(filterRule).should.deep.equal([
			{ constraint_name: 'a', constraint_type: 'a', trigger_id: 1 },
			{ constraint_name: 'a', constraint_type: 'a', trigger_id: 2},
			{ constraint_name: 'b', constraint_type: 'b', trigger_id: triggers[1].id }		  
		]);
		( await RuleParameterModel.findAll() ).length.should.equal(0);
		( await ScopeModel.findAll() ).length.should.equal(0);
		( await FilterModel.findAll() ).length.should.equal(0);
		// Link
		const course = await CourseModel.findOne( {where : { id: 1 } } );
		(await course.hasTrigger( triggers[0] )).should.equal(true, 'Trigger should be link to the course');
		(await course.hasTrigger( triggers[1] )).should.equal(true, 'Trigger should be link to the course');
		(await triggers[1].hasRule( rules[0] )).should.equal(false, 'Rule should be link to the trigger');
		(await triggers[1].hasRule( rules[1] )).should.equal(true, 'Rule should be link to the trigger');
	});

	it('should return status 200 and create data (hard)', async function() {
        const res = await chai
			.request(app)
            .post('/trigger/')
            .set({ "Authorization": `Bearer ${useraT}`})
        	.send( TestDataHard );

        res.should.have.status(200);

		// DB Check : Should create 1 hard data (and the data created before)
		// Data
		const triggers = await TriggerModel.findAll();
		triggers.length.should.equal(3);
		triggers.map(filterTrigger).should.deep.equal([
			{ text: 'simple', has_been_changed: 0 },
			{ text: 'medium', has_been_changed: 0 },
			{ text: 'Le TP 1 est après le CM 3', has_been_changed: 0 }
		]);
		const rules = await RuleModel.findAll();
		rules.length.should.equal(4);
		rules.map(filterRule).should.deep.equal([
			{ constraint_name: 'a', constraint_type: 'a', trigger_id: 1 },
			{ constraint_name: 'a', constraint_type: 'a', trigger_id: 2 },
			{ constraint_name: 'b', constraint_type: 'b', trigger_id: 2 },
			{ constraint_name: 'Sequenced', constraint_type: 'Time', trigger_id: 3 }	
		]);
		const rules_parameters = await RuleParameterModel.findAll();
		rules_parameters.length.should.equal(1);
		rules_parameters.map(filterRuleParameter).should.deep.equal([
			{ name: 'x', type: 'y', value: 'z', rule_id: 4 }
		]);
		const scopes = await ScopeModel.findAll();
		scopes.length.should.equal(1);
		scopes.map(filterScope).should.deep.equal([
			{ mask: '3', scope_order: 1, group_by: 103, rule_id: 4 }
		]);
		const filters = await FilterModel.findAll();
		filters.length.should.equal(1);
		filters.map(filterFilter).should.deep.equal([
			{ including: 0, label: 1, element_type_id: 102, scope_id: 1 }
		]);
		// Link
		const course = await CourseModel.findOne( {where : { id: 1 } } );
		(await course.hasTrigger( triggers[0] )).should.equal(true, 'Trigger should be link to the course');
		(await course.hasTrigger( triggers[1] )).should.equal(true, 'Trigger should be link to the course');
		(await course.hasTrigger( triggers[2] )).should.equal(true, 'Trigger should be link to the course');
		(await triggers[1].hasRule( rules[0] )).should.equal(false, 'Rule should be link to the trigger');
		(await triggers[1].hasRule( rules[1] )).should.equal(true, 'Rule should be link to the trigger');
		(await triggers[2].hasRule( rules[2] )).should.equal(false, 'Rule should be link to the trigger');
		(await rules[2].hasParameter( rules_parameters[0] )).should.equal(false, 'Rules parameters should be link to the rules');
		(await rules[2].hasScope( scopes[0] )).should.equal(false, 'Scope should be link to the rules');
		(await scopes[0].hasFilter( filters[0] )).should.equal(true, 'Filter should be link to the scope');

		const {labels_ids,elements_ids,element_type_id} = TestDataHard.rules[0].scope[0].filter[0];
		await Promise.all(labels_ids.map( async label_id => (await filters[0].hasLabel(await LabelModel.findOne({where : {id : label_id }}) )).should.equal(true, "Filter should be linked to label") ) );
		await Promise.all(elements_ids.map( async element_id => (await filters[0].hasElement(await ElementModel.findOne({where : {id : element_id }}) )).should.equal(true, "Filter should be linked to element") ) );
		(await (await ElementTypeModel.findOne({where : {id : element_type_id }})).hasFilter(filters[0])).should.equal(true, "Filter should be link to element type");
		(await (await ElementTypeModel.findOne({where : {id : TestDataHard.rules[0].scope[0].group_by_id }})).hasScope(scopes[0])).should.equal(true, "Scope should be link to element type (group by)")
	});

	it('should return status 404 (hard but with one id incorrect (require data rollback))', async function() {
		let DataHardCrash = TestDataHard;
		DataHardCrash.rules[0].scope[0].filter[0].labels_ids[0] = 666;
        const res = await chai
			.request(app)
            .post('/trigger/')
            .set({ "Authorization": `Bearer ${adminT}`})
        	.send( DataHardCrash );

		res.should.have.status(404);
		res.body.field.should.equal('labels_ids');

		// DB Check : Should stay the same has the request has been cancel and data should have been rolledback
		// Data
		const triggers = await TriggerModel.findAll();
		triggers.length.should.equal(3);
		triggers.map(filterTrigger).should.deep.equal([
			{ text: 'simple', has_been_changed: 0 },
			{ text: 'medium', has_been_changed: 0 },
			{ text: 'Le TP 1 est après le CM 3', has_been_changed: 0 }
		]);
		const rules = await RuleModel.findAll();
		rules.length.should.equal(4);
		rules.map(filterRule).should.deep.equal([
			{ constraint_name: 'a', constraint_type: 'a', trigger_id: 1 },
			{ constraint_name: 'a', constraint_type: 'a', trigger_id: 2 },
			{ constraint_name: 'b', constraint_type: 'b', trigger_id: 2 },
			{ constraint_name: 'Sequenced', constraint_type: 'Time', trigger_id: 3 }	
		]);
		const rules_parameters = await RuleParameterModel.findAll();
		rules_parameters.length.should.equal(1);
		rules_parameters.map(filterRuleParameter).should.deep.equal([
			{ name: 'x', type: 'y', value: 'z', rule_id: 4 }
		]);
		const scopes = await ScopeModel.findAll();
		scopes.length.should.equal(1);
		scopes.map(filterScope).should.deep.equal([
			{ mask: '3', scope_order: 1, group_by: 103, rule_id: 4 }
		]);
		const filters = await FilterModel.findAll();
		filters.length.should.equal(1);
		filters.map(filterFilter).should.deep.equal([
			{ including: 0, label: 1, element_type_id: 102, scope_id: 1 }
		]);
		// Link
		const course = await CourseModel.findOne( {where : { id: 1 } } );
		(await course.hasTrigger( triggers[0] )).should.equal(true, 'Trigger should be link to the course');
		(await course.hasTrigger( triggers[1] )).should.equal(true, 'Trigger should be link to the course');
		(await course.hasTrigger( triggers[2] )).should.equal(true, 'Trigger should be link to the course');
		(await triggers[1].hasRule( rules[0] )).should.equal(false, 'Rule should be link to the trigger');
		(await triggers[1].hasRule( rules[1] )).should.equal(true, 'Rule should be link to the trigger');
		(await triggers[2].hasRule( rules[2] )).should.equal(false, 'Rule should be link to the trigger');
		(await rules[2].hasParameter( rules_parameters[0] )).should.equal(false, 'Rules parameters should be link to the rules');
		(await rules[2].hasScope( scopes[0] )).should.equal(false, 'Scope should be link to the rules');
		(await scopes[0].hasFilter( filters[0] )).should.equal(true, 'Filter should be link to the scope');
	});

	const TestDataHard = 
	{
		"course_id": 1,
		"text": "Le TP 1 est après le CM 3",
		"file" :"pref.json",
		"request" : "REQ1",
		"rules": [
		  {
			"name": "Sequenced",
			"type": "Time",
			"parameter": [
			  {
				"name": "x",
				"type": "y",
				"value": "z"
			  }
			],
			"scope": [
			  {
				"group_by_id": 103,
				"mask": "3",
				"scope_order": 1,
				"filter": [
				  {
					"including": false,
					"label": true,
					"labels_ids": [
					  301,
					  302
					],
					"elements_ids": [
					  201,
					  202
					],
					"element_type_id": 102
				  }
				]
			  }
			]
		  }
		]
	  }

})