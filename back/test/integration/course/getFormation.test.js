// Import the dependencies for testing
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../../index');

// Configure chai
chai.use(chaiHttp);
chai.should();

// Import model
const { user: UserModel ,course_bundle:CourseBundleModel,formation:FormationModel , waitForSync, sequelize } = require('../../../db/sequelize');

describe('COURSE - GET /formation/search', function() {

	let userT,adminT;

	before( function(done) {
		// Mise en attente pour préparation de la BDD
		waitForSync.then( function () { done() } );
	});
	
	before( async function() {
		// Création d'utilisateurs globaux
		let [user, admin] = await UserModel.bulkCreate([
			{ username: "user", password: "password", id:1, isAdmin: false},
			{ username: "admin", password: "password", id:2, isAdmin: true}
		]);
		userT = user.generateToken();
		adminT = admin.generateToken();

		let [L3_info, m1_info, l1_math] = await CourseBundleModel.bulkCreate([
			{id:1,tag:'L3-INFO'},
        	{id:2,tag:'M1-INFO'},
       		{id:3,tag:'L1-MATH'}
		]);
        
		await FormationModel.bulkCreate([
			{course_bundle_id:L3_info.id},
			{course_bundle_id:m1_info.id},
			{course_bundle_id:l1_math.id}
		]);

	});

    after(async function() {
        // Suppression des données de test
        await sequelize.sync({ force: true });
    });

	it('should return 401 (not connected)', async function() {		
        const res = await chai.request(app)
        	.get('/formation/search')
			.send({});

		res.should.have.status(401);
	});

	it('should return 403 (connected but not as admin)', async function() {

		const res = await chai.request(app)
        	.get('/formation/search')
        	.set({ "Authorization": `Bearer ${userT}` });

		res.should.have.status(403);
	});

	it('should return 200 (if not string typed and return all formations)', async function() {

		const res = await chai.request(app)
        	.get('/formation/search')
        	.set({ "Authorization": `Bearer ${adminT}` });
		res.should.have.status(200);
       
        res.body[0].id.should.equal(1);
        res.body[0].name.should.equal('L3-INFO');
       
        res.body[1].id.should.equal(2);
        res.body[1].name.should.equal('M1-INFO');
        res.body[2].id.should.equal(3);
        res.body[2].name.should.equal('L1-MATH');
		
	});

	it('should return 200 (if a formation contains the string typed)', async function() {

		const res = await chai.request(app)
            .get('/formation/search?name=INFO')   
        	.set({ "Authorization": `Bearer ${adminT}` });
		res.should.have.status(200)
        res.body[0].id.should.equal(1);
        res.body[0].name.should.equal('L3-INFO');
        res.body[1].id.should.equal(2);
        res.body[1].name.should.equal('M1-INFO');
    

	});
    it('should return 200 (if any formation contains the string typed but return any formation)', async function() {

		const res = await chai.request(app)
            .get('/formation/search?name=aaa')   
        	.set({ "Authorization": `Bearer ${adminT}` });
        res.body.should.be.empty     
		res.should.have.status(200)   

	});


		
});