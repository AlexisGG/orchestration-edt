// Import the dependencies for testing
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../../index');

// Configure chai
chai.use(chaiHttp);
chai.should();

// Import model
const { user: UserModel, course: CourseModel, course_bundle: CourseBundleModel, formation: FormationModel, waitForSync, sequelize } = require('../../../db/sequelize');

describe('COURSE - GET /course/search', function() {

	let user,admin;
	let userT,adminT;

	before( /* Attente de la BDD */ async () => await waitForSync );

	before( async function() {
		// Création d'un grand jeu de donnée
		let licence_course =
		CourseModel.bulkCreate([
			{ id: 1001, name : "Algorithmique 1" },
			{ id: 1002, name : "Théorie des langages" },
			{ id: 1003, name : "Bases d'informatique" },
			{ id: 1004, name : "Développement web" },
			{ id: 1005, name : "Fondements de l'informatique" }
		]);
		let master_course =
		CourseModel.bulkCreate([
			{ id: 2001, name : "Réseau" },
			{ id: 2002, name : "Optimisation linéaire" },
			{ id: 2003, name : "Optimisation combinatoire" }
		]);
		let licence_course_bundle = CourseBundleModel.create( { id: 101, tag : "Licence", nr_weeks: 12, days_per_week: 5} );
		let master_course_bundle = CourseBundleModel.create( { id: 102, tag : "Master", nr_weeks: 12, days_per_week: 5} );

		// On attend toutes les données soit créé en même temps
		//et on les génères malgré tout séparèment pour ne pas avoirs à faire de query
		[licence_course,master_course,licence_course_bundle,master_course_bundle] = await Promise.all([ licence_course, master_course, licence_course_bundle, master_course_bundle ]);

		await FormationModel.create( { course_bundle_id: 102 } );
		await FormationModel.create( { course_bundle_id: 101 } );

		// On relie les cours à leurs regroupements
		return Promise.all( [
			... licence_course.map( course => course.addCourseBundle(licence_course_bundle) ),
			... master_course.map( course => course.addCourseBundle(master_course_bundle) )
		]);
	});

	before( async function() {
		// Création d'utilisateurs globaux
		user = await UserModel.create({ username: "eta", password: "eta", id:101, isAdmin: false});
		admin = await UserModel.create({ username: "zeta", password: "zeta", id:102, isAdmin: true});
		userT = user.generateToken();
		adminT = admin.generateToken();
	});

	after(async function() {
		// Suppression des données de test
		await sequelize.sync({ force: true });
	});

	it('should return 401 (not connected)', async function() {
		const res = await chai.request(app)
			.get('/course/search')
			.send({});

		res.should.have.status(401);
	});

	it('should return 403 (connected but not as admin)', async function() {
		const res = await chai.request(app)
			.get('/course/search')
			.set({ "Authorization": `Bearer ${userT}` });

		res.should.have.status(403);
	});

	it('should return 200, with all data (no param)', async function() {
		const res = await chai.request(app)
			.get('/course/search')
			.set({ "Authorization": `Bearer ${adminT}` });

		res.should.have.status(200);

		chai.assert.deepEqual(
			res.body,
			[
				{ id: 1001, name: 'Algorithmique 1' },
				{ id: 1002, name: 'Théorie des langages' },
				{ id: 1003, name: "Bases d'informatique" },
				{ id: 1004, name: 'Développement web' },
				{ id: 1005, name: "Fondements de l'informatique" },
				{ id: 2001, name: 'Réseau' },
				{ id: 2002, name: 'Optimisation linéaire' },
				{ id: 2003, name: 'Optimisation combinatoire' }
			],
			'The full database should be returned'
		);
	});

	it('should return 200, with some data (name param)[1]', async function() {
		const res = await chai.request(app)
			.get('/course/search')
			.set({ "Authorization": `Bearer ${adminT}` })
			.query({name:'OPT'});

		res.should.have.status(200);

		chai.assert.deepEqual(
			res.body,
			[
				{ id: 2002, name: 'Optimisation linéaire' },
				{ id: 2003, name: 'Optimisation combinatoire' }
			],
			'Only the raw containing opt should be returned'
		);
	});

	it('should return 200, with some data (name param)[2]', async function() {
		const res = await chai.request(app)
			.get('/course/search')
			.set({ "Authorization": `Bearer ${adminT}` })
			.query({name:'cT'});

		res.should.have.status(200);

		res.body.length.should.equal(0);
	});

	it('should return 200, with some data (bundle course id param)[1]', async function() {

		const res = await chai.request(app)
			.get('/course/search')
			.set({ "Authorization": `Bearer ${adminT}` })
			.query({formation_id:101});

		res.should.have.status(200);

		chai.assert.deepEqual(
			res.body,
			[
				{ id: 1001, name: 'Algorithmique 1' },
				{ id: 1002, name: 'Théorie des langages' },
				{ id: 1003, name: "Bases d'informatique" },
				{ id: 1004, name: 'Développement web' },
				{ id: 1005, name: "Fondements de l'informatique" }
			],
			'It should return only a part of the database'
		);
	});

	it('should return 200, with some data (bundle course id param)[2]', async function() {
		const res = await chai.request(app)
			.get('/course/search')
			.set({ "Authorization": `Bearer ${adminT}` })
			.query({formation_id:42});

		res.should.have.status(200);

		res.body.length.should.equal(0);
	});

	it('should return 200, with some data (both param)[1]', async function() {
		const res = await chai.request(app)
			.get('/course/search')
			.set({ "Authorization": `Bearer ${adminT}` })
			.query({formation_id:101,name:'ti'});

		res.should.have.status(200);

		chai.assert.deepEqual(
			res.body,
			[
				{ id: 1003, name: "Bases d'informatique" },
				{ id: 1005, name: "Fondements de l'informatique" }
			],
			'It should return only a part of the database'
		);
	});

	it('should return 200, with some data (both param)[2]', async function() {
		const res = await chai.request(app)
			.get('/course/search')
			.set({ "Authorization": `Bearer ${adminT}` })
			.query({formation_id:101,name:'zzzz'});

		res.should.have.status(200);

		res.body.length.should.equal(0);
	});

});
