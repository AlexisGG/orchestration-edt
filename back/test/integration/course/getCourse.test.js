// Import the dependencies for testing
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../../index');

// Configure chai
chai.use(chaiHttp);
chai.should();
chai.use(require('chai-things'));

// Import model
const { 
    user: UserModel ,
    course : CourseModel ,
    course_bundle : CourseBundleModel,
    formation : FormationModel,
    part : PartModel,
    part_type : PartTypeModel,
    room : RoomModel,
    room_allocation : RoomAllocationModel,
    person : PersonModel,
    class : ClassModel,
    service : ServiceModel,
    waitForSync, sequelize } = require('../../../db/sequelize');

describe('COURSE - GET /course/{course_id}', function() {
    // Utlisateurs
    var garcia, admin;
    // Course
    var bdd;
    // Formation
    var L3;
    // Part_Type
    var type_CM, type_TP;
    // PART
    var TP, CM;
    // Room
    var AMPHI_A, H001, H002;
    // Person
    var Touria,Laurent;
    // Classe
    var CM1, TP1, TP2;

    before(async function() {
        // Creation des données requises pour faire les test
        await waitForSync
        // Création des utilisateurs
        admin = await UserModel.create({ username: "admin", password: "password", isAdmin: true});
        garcia = await UserModel.create({ username: "garcia", password: "password"});
        // Création des course bundle
        L3 = await CourseBundleModel.create({tag:"L3"});
        // Création des formations
        await FormationModel.create({course_bundle_id: L3.id});
        // Création des course
        bdd = await CourseModel.create({name:"bdd" });
        // Course <-> CourseBundle
        await bdd.addCourseBundle(L3);
        // User <-> Course
        await garcia.addCourse(bdd);
        // Création des partType
        type_CM = await PartTypeModel.create({name:"CM"});
        type_TP = await PartTypeModel.create({name:"TP"});
        // Création des part
        CM = await PartModel.create({nr_sessions:8,nr_teachers:1,session_length:1,single_room:1});
        TP = await PartModel.create({nr_sessions:7,nr_teachers:1,session_length:2,single_room:1});
        await TP.setCourse(bdd);
        await CM.setCourse(bdd);
        await TP.setType(type_TP);
        await CM.setType(type_CM);
        // Création des rooms
        AMPHI_A = await RoomModel.create({name: "AMPHI-A" , capacity : 90});
        H001 = await RoomModel.create({name: "H001" , capacity : 20});
        H002 = await RoomModel.create({name: "H002" , capacity : 42});
        // Create room_allocation
        await RoomAllocationModel.create({mandatory : false, part_id : CM.id, room_id : AMPHI_A.id});
        await RoomAllocationModel.create({mandatory : false, part_id : TP.id, room_id : H001.id});
        await RoomAllocationModel.create({mandatory : false, part_id : TP.id, room_id : H002.id});
        // Création des teachers
        Touria = await PersonModel.create({name : "AIT EL MEKKI Touria" , teacher : true});
        Laurent = await PersonModel.create({name : "GARCIA Laurent" , teacher : true});
        // Service
        await ServiceModel.create({nr_sessions : 8, part_id : CM.id, teacher_id : Touria.id});
        await ServiceModel.create({nr_sessions : 14, part_id : TP.id, teacher_id : Touria.id});
        await ServiceModel.create({nr_sessions : 7, part_id : TP.id, teacher_id : Laurent.id});
        // Class
        CM1 = await ClassModel.create({name: "Base de données 2-CM1", part_id : CM.id});
        TP1 = await ClassModel.create({name: "Base de données 2-TP1", part_id : TP.id});
        TP2 = await ClassModel.create({name: "Base de données 2-TP2", part_id : TP.id});
    });

    after(async function() {
        // Suppression des données de test
        await sequelize.sync({ force: true });
    });

    it('should return status 403 if user havnt rights on this course', async function() {
		const token = garcia.generateToken();
        const prog = await CourseModel.create({name:"programmation" });
        const res = await chai.request(app)
        .get(`/course/${prog.id}`)
        .set({ "Authorization": `Bearer ${token}`});
        res.should.have.status(403);
    });

    it('should return status 404 if course doesnt exist', async function() {
		const token = admin.generateToken();
        const res = await chai.request(app)
        .get(`/course/999`)
        .set({ "Authorization": `Bearer ${token}`});
        res.should.have.status(404);
    });

    it('should return status 200 and all data', async function() {
		const token = garcia.generateToken();
        const res = await chai.request(app)
        .get(`/course/${bdd.id}`)
        .set({ "Authorization": `Bearer ${token}`});
    
        res.should.have.status(200);
        // Course data
        res.body.id.should.be.equal(bdd.id);
        res.body.name.should.be.equal(bdd.name);
        res.body.formation.should.be.eql({ id : L3.id, name : L3.tag});

        // Part CM
        res.body.parts.should.include.something.that.deep.equals({
            id : CM.id,
            nr_sessions : CM.nr_sessions,
            nr_teachers : CM.nr_teachers,
            max_headcount : null,
            session_length : CM.session_length,
            single_room : CM.single_room,
            starting_slots : null,
            course_id : L3.id,
            element_id : null,
            type : type_CM.name,
            rooms : [
                {
                    id : AMPHI_A.id,
                    name : AMPHI_A.name,
                    capacity : AMPHI_A.capacity,
                    element_id : null,
                    mandatory : 0
                }
            ],
            teacher : [
                {
                    id: Touria.id,
                    teacher: 1,
                    name: Touria.name,
                    element_id: null,
                    nr_sessions: 8
                  }
            ],
            classes : [
                { id: CM1.id, name: CM1.name, element_id: null }
            ]
        });

        // Part TP
        res.body.parts.should.include.something.that.deep.equals({
            id : TP.id,
            nr_sessions : TP.nr_sessions,
            nr_teachers : TP.nr_teachers,
            max_headcount : null,
            session_length : TP.session_length,
            single_room : TP.single_room,
            starting_slots : null,
            course_id : L3.id,
            element_id : null,
            type : type_TP.name,
            rooms : [
                {
                    id : H001.id,
                    name : H001.name,
                    capacity : H001.capacity,
                    element_id : null,
                    mandatory : 0
                },
                {
                    id : H002.id,
                    name : H002.name,
                    capacity : H002.capacity,
                    element_id : null,
                    mandatory : 0
                }
            ],
            teacher : [
                {
                    id: Touria.id,
                    teacher: 1,
                    name: Touria.name,
                    element_id: null,
                    nr_sessions: 14
                },
                {
                    id: Laurent.id,
                    teacher: 1,
                    name: Laurent.name,
                    element_id: null,
                    nr_sessions: 7
                  }
            ],
            classes : [
                { id: TP1.id, name: TP1.name, element_id: null },
                { id: TP2.id, name: TP2.name, element_id: null }
            ]
        });
    });
});