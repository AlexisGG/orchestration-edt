// Import the dependencies for testing
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../../index');

// Configure chai
chai.use(chaiHttp);
chai.should();

// Import model
const { user: UserModel, course: CourseModel, course_bundle: CourseBundleModel, formation: FormationModel, waitForSync, sequelize } = require('../../../db/sequelize');

describe('COURSE - ADVANCED GET /course/search', function() {

	let adminToken;

	let test_data, test_course, test_bundle;

	before( /* Attente de la BDD */ async () => await waitForSync );

	before( async function() { // Création d'un grand jeu de donnée
		// - Création de la liste des nodes
		test_data = [4,3,3,2,1,4,11,9].map( (node_count,index) =>
			[...new Array(node_count)].map( (_,i) => { let v = (100*(index))+i+1;return { id: v ,name : v.toString().padStart(3,'0') } } )
		);
		// - Création des bundles pour test (et d'un Course par CourseBundle)
		let test_course = await Promise.all( test_data.map( data => CourseModel.bulkCreate( data ) ) );
		let test_bundle = await Promise.all( test_data.map( data => CourseBundleModel.bulkCreate( data.map(
			element => ({id:element.id,tag:element.name,nr_weeks:12,days_per_week:5})
		) ) ) ) ;

		// - Création des formations (points d'entrées) :
		await Promise.all(
			[ 001,  101,  201,  301,  401,  501,  601,605,608,  701,704,707 ].map(
				course_bundle_id => FormationModel.create( {course_bundle_id} )
			)
		);

		// - Création des liens entre Course et CourseBundle
		let AttenteDeCreationLien = [];
		for(let i in test_course) {
			for(let j in test_course[i]) {
				AttenteDeCreationLien.push( test_course[i][j].addCourseBundle( test_bundle[i][j] ) );
			}
		}
		await Promise.all( AttenteDeCreationLien );

		// - Création des liens entre CourseBundle
		let fils_parent = [
			// Cycle de taille 4
			[{pere: 0,fils: 1},{pere: 1,fils: 2},{pere: 2,fils: 3},{pere: 3,fils: 0}],
			// Graphe pouvant être vu comme un cycle mais ne l'est pas !
			[{pere: 0,fils: 1},{pere: 0,fils: 2},{pere: 1,fils: 2}],
			// Cycle de taille 3
			[{pere: 0,fils: 1},{pere: 1,fils: 2},{pere: 2,fils: 0}],
			// Cycle de taille 2
			[{pere: 0,fils: 1},{pere: 1,fils: 0}],
			// Cycle de taille 1
			[{pere: 0,fils: 0}],
			// Graphe pouvant être vu comme un cycle mais ne l'est pas !
			[{pere: 0,fils: 1},{pere: 0,fils: 2},{pere: 1,fils: 3},{pere: 2,fils: 3}],
			// Graphe complexe sans cycle !
			[{pere: 0,fils: 1},{pere: 0,fils:10},{pere: 1,fils: 3},{pere: 2,fils: 3},{pere: 4,fils:10},{pere: 7,fils: 8},{pere: 8,fils: 6},{pere: 8,fils: 9}
			,{pere:10,fils: 3},{pere:10,fils: 5},{pere:10,fils: 6},{pere: 0,fils: 2}],
			// Graphe complexe avec cycle !
			[{pere: 0,fils: 1},{pere: 1,fils: 4},{pere: 1,fils: 2},{pere: 3,fils: 4},{pere: 4,fils: 5},{pere: 5,fils: 6},{pere: 6,fils: 7},{pere: 7,fils: 8}
			,{pere: 8,fils: 0}]
		];

		return Promise.all( fils_parent.map( (liste,graph_number) =>
			Promise.all( liste.map( 
				pair => test_bundle[graph_number][ pair.pere ].addChild( test_bundle[graph_number][ pair.fils ] ) 
			) )
		) )
	});

	before( async function() {
		// Création d'utilisateurs globaux
		let admin = await UserModel.create({ username: "zeta", password: "zeta", id:102, isAdmin: true});
		adminToken = admin.generateToken();
	});

	after(async function() {
		// Suppression des données de test
		await sequelize.sync({ force: true });
	});

	it('should return 206, with some data (Graph #0,Entry : 1)', async function() {
		const res = await chai.request(app)
			.get('/course/search')
			.set({ "Authorization": `Bearer ${adminToken}` })
			.query({formation_id:001});

		res.should.have.status(206);
	});

	it('should return 200, with some data (Graph #1,Entry : 1)', async function() {
		const res = await chai.request(app)
			.get('/course/search')
			.set({ "Authorization": `Bearer ${adminToken}` })
			.query({formation_id:101});

		res.should.have.status(200);

		chai.assert.deepEqual(
			res.body,
			[
				{ id: 101, name: '101' },
				{ id: 102, name: '102' },
				{ id: 103, name: '103' }
			]
		);
	});

	it('should return 206, with some data (Graph #2,Entry : 1)', async function() {
		const res = await chai.request(app)
			.get('/course/search')
			.set({ "Authorization": `Bearer ${adminToken}` })
			.query({formation_id:201});

		res.should.have.status(206);
	});

	it('should return 206, with some data (Graph #3,Entry : 1)', async function() {
		const res = await chai.request(app)
			.get('/course/search')
			.set({ "Authorization": `Bearer ${adminToken}` })
			.query({formation_id:301});

		res.should.have.status(206);
	});

	it('should return 206, with some data (Graph #4,Entry : 1)', async function() {
		const res = await chai.request(app)
			.get('/course/search')
			.set({ "Authorization": `Bearer ${adminToken}` })
			.query({formation_id:401});

		res.should.have.status(206);
	});

	it('should return 200, with some data (Graph #5,Entry : 1)', async function() {
		const res = await chai.request(app)
			.get('/course/search')
			.set({ "Authorization": `Bearer ${adminToken}` })
			.query({formation_id:501});

		res.should.have.status(200);

		chai.assert.deepEqual(
			res.body,
			[
				{ id: 501, name: '501' },
				{ id: 502, name: '502' },
				{ id: 503, name: '503' },
				{ id: 504, name: '504' }
			]
		);
		
	});

	it('should return 200, with some data (Graph #6,Entry : 1)', async function() {
		const res = await chai.request(app)
			.get('/course/search')
			.set({ "Authorization": `Bearer ${adminToken}` })
			.query({formation_id:601});

		res.should.have.status(200);

		chai.assert.deepEqual(
			res.body,			  
			[
				{ id: 601, name: '601' },
				{ id: 602, name: '602' },
				{ id: 603, name: '603' },
				{ id: 604, name: '604' },
				{ id: 606, name: '606' },
				{ id: 607, name: '607' },
				{ id: 611, name: '611' }
			]
		);
	});

	it('should return 200, with some data (Graph #6,Entry : 2)', async function() {
		const res = await chai.request(app)
			.get('/course/search')
			.set({ "Authorization": `Bearer ${adminToken}` })
			.query({formation_id:605});

		res.should.have.status(200);

		chai.assert.deepEqual(
			res.body,
			[
				{ id: 604, name: '604' },
				{ id: 605, name: '605' },
				{ id: 606, name: '606' },
				{ id: 607, name: '607' },
				{ id: 611, name: '611' }
			]		
		);
	});

	it('should return 200, with some data (Graph #6,Entry : 3)', async function() {
		const res = await chai.request(app)
			.get('/course/search')
			.set({ "Authorization": `Bearer ${adminToken}` })
			.query({formation_id:608});

		res.should.have.status(200);

		chai.assert.deepEqual(
			res.body,
			[
				{ id: 607, name: '607' },
				{ id: 608, name: '608' },
				{ id: 609, name: '609' },
				{ id: 610, name: '610' }
			]		  
		);
	});

	it('should return 206, with some data (Graph #7,Entry : 1)', async function() {
		const res = await chai.request(app)
			.get('/course/search')
			.set({ "Authorization": `Bearer ${adminToken}` })
			.query({formation_id:701});

		res.should.have.status(206);
	});

	it('should return 206, with some data (Graph #7,Entry : 2)', async function() {
		const res = await chai.request(app)
			.get('/course/search')
			.set({ "Authorization": `Bearer ${adminToken}` })
			.query({formation_id:704});

		res.should.have.status(206);
	});

	it('should return 206, with some data (Graph #7,Entry : 3)', async function() {
		const res = await chai.request(app)
			.get('/course/search')
			.set({ "Authorization": `Bearer ${adminToken}` })
			.query({formation_id:707});

		res.should.have.status(206);
	});

});
