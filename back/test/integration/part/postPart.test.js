// Import the dependencies for testing
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../../index');

// Configure chai
chai.use(chaiHttp);
chai.should();
chai.use(require('chai-things'));

// Import model
const { 
	user: UserModel ,
	course : CourseModel ,
	part: PartModel,
	element_type: ElementTypeModel,
	part_type: PartTypeModel,
	  waitForSync,  sequelize } = require('../../../db/sequelize');

describe('PART - POST /part', function() {

	let adminT, useraT, userbT;

	const filterField = ({nr_sessions,session_length,nr_teachers,single_room}) => ({nr_sessions,session_length,nr_teachers,single_room});

	before( /* Attente de la BDD */ async () => await waitForSync );

    after( /* Suprresion des données de test */ async () => await sequelize.sync({ force: true }) );

    before( async function(){
		
        // Création :
		// - User :
		const [ admin, usera, userb ] = await UserModel.bulkCreate([
			{ username: "admin", password: "password", isAdmin: true},
			{ username: "usera", password: "password" },
			{ username: "userb", password: "password" }
		]);

		adminT = admin.generateToken();
		useraT = usera.generateToken();
		userbT = userb.generateToken();

		// - Course :
		const C = await CourseModel.create({ name: "Test", id:1 });

		// - ElementType :
		await ElementTypeModel.create({ name: 'PART' });

		// - PartTypeModel :
		await PartTypeModel.bulkCreate([{ id: 2},{id: 6}]);

		// Liaison :
		await C.addUser( usera );

    });

    it('should return status code 401 (not being connected)', async function() {
        const res = await chai
			.request(app)
            .post('/part/1')
        	.send({ part_type_id:6, nr_sessions:1, session_length:2, nr_teachers:4, single_room: false});
        
		res.should.have.status(401);
    });

    it('should return status code 404 (course does not exist)', async function() {
        const res = await chai
			.request(app)
            .post('/part/99')
            .set({ "Authorization": `Bearer ${adminT}`})
        	.send({ part_type_id:6, nr_sessions:1, session_length:2, nr_teachers:4, single_room: false});
        
		res.should.have.status(404);
    });

    it('should return status code 403 (connected but neither as admin or right granted user)', async function() {
        const res = await chai
			.request(app)
            .post('/part/1')
            .set({ "Authorization": `Bearer ${userbT}`})
        	.send({ part_type_id:6, nr_sessions:1, session_length:2, nr_teachers:4, single_room: false});

        res.should.have.status(403);
    });

	it('should return status 406 (missing field)', async function() {
        const res = await chai
			.request(app)
            .post('/part/1')
            .set({ "Authorization": `Bearer ${useraT}`})
        	.send({ part_type_id:6, nr_sessions:1, nr_teachers:4, single_room: false});

        res.should.have.status(406);
		res.body.field.should.equal('session_length');
	});

	it('should return status 200 and create data (as admin)', async function() {
        const res = await chai
			.request(app)
            .post('/part/1')
            .set({ "Authorization": `Bearer ${useraT}`})
        	.send({ part_type_id:6, nr_sessions:1, session_length:2, nr_teachers:4, single_room: false});

        res.should.have.status(200);

		let data = (await PartModel.findAll()).map(filterField);
		chai.assert.deepEqual( data , [
			{ nr_sessions:1, session_length:2, nr_teachers:4, single_room: 0}
		] );
	});

	it('should return status 200 and create data (as user with right on course)', async function() {
        const res = await chai
			.request(app)
            .post('/part/1')
            .set({ "Authorization": `Bearer ${adminT}`})
        	.send({ part_type_id:2, nr_sessions:3, session_length:7, nr_teachers:2, single_room: false});

        res.should.have.status(200);

		let data = (await PartModel.findAll()).map(filterField);
		chai.assert.deepEqual( data , [
			{ nr_sessions:1, session_length:2, nr_teachers:4, single_room: 0},
			{ nr_sessions:3, session_length:7, nr_teachers:2, single_room: 0}
		] );
	});

})