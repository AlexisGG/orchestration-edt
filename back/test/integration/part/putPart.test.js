// Import the dependencies for testing
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../../index');

// Configure chai
chai.use(chaiHttp);
chai.should();

// Import model
const { waitForSync,
	user: UserModel,
	course: CourseModel,
	part: PartModel,
	trigger: TriggerModel,
	rule: RuleModel,
	scope: ScopeModel,
	sequelize } = require('../../../db/sequelize');

describe('PART - PUT /part/time_division/{course_id}', function() {
	
	// Donnée global :
	let alphaT, betaT, gamaT;

	// Attente chargement BDD :
	before( async () => await waitForSync );

	// A la fin purge des données :
	after( async () => await sequelize.sync({ force: true }) );

	// Création des jeu de données pour les test:
	before( async function() {
		// Création des comptes utilisateurs
		const [ alpha, beta, gama ] = await UserModel.bulkCreate([
			{ username: "alpha", password: "alpha", isAdmin: true },
			{ username: "beta", password: "beta" , isAdmin: false},
			{ username: "gama", password: "gama" , isAdmin: false}
		]);
		alphaT = alpha.generateToken();
		betaT = beta.generateToken();
		gamaT = gama.generateToken();

		// Création des Course, Part, Trigger, Rule, Scope
		const [ Ca, Cb , Cc ] = await CourseModel.bulkCreate([
			{id:101,name:"algo1"},
			{id:102,name:"web1"},
			{id:103,name:"assembleur"}
		]);

		const [ Ta, Tb, Tc ] = await TriggerModel.bulkCreate([
			{text:"Ich bin ein kartofle",id:1, file:"pref.json", request : "REQ1"},
			{text:"Soy una papa",id:2, file:"pref.json", request : "REQ1"},
			{text:"Je suis une patate",id:3, file:"pref.json", request : "REQ1"}
		]);

		const [ Ra, Rb, Rc] = await RuleModel.bulkCreate([
			{id:1},
			{id:2},
			{id:3}
		]);

		const [ Sa, Sb, Sc , Sd] = await ScopeModel.bulkCreate([
			({mask:'1', scope_order:1, id:1}),
			({mask:'1-3', scope_order:2, id:2}),
			({mask:'1,3-5', scope_order:3, id:3}),
			({mask:'1-9', scope_order:4, id:4})
		]);

		const [ Pa, Pb ] = await PartModel.bulkCreate([
			{id:1,nr_sessions:6,session_length:2},
			{id:2,nr_sessions:4,session_length:1}
		]);

		// Liaisons des éléments :
		await Promise.all([
			Ca.addPart(Pa),
			Cb.addPart(Pb),
			Ca.addTrigger(Ta),
			Ca.addTrigger(Tb),
			Cc.addTrigger(Tc),
			Ta.addRule(Ra),
			Ta.addRule(Rb),
			Tc.addRule(Rc),
			Ra.addScope(Sa),
			Ra.addScope(Sb),
			Rb.addScope(Sc),
			Rc.addScope(Sd)
		]);

		// Droit sur cour
		await Promise.all([
			gama.addCourse( Ca ),
			gama.addCourse( Cb )
		]);
	});

	// Test Partie 1 : Non connecté
	it('Should return 401 : Not connected ', async function() {
		const res = await chai
			.request(app)
            .put('/part/time_division/1')
        	.send({session_length:3,nr_sessions:3});
		
		res.should.have.status(401);
	});

	// Test Partie 2 : Donnée(s) non fournie(s) / manquante(s) 
	it('Should return 404 : Missing url param : partID', async function() {
		const res = await chai
			.request(app)
            .put('/part/time_division/')
        	.set({ "Authorization": `Bearer ${alphaT}`})
			.send();

		res.should.have.status(404);
	});

	it('Should return 406 : Missing query param : nr_sessions & session_length', async function() {
		const res = await chai
			.request(app)
            .put('/part/time_division/1')
        	.set({ "Authorization": `Bearer ${alphaT}`})
			.send({});

		res.should.have.status(406);
		res.body.field.should.satisfy( (field) => field == 'nr_sessions' || field == 'session_length' );
	});

	it('Should return 406 : Missing query param : nr_sessions', async function() {
		const res = await chai
			.request(app)
            .put('/part/time_division/1')
        	.set({ "Authorization": `Bearer ${alphaT}`})
			.send({session_length:2});
			
		res.should.have.status(406);
		res.body.field.should.equal('nr_sessions');
	});

	it('Should return 406 : Missing query param : session_length', async function() {
		const res = await chai
			.request(app)
            .put('/part/time_division/1')
        	.set({ "Authorization": `Bearer ${alphaT}`})
			.send({nr_sessions:2});
			
		res.should.have.status(406);
		res.body.field.should.equal('session_length');
	});

	// Test Partie 3 : Donnée(s) érroné(s)
	it('Should return 404 : No corresponding part with the given id', async function() {
		const res = await chai
			.request(app)
            .put('/part/time_division/99')
        	.set({ "Authorization": `Bearer ${alphaT}`})
			.send({session_length:3,nr_sessions:3});

		res.should.have.status(404);
	});

	it('Should return 409 : The total amount of hour does not stay constant', async function() {
		const res = await chai
			.request(app)
            .put('/part/time_division/1')
        	.set({ "Authorization": `Bearer ${alphaT}`})
			.send({nr_sessions:1,session_length:1});

		
		res.should.have.status(409);
	});

	it('Should return 403 : Right refused on this course', async function() {
		const res = await chai
			.request(app)
            .put('/part/time_division/1')
        	.set({ "Authorization": `Bearer ${betaT}`})
			.send({nr_sessions:2,session_length:6});
			
		res.should.have.status(403);
	});

	// Test Partie 4 : Changement simple ( Sans scope )
	it('Should return 200 : Simple change with admin rights', async function() {
		const res = await chai
			.request(app)
			.put('/part/time_division/2')
        	.set({ "Authorization": `Bearer ${alphaT}`})
			.send({nr_sessions:2,session_length:2});

		res.should.have.status(200);

		let result = await PartModel.findOne({where:{id:2}});
		result.nr_sessions.should.equal(2);
		result.session_length.should.equal(2);
	});
	
	it('Should return 200 : Simple change with user granted rights', async function() {
		const res = await chai
			.request(app)
			.put('/part/time_division/2')
        	.set({ "Authorization": `Bearer ${gamaT}`})
			.send({nr_sessions:1,session_length:4});

		res.should.have.status(200);

		let result = await PartModel.findOne({where:{id:2}});
		result.nr_sessions.should.equal(1);
		result.session_length.should.equal(4);
	});

	// Test Partie 5 : Changement complexe ( Avec scope )
	it('Should return 200 : Complex change [12;1]', async function() {
		const res = await chai
			.request(app)
			.put('/part/time_division/1')
        	.set({ "Authorization": `Bearer ${alphaT}`})
			.send({nr_sessions:12,session_length:1});

		res.should.have.status(200);

		let trigger = await TriggerModel.findByPk(1);
		trigger.has_been_changed.should.equal(1);

		trigger = await TriggerModel.findByPk(2);
		trigger.has_been_changed.should.equal(0);

		trigger = await TriggerModel.findByPk(3);
		trigger.has_been_changed.should.equal(0);

		let result = await PartModel.findOne({where:{id:1}});
		result.nr_sessions.should.equal(12);
		result.session_length.should.equal(1);

		const t1 = await ScopeModel.findOne({where:{id:1}});
		t1.scope_order.should.equal(1);
		t1.mask.should.equal('1-2');
		const t2 = await ScopeModel.findOne({where:{id:2}});
		t2.scope_order.should.equal(2);
		t2.mask.should.equal('1-6');
		const t3 = await ScopeModel.findOne({where:{id:3}});
		t3.scope_order.should.equal(3);
		t3.mask.should.equal('1-2,5-10');
		const t4 = await ScopeModel.findOne({where:{id:4}});
		t4.scope_order.should.equal(4);
		t4.mask.should.equal('1-9');
	});

	it('Should return 200 : Complex change [4;3]', async function() {
		const res = await chai
			.request(app)
			.put('/part/time_division/1')
        	.set({ "Authorization": `Bearer ${alphaT}`})
			.send({nr_sessions:4,session_length:3});

		res.should.have.status(200);

		let trigger = await TriggerModel.findByPk(2);
		trigger.has_been_changed.should.equal(0);

		trigger = await TriggerModel.findByPk(3);
		trigger.has_been_changed.should.equal(0);
		
		let result = await PartModel.findOne({where:{id:1}});
		result.nr_sessions.should.equal(4);
		result.session_length.should.equal(3);

		const t1 = await ScopeModel.findOne({where:{id:1}});
		t1.scope_order.should.equal(1);
		t1.mask.should.equal('1');
		const t2 = await ScopeModel.findOne({where:{id:2}});
		t2.scope_order.should.equal(2);
		t2.mask.should.equal('1-2');
		const t3 = await ScopeModel.findOne({where:{id:3}});
		t3.scope_order.should.equal(3);
		t3.mask.should.equal('1-4');
		const t4 = await ScopeModel.findOne({where:{id:4}});
		t4.scope_order.should.equal(4);
		t4.mask.should.equal('1-9');
	});

	it('Should return 200 : Complex change [3;4]', async function() {
		const res = await chai
			.request(app)
			.put('/part/time_division/1')
        	.set({ "Authorization": `Bearer ${alphaT}`})
			.send({nr_sessions:3,session_length:4});

		res.should.have.status(200);

		let trigger = await TriggerModel.findByPk(2);
		trigger.has_been_changed.should.equal(0);

		trigger = await TriggerModel.findByPk(3);
		trigger.has_been_changed.should.equal(0);
		
		let result = await PartModel.findOne({where:{id:1}});
		result.nr_sessions.should.equal(3);
		result.session_length.should.equal(4);

		const t1 = await ScopeModel.findOne({where:{id:1}});
		t1.scope_order.should.equal(1);
		t1.mask.should.equal('1');
		const t2 = await ScopeModel.findOne({where:{id:2}});
		t2.scope_order.should.equal(2);
		t2.mask.should.equal('1-2');
		const t3 = await ScopeModel.findOne({where:{id:3}});
		t3.scope_order.should.equal(3);
		t3.mask.should.equal('1-3');
		const t4 = await ScopeModel.findOne({where:{id:4}});
		t4.scope_order.should.equal(4);
		t4.mask.should.equal('1-9');
	});

	it('Should return 200 : Complex change [6;2]', async function() {
		const res = await chai
			.request(app)
			.put('/part/time_division/1')
        	.set({ "Authorization": `Bearer ${alphaT}`})
			.send({nr_sessions:6,session_length:2});

		res.should.have.status(200);

		let trigger = await TriggerModel.findByPk(2);
		trigger.has_been_changed.should.equal(0);

		trigger = await TriggerModel.findByPk(3);
		trigger.has_been_changed.should.equal(0);
		
		let result = await PartModel.findOne({where:{id:1}});
		result.nr_sessions.should.equal(6);
		result.session_length.should.equal(2);

		const t1 = await ScopeModel.findOne({where:{id:1}});
		t1.scope_order.should.equal(1);
		t1.mask.should.equal('1-2');
		const t2 = await ScopeModel.findOne({where:{id:2}});
		t2.scope_order.should.equal(2);
		t2.mask.should.equal('1-4');
		const t3 = await ScopeModel.findOne({where:{id:3}});
		t3.scope_order.should.equal(3);
		t3.mask.should.equal('1-6');
		const t4 = await ScopeModel.findOne({where:{id:4}});
		t4.scope_order.should.equal(4);
		t4.mask.should.equal('1-9');
	});

	it('Should return 200 : Complex change [1;12]', async function() {
		const res = await chai
			.request(app)
			.put('/part/time_division/1')
        	.set({ "Authorization": `Bearer ${alphaT}`})
			.send({nr_sessions:1,session_length:12});

		res.should.have.status(200);

		let trigger = await TriggerModel.findByPk(2);
		trigger.has_been_changed.should.equal(0);

		trigger = await TriggerModel.findByPk(3);
		trigger.has_been_changed.should.equal(0);

		let result = await PartModel.findOne({where:{id:1}});
		result.nr_sessions.should.equal(1);
		result.session_length.should.equal(12);

		const t1 = await ScopeModel.findOne({where:{id:1}});
		t1.scope_order.should.equal(1);
		t1.mask.should.equal('1');
		const t2 = await ScopeModel.findOne({where:{id:2}});
		t2.scope_order.should.equal(2);
		t2.mask.should.equal('1');
		const t3 = await ScopeModel.findOne({where:{id:3}});
		t3.scope_order.should.equal(3);
		t3.mask.should.equal('1');
		const t4 = await ScopeModel.findOne({where:{id:4}});
		t4.scope_order.should.equal(4);
		t4.mask.should.equal('1-9');
	});
	
	it('Should return 200 : Complex change [2;6]', async function() {
		const res = await chai
			.request(app)
			.put('/part/time_division/1')
        	.set({ "Authorization": `Bearer ${alphaT}`})
			.send({nr_sessions:2,session_length:6});

		res.should.have.status(200);

		let trigger = await TriggerModel.findByPk(2);
		trigger.has_been_changed.should.equal(0);

		trigger = await TriggerModel.findByPk(3);
		trigger.has_been_changed.should.equal(0);

		let result = await PartModel.findOne({where:{id:1}});
		result.nr_sessions.should.equal(2);
		result.session_length.should.equal(6);

		const t1 = await ScopeModel.findOne({where:{id:1}});
		t1.scope_order.should.equal(1);
		t1.mask.should.equal('1-2');
		const t2 = await ScopeModel.findOne({where:{id:2}});
		t2.scope_order.should.equal(2);
		t2.mask.should.equal('1-2');
		const t3 = await ScopeModel.findOne({where:{id:3}});
		t3.scope_order.should.equal(3);
		t3.mask.should.equal('1-2');
		const t4 = await ScopeModel.findOne({where:{id:4}});
		t4.scope_order.should.equal(4);
		t4.mask.should.equal('1-9');
	});

});