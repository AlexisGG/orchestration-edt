const { user: UserModel, course: CourseModel, Sequelize } = require('../db/sequelize');
var _ = require('lodash');
const { Op } = require("sequelize");

const auth = require('../middlewares/auth');
const rights = require('../middlewares/rights');
const requiredField = require('../middlewares/requiredField');
const sequelize = require('sequelize');

module.exports = {
    path: "/users",
    config: (router) => {
        
        /**
        *   @openapi
        *   /users/register:
        *    post:
        *      tags:
        *      - "User"
        *      summary: "Register a new account"
        *      requestBody :
        *        required: true
        *        content:
        *           application/json:
        *               schema:
        *                   $ref: "#/definitions/User"
        *      responses:
        *        "200":
        *          description: "Success, account created and return user data"
        *          content:
        *            application/json:
        *               schema:
        *                   $ref: "#/definitions/User"
        *        "406":
        *          description: "Not acceptable due to a parameter"
        *          content:
        *            application/json:
        *              schema:
        *                type: "object"
        *                properties :
        *                   field :
        *                       type : string
        *                       enum : ['username','password']
        *                   type :
        *                       type : string
        *                       enum : ['integer','string','boolean']
        *        "409":
        *          description: "Login already used"
        *        "400":
        *          description: "Bad request"
        *        "500":
        *          description: "Internal server error"
        */
        
        // Création d'un compe utilisateur avec identifiant et mot de passe
        router.post("/register",requiredField({'username':'string','password':'string'}),async (req, res, next) => {
            try {
                const newUser = await UserModel.create({ username: req.body.username, password: req.body.password });
                
                res.status(200).send(newUser.anonymize());
            } catch ( err ) {
                next(err);
            }
        });


        /**
        *   @openapi
        *   /users/login:
        *    post:
        *      tags:
        *      - "User"
        *      summary: "Login with login and password"
        *      requestBody :
        *        required: true
        *        content:
        *           application/json:
        *               schema:
        *                   $ref: "#/definitions/User"
        *      responses:
        *        "200":
        *          description: "Success, succesfuly logged in and return user data"
        *          content:
        *            application/json:
        *               schema:
        *                   $ref: "#/definitions/User"
        *        "400":
        *           description: "Username too short"
        *        "406":
        *          description: "Not acceptable due to a parameter"
        *          content:
        *            application/json:
        *              schema:
        *                type: "object"
        *                properties :
        *                   field :
        *                       type : string
        *                       enum : ['username','password']
        *                   type :
        *                       type : string
        *                       enum : ['integer','string','boolean']
        *        "500":
        *          description: "Internal server error"
        */

        // Connexion d'un utilisateur via nom d'utilisateur et mot de passe
        router.post("/login",requiredField({'username':'string','password':'string'}),async (req, res, next) => {
            try {
                const user = await UserModel.findOne({ where : {
                    username : req.body.username
                }});

                if(user && user.validatePassword(req.body.password))
                {
                    // Generate web token
                    res.status(200).send({...user.anonymize(), token: user.generateToken()});
                }
                else
                    res.sendStatus(400);
            } catch ( err ) {
				next(err);
            }
        });

        /**
        *   @openapi
        *   /users:
        *    get:
        *      security:
        *      - token: []
        *      tags:
        *      - "User"
        *      summary: "Get data of connnected user"
        *      responses:
        *        "200":
        *          description: "Success, return user data"
        *          content:
        *            application/json:
        *               schema:
        *                   $ref: "#/definitions/User"
        *        "401":
        *           description: "Bad request"
        *        "500":
        *          description: "Internal server error"
        */

        // Récupération des informations de l'utilisateur connecté
        router.get("/", auth,async (req, res, next) => {
            try {
                const user = await UserModel.findOne({ 
                    where : {
                        username : req.user.username
                    }, include : CourseModel
                });
                
                if(user)
                {
                    for(i in user.courses)
                    {
                        const formation = await user.courses[i].getFormation();
                        user.courses[i].dataValues.formation = formation.tag;
                        delete user.courses[i].userRights;
                    }

                    const result = user.courses.reduce(function (r, a) {
                        r[a.dataValues.formation] = r[a.dataValues.formation] || [];
                        delete a.dataValues.userRights;
                        r[a.dataValues.formation].push(a);
                        delete a.dataValues.formation;
                        return r;
                    }, Object.create(null));

                    user.dataValues.rights = result;
                    delete user.dataValues.courses;

                    res.status(200).send({...user.anonymize()});
                }
                else
					res.sendStatus(401);
            } catch ( err ) {
				next(err);
            }
        });
        
        /**
        *   @openapi
        *   /users:
        *    put:
        *      security:
        *      - token: []
        *      tags:
        *      - "User"
        *      summary: "Change information of the current user"
        *      requestBody :
        *        required: true
        *        content:
        *          application/json:
        *            schema:
        *              $ref: "#/definitions/UserEdit"
        *      responses:
        *        "200":
        *          description: "Success, data has been modified and returned"
        *          content:
        *            application/json:
        *              schema:
        *                $ref: "#/definitions/User"
        *        "401":
        *           description: "Bad request"
        *        "409":
        *           description: "Can't change to that username, this username is already taken"
        *        "500":
        *          description: "Internal server error"
        */

		// Modifie le mot de passe ou le login de l'utilisateur, puis renvoie les informations mises à jours
		router.put("/", auth,async (req, res, next) => {
			try {
				if(req.body.password) {
					await UserModel.update({ password: req.body.password}, { where: { username: req.user.username } } );
				}
				if(req.body.username) {
					await UserModel.update({ username: req.body.username}, { where: { username: req.user.username } } );
					req.user.username = req.body.username;
				}
				const user = await UserModel.findOne({ where : {
					username : req.user.username
				}});
				res.status(200).send({...user.anonymize(),token : user.generateToken()});
			} catch ( err ) {
				next(err);
			}
		});
		
		/**
        *   @openapi
        *   /users/disconnect:
        *    get:
        *      security:
        *      - token: []
        *      tags:
        *      - "User"
        *      summary: "Disconnect the current user."
        *      responses:
        *        "200":
        *          description: "Successfuly disconnected !"
        *        "401":
        *           description: "Bad request"
        *        "500":
        *          description: "Internal server error"
        */

		// Déconnecte l'utilisateur qui est actuellement connecté
		router.get("/disconnect/", auth,async (req, res) => {
			const authHeader = req.headers['authorization'];
			const token = authHeader && authHeader.split(' ')[1];

			process.deletedAPIToken[token] = (+new Date()) + process.env.API_TOKEN_DURATION;

			res.status(200).send({token:'deleted'});
		});     
              
        /**
        *   @openapi
        *   /users:
        *    delete:
        *      security:
        *      - token: []
        *      tags:
        *      - "User"
        *      summary: "Delete the current user from Database"
        *      responses:
        *        "200":
        *          description: "Success"
        *        "401":
        *           description: "Bad request"
        *        "500":
        *          description: "Internal server error"
        */

        // Supression de l'utilisateur connecté
        router.delete("/",auth,async(req,res, next)=>{
            try {
                const user = await UserModel.findOne({ where : {
                    username : req.user.username
                }});
                if(user){
                    user.destroy();
                    res.sendStatus(200);
                }
                else{
                    res.sendStatus(401);
                }
            } catch( err ) {
                next(err);
            }
        });

        /**
        *   @openapi
        *   /users/search:
        *    get:
        *      security:
        *      - token: []
        *      tags:
        *      - "User"
        *      summary: "Search user by many parameters, need to be admin"
        *      parameters:
        *      - in: query
        *        name : "name"
        *        schema:
        *           type: string
        *        description: The name of the user searched
        *        example: "Jean"
        *      responses:
        *        "200":
        *          description: "Success"
        *          content:
        *            application/json:
        *             schema:
        *                type: "array"
        *                items:
        *                   type: "object"
        *                   properties:
        *                      username:
        *                         type: "string"
        *                         example: "j.dupont"
        *                      id:
        *                         type: "int"
        *                         example: 1234
        *                
        *        "401":
        *           description: "Bad request"
        *        "403":
        *           description: "Access denied (must be admin)"
        *        "500":
        *           description: "Internal server error"
        */

        // Cherche les utilisateurs en utilisant les paramètres fournit dans la requête, pour l'instant le nom
        router.get("/search", auth, rights.isAdmin,async (req, res, next) => {
            try {
				if( req.query.name ) {
					// l'instruction like dans mysql est par défaut case insensitive et donc pas besoin de le faire
                	const result = await UserModel.findAll({ where : { username : { [ Op.like ] : `%${req.query.name}%` } } });
					// on filtre en déconstruisant l'objet pour retiré les informations non-nécessaires 
					res.status(200).send(result.map( ({username,id}) => ({username,id}) ));
				} else {
                    // On retourne tous les utilisateurs si pas de paramètres
					const result = await UserModel.findAll();
                    res.status(200).send(result.map( ({username,id}) => ({username,id}) ));
				}
            } catch( err ) {
                next(err);
            }
        });

        return router;
    },
};