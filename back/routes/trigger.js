const { 
	user : UserModel,
	course : CourseModel,
	trigger : TriggerModel,	
	rule : RuleModel,
	rule_parameter : RuleParameterModel,
	scope: ScopeModel,
	filter: FilterModel,
	element_type: ElementTypeModel,
	label: LabelModel,
	element: ElementModel,
	sequelize } = require('../db/sequelize');
const auth = require('../middlewares/auth');
const requiredField = require('../middlewares/requiredField');
const rights = require('../middlewares/rights')

module.exports = {
    path: "/trigger",
    middlewares : [auth], // Utilisation de middlewares pour vérifier si l'utilisateur est admin
    config: (router) => {
        /**
        *   @openapi
        *   /trigger/all/{course_id}:
        *     get:
        *      security:
        *      - token: []
        *      tags:
        *      - "Trigger"
        *      summary: "Get all triggers with course_id"
        *      parameters:
        *      - in: path
        *        name: course_id
        *        schema:
        *          type: integer
        *        required: true
        *        description: Id of the course
        *
        *      responses:
        *        "200":
        *          description: "Success"
        *          content:
        *            application/json:
        *             schema:
        *                type: "array"
        *                items:
        *                   type: "object"
        *                   properties:
        *                      id :
        *                         type : integer
        *                         example : 2
        *                      text:
        *                         type: "string"
        *                         example: "Le TP 1 est après le CM 3"
        *                      rules_id:
        *                         type: "array"
        *                         items : 
        *                           type: integer
        *                           example :
        *                           - 4
        *        "404":
        *           description: "Not Found"
        *        "500":
        *          description: "Internal server error"
        */

        // Récupération de tous les triggers liée à une matière, cette route retourne également toutes les rule_id liés à ce trigger
        // Il faut vérifier que l'utilisateur à accès à ce cours
        router.get("/all/:course_id", rights.onCourse ,async (req, res,next) => {
            try {
                if(req.params.course_id!=null)
                {
                    const triggers = await TriggerModel.findAll({
                        include : [{
                            model: RuleModel,
                            attributes: ['id']
                        }],
                        where : { 'course_id' :req.params.course_id}
                    });
               
                    for(t in triggers){
                        let ids=[]
                        for (r in triggers[t].rules){
                            ids.push(triggers[t].rules[r].id);
                        }
                        triggers[t].dataValues.rules_id=ids;
                        delete triggers[t].dataValues.rules
                    }
               
                    res.status(200).send(triggers);   
                }
                else
                    res.status(404).send({'field':'course_id'})
            
            } catch (error) {
              next(error);  
            }
        })
        /**
        *   @openapi
        *   /trigger/{trigger_id}:
        *     get:
        *      security:
        *      - token: []
        *      tags:
        *      - "Trigger"
        *      summary: "Get all data about a trigger with his trigger_id"
        *      parameters:
        *      - in: path
        *        name: trigger_id
        *        schema:
        *          type: integer
        *        required: true
        *        description: Id of the trigger
        *
        *      responses:
        *        "200":
        *          description: "Success"
        *          content:
        *            application/json:
        *             schema:
        *               $ref: "#/definitions/Trigger"
        *        "404":
        *           description: "Not Found"
        *        "500":
        *          description: "Internal server error"
        */

        // Récupération de toutes les informations liées à un trigger, les rules liée à ce trigger, et les paramètres et scopes de ces rules
        // Il faut vérifier que l'utilisateur à accès au cours du trigger
        router.get("/:trigger_id",async (req, res, next) => {
            try {
                // Check if trigger exist
                const trigger = await TriggerModel.findByPk(req.params.trigger_id, 
                    {attributes : ['id','text','file','request'],
                    include : [
                        CourseModel,
                        { model : RuleModel , attributes : ['id','constraint_name','constraint_type'], include : [
                            {model : RuleParameterModel, attributes : ['id','name','type','value'] , as:"parameter"},
                            {model : ScopeModel, attributes : ['id','mask','scope_order'], include : [
                                { model : ElementTypeModel , as : "groupBy"},
                                { model : FilterModel, attributes : ['id','including','label'], include : [
                                    { model : LabelModel},
                                    { model : ElementModel, attributes: ["id"]},
                                    { model : ElementTypeModel , as : "type"}
                                ]}
                            ]}
                        ]},

                    ]}
                );
                
                if(trigger)
                {
                    // Check if user has rights on the course of the trigger
                    const user = await UserModel.findOne({where : {username : req.user.username }});
                    if(user.isAdmin || await user.hasCourse(trigger.course))
                        res.status(200).send(trigger);
                    else
                        res.sendStatus(403);
                }
                else
                    res.sendStatus(404);
            } catch(error)
            {
                console.log(error);
                next(error);
            }
        });

        /**
        *   @openapi
        *   /trigger/{trigger_id}:
        *    delete:
        *      security:
        *      - token: []
        *      tags:
        *      - "Trigger"
        *      summary: "Delete the current trigger and all the rules who are linked to this trigger"
        *      parameters:
        *      - in: path
        *        name: trigger_id
        *        schema:
        *           type: integer
        *           example : 12
        *        required: true
        *        description: Id of the trigger to delete
        *      responses:
        *        "200":
        *          description: "Success"
        *        "401":
        *           description: "Not connected"
        *        "403":
        *           description: "Access denied"
        *        "404":
        *           description: "Not Found"
        *        "500":
        *          description: "Internal server error"
        */

        // Supprime le trigger et toutes les rules qui sont liées
        // Il faut vérifier que l'utilisateur à accès à ce cours
        router.delete("/:trigger_id",async (req, res, next) => {
			try {
				let trigger = await TriggerModel.findOne({ where : { id : req.params.trigger_id } });
				if( trigger ) {
					let U = await UserModel.findOne({ where : { username : req.user.username } });
					let C = await CourseModel.findOne({ where : { id : trigger.course_id } });
					if( req.user.isAdmin || await C.hasUser( U ) ) {
						// Toutes les Rule Scope etc seront supprimé automatiquement en cascade
						await trigger.destroy();
						res.sendStatus(200);
					} else {
						res.sendStatus(403);
					}
				} else {
					res.sendStatus(404);
				}
			} catch( err ) {
				next( err );
			}
        });

        /**
        *   @openapi
        *   /trigger:
        *    post:
        *      security:
        *      - token: []
        *      tags:
        *      - "Trigger"
        *      summary: "Create one or mutliple rules"
        *      requestBody :
        *        required: true
        *        content:
        *           application/json:
        *               schema:
        *                   $ref: "#/definitions/TriggerCreate"
        *      responses:
        *        "200":
        *          description: "Success, trigger and children element created"
        *        "403":
        *          description: "Access denied on this course"
        *        "404":
        *          description: "Data does not exist for this field given"
        *          content:
        *            application/json:
        *              schema:
        *                type: "object"
        *                properties :
        *                   field :
        *                       type : string
        *                       enum : ['course_id','group_by_id','element_type_id','elements_ids','labels_id']
        *        "406":
        *          description: "Not acceptable due to a parameter"
        *          content:
        *            application/json:
        *              schema:
        *                type: "object"
        *                properties :
        *                   field :
        *                       type : string
        *                       enum : ['course_id','text','rules']
        *                   type :
        *                       type : string
        *                       enum : ['integer','string','array']
        *        "500":
        *          description: "Internal server error"
        */

        // Crée une nouvelle règle, et retourne l'id de la nouvelle règle
        // Il faut vérifier que l'utilisateur à accès au cours pour lequel il veut créer un trigger
        router.post("/", auth,
		requiredField({"course_id":"integer","text":"string","file":"string","request":"string"}),
		async (req, res, next) => {

			class DBNotFindError extends Error {
				constructor( field ) {
					super( `Nothing found in database that matches the field : ${field} !` );
					this.field = field;
				}
			}

			let transaction;
			try {
				// Récupération Course
				const course = await CourseModel.findByPk( req.body.course_id );
				if( ! course ) {
					throw new DBNotFindError('course_id');
				}

				// Vérification rules
				if( (!req.body.rules) || (req.body.rules.length < 1) ) {
					return res.status( 406 ).send({ field: 'rules', type: 'array' });
				}

				// Récupération User
				const user = await UserModel.findOne( { where : { username : req.user.username } } );
				// Vérification right on course
				if( user.isAdmin || await user.hasCourse( course ) ) {
					// Création transaction (en cas d'echec après la première transaction)
					transaction = await sequelize.transaction();

					// Création Trigger
					const trigger = await TriggerModel.create({ text: req.body.text, file : req.body.file, request: req.body.request}, {transaction} );
					await course.addTrigger( trigger , {transaction} );

					// Création Rule
					if( req.body.rules && req.body.rules.map ) {
						await Promise.all( req.body.rules.map( async (reqrule) => {
							if( typeof reqrule == 'object' ) {
								const rule = await RuleModel.create({ constraint_name: reqrule.name, constraint_type: reqrule.type}, {transaction});
								await trigger.addRule(rule, {transaction} );

								// Création Rule Parameter
								let task1 = null;
								if( reqrule.parameter && reqrule.parameter.map ) {
									task1 =Promise.all( reqrule.parameter.map( async ( reqruleparam) => {
										if( typeof reqruleparam == 'object' ) {
											const rule_parameter = await RuleParameterModel.create({ name: reqruleparam.name, type: reqruleparam.type, value: reqruleparam.value }, {transaction});
											await rule.addParameter( rule_parameter , {transaction} );
										}
									} ) );
								}

								// Création Scope
								let task2 = null;
								if( reqrule.scope && reqrule.scope.map ) {
									task2 = Promise.all( reqrule.scope.map( async ( reqscope ) => {
										if( typeof reqscope == 'object' ) {
											const scope = await ScopeModel.create({ mask: reqscope.mask, scope_order: reqscope.scope_order }, {transaction});
											await rule.addScope( scope , {transaction} );

											let taskX = async function() {
												if( typeof reqscope.group_by_id == 'number' ) {
													const element_type = await ElementTypeModel.findByPk( reqscope.group_by_id );
													if( element_type ) {
														element_type.addScope( scope , {transaction} );
													} else {
														throw new DBNotFindError('group_by_id');
													}
												}
											}();

											// Création Filter
											let taskY = null;
											if( reqscope.filter && reqscope.filter.map ) {
												taskY = Promise.all( reqscope.filter.map( async (reqfilter) => {
													if( typeof reqfilter == 'object' ) {
														const filter = await FilterModel.create({ including: reqfilter.including , label: reqfilter.label }, {transaction});
														await scope.addFilter( filter , {transaction} );
														

														async function LinkCreator( A , id , model , field) {
															const B = await model.findByPk( id , {transaction});
															if( B ) {
																await B.addFilter( A , {transaction});
															} else {
																throw new DBNotFindError(field);
															}
														}
														
														// Liaison Element Type
														let taskA = LinkCreator( filter , reqfilter.element_type_id , ElementTypeModel , 'element_type_id' );

														// Liaison Label
														let taskB = null;
														if( reqfilter.labels_ids && reqfilter.labels_ids.map ) {
															taskB = Promise.all( reqfilter.labels_ids.map( (label_id) => LinkCreator( filter , label_id , LabelModel , 'labels_ids') ) );
														}

														// Liaison Element
														let taskC = null;
														if( reqfilter.elements_ids && reqfilter.elements_ids.map ) {
															taskC = Promise.all( reqfilter.elements_ids.map( async (element_id) => LinkCreator( filter , element_id , ElementModel , 'elements_ids') ) );
														}

														// Attente parallélisé
														await Promise.all([taskA,taskB,taskC]);
													}
												} ) );
											}

											// Attente parallélisé
											await Promise.all([taskX,taskY]);
										}
									} ) );
								}

								// Attente parallélisé
								await Promise.all([task1,task2]);
							}
						} ) );
					}
					await transaction.commit();
				} else {
					return res.sendStatus(403);
				}
				res.sendStatus(200);
			} catch( err ) {
				try {
					if( transaction )
						await transaction.rollback();
				} catch ( error ) {
					next( error );
				}
				if( err instanceof DBNotFindError ) {
					return res.status(404).send({ field : err.field });
				} else {
					next( err );
				}
			}
        });

        return router;
    }
};