const { 
    course: CourseModel,
    part : PartModel,
    part_type : PartTypeModel,
    room : RoomModel,
    person : PersonModel,
	course_bundle: CourseBundleModel,
    class : ClassModel,
	formation: FormationModel,
	Sequelize
} = require('../db/sequelize');
const auth = require('../middlewares/auth');
const rights = require('../middlewares/rights');

module.exports = {
    path: "/course",
    middlewares : [auth],
    config : (router) => {
        /**
        *   @openapi
        *   /course/search:
        *    get:
        *      security:
        *      - token: []
        *      tags:
        *      - "Course"
        *      summary: "Search course by course_bundle_id or/and name of the course"
        *      parameters:
        *      - in: query
        *        name : "name"
        *        schema:
        *           type: string
        *        example: math
        *      - in: query
        *        name : "formation_id"
        *        schema:
        *           type: int
        *        example: 1
        *      responses:
        *        "200":
        *          description: "Success"
        *          content:
        *            application/json:
        *             schema:
        *                type: "array"
        *                items:
        *                   type: "object"
        *                   properties:
        *                      name:
        *                         type: "string"
        *                         example: "mathematics"
        *                      id:
        *                         type: "int"
        *                         example: 1
        *        "206":
        *          description: "Success, but a cycle has been found in the database!"
        *          content:
        *            application/json:
        *             schema:
        *                type: "array"
        *                items:
        *                   type: "object"
        *                   properties:
        *                      name:
        *                         type: "string"
        *                         example: "mathematics"
        *                      id:
        *                         type: "int"
        *                         example: 1
        *        "401":
        *           description: "Bad request"
        *        "403":
        *           description: "Access denied (must be admin)"
        *        "500":
        *          description: "Internal server error"
        */

        // Fonction de recherche des course (matières), deux paramètre sont possibles, l'id de la formation et le nom de la matière
        // Si aucun paramètre n'est envoyée, toutes les matières sont retournées
        // Cette fonction retourne une liste de toutes les correspondances, pour chaque course retourne l'id et le nom de la course
        // Accessible seulement par l'administrateur
        router.get("/search", rights.isAdmin ,async (req, res, next) => {
		try {
			let CourseBundleCycle = false;
			const { formation_id:formationID, name:courseName } = req.query;

			let result;
			if( formationID ) {
				// Récupération de tous les CourseBundle directement lié à la formation
				let firstQuery = await CourseBundleModel.findAll({
					include : [
						{
							model : FormationModel ,
							where : { course_bundle_id : formationID }
						}
					]
				});

				// Récupération de tous les CourseBundle indirectement lié à la formation
				let bundleList = [];
				let toCheckList = firstQuery;

				let verificationCycle = {}; //Liste des prédécesseurs pour chaque noeud (utilisation des id)
				firstQuery.forEach( query => verificationCycle[query.id] = [] );
				while( toCheckList.length > 0 ) {
					// Recherche de bundle fils
					toCheckList = ( await Promise.all( toCheckList.map( toCheck => {
					// Ajout à la liste
					bundleList.push( toCheck );

					// Vérification si enfant
					return CourseBundleModel
						.findAll({
							as: 'Child',
							include: [ {
								model: CourseBundleModel,
								as: 'Parent',
								where: { id: toCheck.id }
								} ]
							})
							.then( liste => liste.map( k => { k.parentID = toCheck.id ; return k } ) );
					}) ) ).flat();

					// Vérification de la présence de cycle :
					toCheckList = toCheckList.filter( element => {
						if( verificationCycle[ element.id ] == undefined ) {
							verificationCycle[ element.id ] = verificationCycle[ element.parentID ];
							verificationCycle[ element.id ].push( element.parentID );
						} else {
							if( verificationCycle[ element.id ].indexOf( element.parentID ) != -1) {
								CourseBundleCycle = true;
								if( process.env.ENV !== 'test' )
									console.warn('\033[1;41;70m',`⚠ Cycle find in database, in course bundle ( id : ${element.id} ) ⚠`,'\033[0m');
								return false;
							} else {
								verificationCycle[ element.id ] = [...new Set([...(verificationCycle[ element.parentID ]), ...(verificationCycle[ element.id ])])];
								verificationCycle[ element.id ].push( element.parentID );
							}
						}
						return true;
					});
				}

				// Récupération de tous les Course liés aux CourseBundle précédents
				result = await CourseModel.findAll({
					// Restricition suplémentaire si fournis :
					where: ( (courseName) ? { name : { [Sequelize.Op.like] : `%${courseName}%` } } : {} ),
					include: [ {
						as: 'CourseBundle',
						model: CourseBundleModel,
						where: { [Sequelize.Op.or] : bundleList.map( ({id}) => ({id}) ) }
					} ]
				});
			} else {
				if( courseName ) {
					// Si seul nom défini on fait une recherche basé sur ce nom
					result = await CourseModel.findAll({ where : { name : { [Sequelize.Op.like] : `%${courseName}%` } } });
				} else {
					// Si aucun définie on retourne tous
					result = await CourseModel.findAll();
				}
			}
			res.status( CourseBundleCycle ? 206 : 200 ).send(result.map( element => ({id:element.dataValues.id,name:element.dataValues.name}) ));
		} catch( err ) {
			console.error(err);
			next(err);
		}
	});

        /**
        *   @openapi
        *   /course/{course_id}:
        *     get:
        *      security:
        *      - token: []
        *      tags:
        *      - "Course"
        *      summary: "Get all data about a course with his id"
        *      parameters:
        *      - in: path
        *        name: course_id
        *        schema:
        *          type: integer
        *        required: true
        *        description: Id of the course
        *
        *      responses:
        *        "200":
        *           description: "Success"
        *           content:
        *            application/json:
        *             schema:
        *                $ref: "#/definitions/GetCourse"
        *        "404":
        *           description: "Course Not Found"
        *        "500":
        *          description: "Internal server error"
        */
        
        // Retourne toutes les informations d'une matière en fonction de son id
        // Il faut vérifier que l'utilisateur à accès à ce cours
        // A voir avec le front ce dont il ont besoin mais pour l'instant, les part, le nombre de session, les salles possibles de ses parts

        router.get('/:course_id', rights.onCourse ,async (req, res,next) => {
            try {
                const course =  await CourseModel.findOne({
                    where : {
                        id : req.params.course_id
                    },
                    include : {
                        model : PartModel,
                        include : [
                            { model : PartTypeModel , as : "type"},
                            RoomModel,
                            { model : PersonModel, as : "teacher" },
                            { model : ClassModel , attributes : ['id','name','element_id']}
                        ]
                    }
                });

                // Add formation to response
                const formation = await course.getFormation();
                course.dataValues.formation = {id : formation.id, name : formation.tag};

                // Delete useless data
                course.parts.map(p => {
                    p.dataValues.type = p.type.name
                    delete p.dataValues.part_type_id;

                    p.rooms.map(r => {
                        r.dataValues.mandatory = r.room_allocation.mandatory;
                        delete r.dataValues.room_allocation;
                    })

                    p.teacher.map(t => {
                        t.dataValues.nr_sessions = t.service.nr_sessions;
                        delete t.dataValues.service;
                    })
                });
                
                res.status(200).send(course);
            } catch (e)
            {
                next(e);
            }
        });

        return router;
    }
}
