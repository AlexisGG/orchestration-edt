const auth = require('../middlewares/auth');
const rights = require('../middlewares/rights');
const requiredField = require('../middlewares/requiredField');

const { 
	part : PartModel,  
	course : CourseModel , 
	user : UserModel,
	scope : ScopeModel,
	rule : RuleModel,
	trigger : TriggerModel,
	element: ElementModel,
	element_type: ElementTypeModel
} = require('../db/sequelize');

const { Op } = require("sequelize");

module.exports = {
    path : "/part",
    middlewares : [auth],
    config : (router) => {
        
        /**
        *   @openapi
        *   /part/{course_id}:
        *    post:
        *      security:
        *      - token: []
        *      tags:
        *      - "Part"
        *      summary: "Create new part"
        *      parameters:
        *      - in: path
        *        name: course_id
        *        schema:
        *          type: integer
        *        required: true
        *        description: Id of the course
        *      requestBody :
        *        required: true
        *        content:
        *           application/json:
        *               schema:
        *                   type : object
        *                   properties :
        *                       part_type_id :
        *                           type : integer
        *                           example : 6
        *                       nr_sessions :
        *                           type : integer
        *                           example : 1
        *                       session_length :
        *                           type : integer
        *                           example : 2
        *                       nr_teachers :
        *                           type : integer
        *                           example : 4
        *                       single_room :
        *                           type : boolean
        *                           example : false
        *      responses:
        *        "200":
        *          description: "Success"
        *        "406":
        *          description: "Not acceptable due to a parameter"
        *          content:
        *            application/json:
        *              schema:
        *                type: "object"
        *                properties :
        *                   field :
        *                       type : string
        *                       enum : ['part_type_id','nr_sessions','session_length','nr_teachers','single_room']
        *                   type :
        *                       type : string
        *                       enum : ['integer','string','boolean']
        *        "404":
        *          description: "No matching course for the given id"
        *        "500":
        *          description: "Internal server error"
        */

        // Création d'une nouvelle part, utiliser pour créer des CC etc
        // Il faut vérifier que l'utilisateur à accès à ce cours
        // Il faut penser également à créer un nouvel element du type PART et le liée à cette part
        // Il vaut vérifier si il n'existe pas déja une part dans ce cours avec le même type, si c'est le un code 409 est retourné
        router.post("/:course_id" , auth, rights.onCourse, 
			requiredField({'part_type_id':'integer','nr_sessions':'integer','session_length':'integer','nr_teachers':'integer','single_room':'boolean'}),
			async (req, res, next) => {
			try {
				let e = await ElementModel.create({ type: (await ElementTypeModel.findOne({ where : { name: 'PART' } })).id });
				let p = await PartModel.create({
					part_type_id: req.body.part_type_id,
					nr_sessions: req.body.nr_sessions,
					nr_teachers: req.body.nr_teachers,
					single_room: req.body.single_room,
					session_length: req.body.session_length
				});
				await Promise.all([
					req.course.addPart( p ),
					e.addPart( p )
				]);
				res.sendStatus(200);
				
			} catch (err) {
				next( err );
			}
        });

        /**
        *   @openapi
        *   /part/time_division/{course_id}:
        *    put:
        *      security:
        *      - token: []
        *      tags:
        *      - "Part"
        *      summary: "Update time vision of a part"
        *      parameters:
        *      - in: path
        *        name: course_id
        *        schema:
        *          type: integer
        *        required: true
        *        description: Id of the course
        *      requestBody :
        *        required: true
        *        content:
        *           application/json:
        *               schema:
        *                   type : object
        *                   properties :
        *                       nr_sessions :
        *                           type : integer
        *                           example : 1
        *                       session_length :
        *                           type : integer
        *                           example : 2
        *      responses:
        *        "200":
        *          description: "Success"
        *        "409":
        *          description: "Amount of time before and after need to stay constant"
        *        "403":
        *          description: "You do not have access to this course"
        *        "404":
        *          description: "The course with the given id do not exist"
        *        "401":
        *          description: "Not connected"
        *        "406":
        *          description: "Not acceptable due to a parameter"
        *          content:
        *            application/json:
        *              schema:
        *                type: "object"
        *                properties :
        *                   field :
        *                       type : string
        *                       enum : ['nr_sessions','session_length']
        *                   type :
        *                       type : string
        *                       enum : ['integer','string','boolean']
        *        "500":
        *          description: "Internal server error"
        */

		// 1 - Est-ce que l'id fourni correspond bien à une `part` existante
		// 		Oui : suivant
		//		Non : renvoie code 404
		// 2 - Est-ce que l'utilisateur connecté a le droit sur le cours dont la `part` fait partie OU il est admin
		//		Oui : suivant
		//		Non : renvoie code 403
		// 3 - Est-ce que le temps total est consérvé (i.e. (nr_sessions*session_length)=C)
		//		Oui : suivant
		//		Non : renvoie code 400
		// 4 - Edition de la part
		// 5 - Changement scope des trigger
		//  a - Récupération de toutes les règles
		//	b - éditions des scopes
		//  c - indiquer dans un nouveau champs de scope que le scope à été éditer automatiquement et qu'il doit être controlé

        // Modification du découpage horaire
        // Il faut vérifier que l'utilisateur à accès à ce cours
        // il faut vérifier que le temps total ne change pas entre avant et après la modification
        // Niveau de difficulté :
        //  - Supprimmer toutes les triggers de ce course
        // OU
        //  - Supprimmer seulement les triggers qui sont en rapport avec des numéros de sessions sur ce course
        // OU
        //  - Changer les scopes des triggers pour qu'il correspondent au changement de découpage horaire (nr_sessions)
        router.put("/time_division/:partId",requiredField({'session_length':'integer','nr_sessions':'integer'}), async (req, res, next) => {
			try {
				// 1
				let {partId} = req.params;
				let part = await PartModel.findOne({where: { id: partId }});
				if( part ) {

					const hadPermission = async function(uname,cid) {
						let u = await UserModel.findOne({where:{username:uname}});
						let c = await CourseModel.findOne({where:{id:cid}});
						if(u && c)
							return await u.hasCourse(c);
						else
							return false;
					}

					if( req.user.isAdmin || await hadPermission(req.user.username,part.course_id) ) {

						// 3 
						let before = {
							GlobalTime : (+part.nr_sessions) * (+part.session_length),
							nr_sessions: (+part.nr_sessions),
							session_length: (+part.session_length)
						};
						let after = {
							GlobalTime : (+req.body.nr_sessions) * (+req.body.session_length),
							nr_sessions: (+req.body.nr_sessions),
							session_length: (+req.body.session_length)
						}
						if( before.GlobalTime == after.GlobalTime ) {

							// 4
							part.nr_sessions = after.nr_sessions;
							part.session_length = after.session_length
							let tacheA = part.save();

							// 5a
							let scope_list = [];
							let trigger_list = await TriggerModel.findAll({ where : { course_id : part.course_id } } );
							if( trigger_list.length > 0) {
								let rule_list = await RuleModel.findAll({ where : { trigger_id : { 
									[Op.or] : (trigger_list.map( trigger => trigger.id)) 
								}}});
								if( rule_list.length > 0) {
									scope_list = await ScopeModel.findAll({ where : { rule_id : {
										[Op.or] : (rule_list.map( rule => rule.id )) 
									}}});
								}
							}

							if(scope_list.length == 0) {
								await tacheA;
								return res.sendStatus(200); 
							}
									

							// 5b
							let tacheB = Promise.all(scope_list.map(async scope => {
								// 5b.1 : Récupération et parsage des données
								// Format imprimante de séance à liste de séance
								let a = scope.mask.split(',').map( k=>{
									let format = k.match(/^([0-9]+)-([0-9]+)$/);
									if( format ) {
										// k est du format x-y
										let min = parseInt(format[1]);
										let max = parseInt(format[2]);
										return [... new Array(max-min+1)].map( (_,i) => i+min );
									} else {
										// k est du format x
										return parseInt(k);
									}
								} ).flat();
								
								// La variable `a` représente ici la liste des séances sur lesquel s'applique le scope

								// 5b.2 : Transformation "aller"
								// Passage de par séance (old) à par créneau
								let b = a.map( slot => {
									slot -= 1;
									slot *= before.session_length;
									return [... new Array(before.session_length)].map( (_,i) => slot+i+1 );
								}).flat();
									
								// La variable `b` représente ici la liste des créneaux sur lesquel s'applique le scope


								// 5b.3 : Transformation "retour"
								// Passage de par créneau à par séance (new)
								//	> Comportement : Si une partie des créneaux de la séance à une règle alors elle le sera pour toute la séance 
								//	  dans le cas d'un rétrécissement du nombre de créneau et d'augmentation de leurs durée
								//	> méthode : division de toutes les valeurs par le 'scale factor' (i.e. after.session_length) arrondi inférrieur
								//	  on supprime les doublons ( pour changer le comportement d'au moins un à tous il faudrait vérifié que le nombre de doublon est égale au scale factor de même si on souhaite une proportion)
								let c = [... new Set(
									b.map( element => Math.ceil(element / after.session_length) )
								)];
								
								// La variable `c` représente ici la liste des créneaux sur lesquel s'appliquera le scope


								// 5b.4 : Reformater et renvoyer
								// Passage du format actuel au format "imprimante"
								let d = c.sort( (x,y) => x-y );
								let result = [];
								let i = 0;
								while( i < d.length ) {
									let j = 0;
									while( ((i+j) < d.length) && (d[i+j+1] == (d[i]+j+1)) ) {
										j++;
									}
									if( j == 0 ) {
										result.push( `${d[i]}` );
									} else {
										result.push( `${d[i]}-${d[i+j]}` );
									}
									i+=j+1;
								}

								// 5b.5 : Mise à jour de la valeur
								scope.mask = result.join`,`;

								// 5c
								let r = await scope.getRule();
								let t = await r.getTrigger();
								t.has_been_changed = true;
								t.save();

								return scope.save();
							} ) );

							await Promise.all([tacheA,tacheB]);
							return res.sendStatus(200);
						} else {
							return res.sendStatus(409);
						}
						
					}else {
						return res.sendStatus(403);
					}
				} else {
					return res.sendStatus(404);
				}
			} catch ( err ) {
				next( err );					
			}
		});

		return router;
    }
}