const { userRights : UserRightsModel, user : UserModel, course: CourseModel, sequelize } = require('../db/sequelize');
const auth = require('../middlewares/auth');
const rights = require('../middlewares/rights');
const requiredField = require('../middlewares/requiredField');
module.exports = {
    path: "/rights",
    middlewares : [auth, rights.isAdmin], // Utilisation de middlewares pour vérifier si l'utilisateur est admin
    config: (router) => {
        /**
        *   @openapi
        *   /rights:
        *    get:
        *      security:
        *      - token: []
        *      tags:
        *      - "Rights"
        *      summary: "Get all user and their rights, need to be admin"
        *      parameters:
        *      - in: query
        *        name : "user_id"
        *        schema:
        *           type: int
        *        example: 1
        *      - in: query
        *        name : "course_id"
        *        schema:
        *           type: int
        *        example: 1
        *      - in: query
        *        name : "formation_id"
        *        schema:
        *           type: int
        *        example: 1
        *      responses:
        *        "200":
        *          description: "Success"
        *          content:
        *            application/json:
        *             schema:
        *                type: "array"
        *                items:
        *                   type: "object"
        *                   properties:
        *                      user:
        *                         type: "object"
        *                         properties:
        *                           id:
        *                             type: "int"
        *                             example: 1
        *                           username:
        *                             type: "string"
        *                             example: "j.dupont"
        *                      course:
        *                         type: "object"
        *                         properties:
        *                           id:
        *                             type: "int"
        *                             example: 8
        *                           name:
        *                             type: "string"
        *                             example: "programmation"
        *                      formation:
        *                         type: "object"
        *                         properties:
        *                           id:
        *                             type: "int"
        *                             example: 12
        *                           name:
        *                             type: "string"
        *                             example: "L3"
        *        "401":
        *           description: "Bad request"
        *        "500":
        *          description: "Internal server error"
        */

        // Retourne les droits de tous les utilisateurs et leurs droits, en fonction des attributs de recherche sur l'utilisateur, la matière, et la formation.
        // Si aucun attribut n'est envoyé, alors on retourne tous les droits.
        router.get("/", async (req, res,next) => {
            try{
                var whereObject = {};

                if(req.query.user_id)
                {
                    whereObject['userId'] = req.query.user_id;
                }
                if(req.query.course_id)
                    whereObject['courseId'] = req.query.course_id;

                const userRights = await UserRightsModel.findAll({
                    attributes:[],
                    where: whereObject,
                    include: [
                        {
                            model: UserModel,
                            attributes:['id','username'],
                        },
                        {
                            model : CourseModel,
                            attributes:['id','name']
                        }
                    ]
                });
                
                // Récupération de la formation et filtre si le paramètre course_bundle_id est saisie
                var result = [];

                for(i in userRights)
                {
                    const formation = await userRights[i].course.getFormation();
                    userRights[i]['dataValues']['formation'] = {id: formation.id, name: formation.tag};
                    if(!(req.query.formation_id && req.query.formation_id != formation.id))
                        result.push(userRights[i]);
                }

                res.status(200).send(result);
            }
            catch(error){
                next(error)
            }
        });

        /**
        *   @openapi
        *   /rights:
        *    post:
        *      security:
        *      - token: []
        *      tags:
        *      - "Rights"
        *      summary: "Add rights to a user"
        *      requestBody :
        *        required: true
        *        content:
        *           application/json:
        *             schema:
        *               type: object
        *               properties:
        *                 user_id:
        *                   type: "integer"
        *                   format: "int64"
        *                   example: 1
        *                 course_id:
        *                   type: "integer"
        *                   format: "int64"
        *                   example: 1
        *      responses:
        *        "200":
        *          description: "Success"
        *        "401":
        *           description: "Bad request"
        *        "404":
        *           description: "Not found"
        *           content:
        *            application/json:
        *             schema:
        *                type: "object"
        *                properties :
        *                   field :
        *                       type : string
        *                       enum : ['course','user']  
        *        "406":
        *          description: "Not acceptable due to a parameter"
        *          content:
        *            application/json:
        *              schema:
        *                type: "object"
        *                properties :
        *                   field :
        *                       type : string
        *                       enum : ['course_id','user_id']   
        *                   type :
        *                       type : string
        *                       enum : ['integer','string','boolean']                 
        *        "409":
        *          description: "Conflict in db"
        *        "500":
        *          description: "Internal server error"
        * 
        *   
        */

        // Ajoute un droit à un utilsateur
        router.post("/", requiredField({'course_id':'integer','user_id':'integer'}),async (req, res) => {
            try {
                if(req.body.user_id==null || req.body.course_id==null)
                    res.sendStatus(401);
                else {
                    var user = await UserModel.findOne({
                        where : {id : req.body.user_id }
                    })
                    if(user){
                        var course = await CourseModel.findOne({
                            where : { id : req.body.course_id}
                        })
                        if(course){
                            if(await user.hasCourse(course))
                                res.sendStatus(409);
                            else{
                                await user.addCourse(course)
                                res.sendStatus(200);
                            }
                        }
                        else
                            res.status(404).send({field:'course'});
                    }
                    else
                        res.status(404).send({field:'user'});
                }
            }  
            catch (error) {
                console.log(err);
            }
        });

        /**
        *   @openapi
        *   /rights/{userId}/{courseId}:
        *     delete:
        *      security:
        *      - token: []
        *      tags:
        *      - "Rights"
        *      summary: "Delete rights of a user"
        *      parameters:
        *      - in: path
        *        name: userId
        *        schema:
        *          type: integer
        *        required: true
        *        description: Id of the user
        *      - in: path
        *        name: courseId
        *        schema:
        *          type: integer
        *        required: true
        *        description: Id of the user
        *
        *      responses:
        *        "200":
        *          description: "Success"
        *        "401":
        *           description: "Not connected"
        *        "403":
        *           description: "Access denied : need to be admin !"
        *        "404":
        *           description: "No user or course with the given id"
        *           content:
        *             application/json:
        *               schema:
		*                  type: object
		*                  properties:
		*                    field:
		*                       type: string
		*                       enum:
		*                         - course
		*                         - user
        *        "409":
        *           description: "Invalid request (user and course not linked)"
        *        "500":
        *          description: "Internal server error"
        */

        // Supprime un droit d'accès à un utilisateur 
        router.delete("/:userId/:courseId", async (req, res,next) => {
			try {
				let {userId,courseId} = req.params;
				let user = await UserModel.findOne({where:{ id: userId}});
				if( user ) {
					let course = await CourseModel.findOne({where:{ id: courseId}});
					if( course ) {
						if( await user.hasCourse(course) ) {
							await user.removeCourse(course);
							res.sendStatus(200);
						} else {
							res.sendStatus(409);
						}
					} else {
						res.status(404).send({field:'course'});
					}
				} else {
					res.status(404).send({field:'user'});
				}
			} catch(err) {
				console.error(' here' ,err);
				next(err);
			}
        });

        return router;
    },
};