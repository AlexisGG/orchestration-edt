const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  const CourseBundle = sequelize.define('course_bundle', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    tag: {
      type: DataTypes.STRING(256),
      allowNull: true
    },
    nr_weeks: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    days_per_week: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'course_bundle',
    timestamps: false
  });

  // Associations
	CourseBundle.associate = function(models){
    // Many to Many entre course et course_bundle
    CourseBundle.belongsToMany(models.course, {through: 'courseXcourse_bundle',foreignKey: "course_bundle_id"});
    // Many to Many entre course_bundle et course_bundle
    CourseBundle.belongsToMany(models.course_bundle, {as:'Parent', foreignKey:'course_bundle_id', through:"course_bundleXcourse_bundle"});
    CourseBundle.belongsToMany(models.course_bundle, {as:'Child', foreignKey:'course_bundle_id_parent', through:"course_bundleXcourse_bundle"});
    // One to Many entre course_bundle et formation
    CourseBundle.hasOne(models.formation, {foreignKey: "course_bundle_id"});
	}

  return CourseBundle;
};
