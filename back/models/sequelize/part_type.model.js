const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  const PartType = sequelize.define('part_type', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(256),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'part_type',
    timestamps: false
  });

  // Associations
	PartType.associate = function(models){
    // One to Many entre part et part_type
    PartType.hasMany(models.part, {foreignKey:"part_type_id"});
	}

  return PartType;
};
