const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  const RuleParameter = sequelize.define('rule_parameter', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(256),
      allowNull: true
    },
    type: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    value: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'rule_parameter',
    timestamps: false
  });

  RuleParameter.associate = function(models)
  {
    // Many to One entre filter et element_type
    RuleParameter.belongsTo(models.rule, {foreignKey:"rule_id", onDelete: "CASCADE"});
  }

  return RuleParameter;
};
