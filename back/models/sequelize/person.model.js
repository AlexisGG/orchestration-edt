const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  const Person = sequelize.define('person', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    teacher: {
      type: DataTypes.TINYINT,
      allowNull: true
    },
    name: {
      type: DataTypes.STRING(256),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'person',
    timestamps: false
  });

  Person.associate = function(models)
  {
    // Many to Many entre person et class en utilisant la table teacherXsession
    Person.belongsToMany(models.class, {through:"teacherXsession", foreignKey:"teacher_id"});
    // Many to One entre person et element
    Person.belongsTo(models.element, {foreignKey:"element_id"});
    // Many to Many entre person et part en utilisant la table service
    Person.belongsToMany(models.part, {through:"service", foreignKey:"teacher_id"});
  }

  return Person;
};
