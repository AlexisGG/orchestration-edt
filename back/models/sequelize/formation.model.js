const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
    const Formation = sequelize.define('formation', {
    },
      {
        sequelize,
        tableName: 'formation',
        timestamps: false
      }
    );

    Formation.associate = function (models)
    {
      // Many to One
      Formation.belongsTo(models.course_bundle , {foreignKey: "course_bundle_id"});
    };

    Formation.removeAttribute('id');

    return Formation;
};