const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  const Scope = sequelize.define('scope', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    mask: {
      type: DataTypes.STRING(256),
      allowNull: true
    },
    scope_order: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
  }, {
    sequelize,
    tableName: 'scope',
    timestamps: false
  });

  Scope.associate = function(models)
  {
    // Many to One entre filter et element_type
    Scope.belongsTo(models.element_type, {foreignKey:"group_by", as : "groupBy"});
    // Many to One entre filter et element_type
    Scope.belongsTo(models.rule, {foreignKey:"rule_id", onDelete: "CASCADE"});
	// Many To One entre filter et scope
	Scope.hasMany(models.filter, {foreignKey:"scope_id", onDelete: "CASCADE"});
  }

  return Scope;
};
