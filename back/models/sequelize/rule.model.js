const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  const Rule = sequelize.define('rule', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    constraint_name: {
      type: DataTypes.STRING(256),
      allowNull: true
    },
    constraint_type: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'rule',
    timestamps: false
  });

  // Associations
	Rule.associate = function(models){
    // One to Many entre rule et scopre
    Rule.hasMany(models.scope, {foreignKey:"rule_id", onDelete: "CASCADE"});
    // One to Many entre rule et rule_parameter
    Rule.hasMany(models.rule_parameter, {foreignKey:"rule_id", onDelete: "CASCADE", as: "parameter"});
    // Many to One entre rule et trigger
    Rule.belongsTo(models.trigger, {foreignKey:"trigger_id", onDelete: "CASCADE"});
	}

  return Rule;
};
