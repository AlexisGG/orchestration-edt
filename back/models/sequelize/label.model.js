const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  const Label = sequelize.define('label', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(256),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'label',
    timestamps: false,
  });

  Label.associate = function(models)
  {
    // Many to Many entre label et element
    Label.belongsToMany(models.element, {through:"elementXlabel", foreignKey:"label_id"});
    // Many to Many entre filter et label
    Label.belongsToMany(models.filter, {through:"filter_label", foreignKey:"label_id"});
  }

  return Label;
};
