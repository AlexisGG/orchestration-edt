const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = (sequelize, DataTypes) => {
	const User = sequelize.define(
		'user', // table name
		{
			// Model attributes are defined here
			id: {
				type: DataTypes.INTEGER,
				allowNull: false,
				primaryKey: true,
				autoIncrement: true
			},
			username: {
				type: DataTypes.STRING(100),
				allowNull: false,
				unique: true,
				validate: {
					len: [2,15]
				}
			},
			password: {
				type: DataTypes.STRING(64),
				allowNull: false,
				set(value) {
					this.setDataValue('password', bcrypt.hashSync(value, 10));
				}
			},
			isAdmin:{
				type: DataTypes.BOOLEAN,
				defaultValue : false
			}
		}, { timestamps: true }
	);

	// Associations
	User.associate = function(models){
		User.belongsToMany(models.course, {through: models.userRights, onDelete: 'cascade' });
	}

	// Test validation of password
	User.prototype.validatePassword = function(password) {
		return bcrypt.compareSync(password, this.password);
  	};

	User.prototype.generateToken = function() {
		return jwt.sign({username : this.username, isAdmin : this.isAdmin}, process.env.NODE_TOKEN_SECRET, { expiresIn: `${process.env.API_TOKEN_DURATION}s` });
	};

	// Return useful data
	User.prototype.anonymize = function()
	{
		delete this.dataValues.password;
		return this.dataValues;
	}

	return User;
};
