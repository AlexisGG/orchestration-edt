const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  const Room = sequelize.define('room', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(256),
      allowNull: true
    },
    capacity: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'room',
    timestamps: false
  });

  Room.associate = function(models){
    // Many to Many entre Room et Part en utlilisant la table room_allocation
    Room.belongsToMany(models.part, {through: models.room_allocation, foreignKey:"room_id"});
    // Many to One entre room et element
    Room.belongsTo(models.element, {foreignKey:"element_id"});
    // Many to Many entre class et room
    Room.belongsToMany(models.class, {through:"roomXsession", foreignKey:"room_id"});
  }

  return Room;
};
