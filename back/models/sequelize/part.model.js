const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  const Part = sequelize.define('part', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    nr_sessions: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    nr_teachers: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    max_headcount: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    session_length: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    single_room: {
      type: DataTypes.TINYINT,
      allowNull: true
    },
    starting_slots: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'part',
    timestamps: false
  });

  // Associations
	Part.associate = function(models){
    // Many to One entre part et course
    Part.belongsTo(models.course, {foreignKey:"course_id"});
    // Many to One entre part et part_type
    Part.belongsTo(models.part_type, {foreignKey:"part_type_id", as:"type"});
    // Many to One entre part et element
    Part.belongsTo(models.element, {foreignKey:"element_id"});
    // Many to Many entre part et room en utilisant la table room_allocation
    Part.belongsToMany(models.room, {through: models.room_allocation, foreignKey:"part_id"});
    // One to Many entre part et class
    Part.hasMany(models.class, {foreignKey:"part_id"});
    // Many to Many entre part et group
    Part.belongsToMany(models.group, {through:"group_registration", foreignKey:"part_id"});
    // Many to Many entre part et person en utilisant la table service
    Part.belongsToMany(models.person, {through:"service", foreignKey:"part_id", as : "teacher"});
	}

  return Part;
};