module.exports = function(sequelize, DataTypes) {
  const Course = sequelize.define('course', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(256),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'course',
    timestamps: false
  });

  // Associations
	Course.associate = function(models){
    // Many To Many entre course et user
		Course.belongsToMany(models.user, {through: models.userRights ,onDelete: 'cascade' });
    // Many to Many entre course et course_bundle
    Course.belongsToMany(models.course_bundle, {through: 'courseXcourse_bundle', foreignKey: "course_id", as:'CourseBundle'});
    // One to Many entre course et part
    Course.hasMany(models.part, {foreignKey:"course_id"});
    // Many to One entre course et element
    Course.belongsTo(models.element, {foreignKey:"element_id"});
    // One to Many entre course et trigger
    Course.hasMany(models.trigger, {foreignKey:"course_id"});
	}

  // Fonction qui recupère la première formation trouvée
  Course.prototype.getFormation = async function() {
    const course_bundles = await this.getCourseBundle();
    var alreadySeen = course_bundles.map(c => {return c.id});

    do{
      var cb = course_bundles.shift();
      var parent = await cb.getParent();
      if(await cb.getFormation())
      {
        return cb;
      } else if(parent)
      {
        alreadySeen.push(parent.id);
        course_bundles.push(...parent);
      }
    }while(course_bundles.length > 0)

    return null;
  }

  return Course;
};
