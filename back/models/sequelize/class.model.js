const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  const Class = sequelize.define('class', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(256),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'class',
    timestamps: false
  });

  Class.associate = function(models){
    // Many to One entre class et element
    Class.belongsTo(models.element, {foreignKey:"element_id"});
    // Many to One entre class et part
    Class.belongsTo(models.part, {foreignKey:"part_id"});
    // Many to Many entre class et class_parent
    Class.belongsToMany(models.class, {as:'child', foreignKey:'class_id', through:"class_parent"});
    Class.belongsToMany(models.class, {as:'parent', foreignKey:'parent_class_id', through:"class_parent"});
    // One to Many entre class et slotSession
    Class.hasMany(models.slotXsession, {foreignKey:"class_id"});
    // Many to Many entre class et group
    Class.belongsToMany(models.group, {through:"groupXclass", foreignKey:"class_id"});
    // Many to Many entre class et room
    Class.belongsToMany(models.room, {through:"roomXsession", foreignKey:"class_id"});
    // Many to Many entre class et person en utilisant la table teacherXsession
    Class.belongsToMany(models.person, {through:"teacherXsession", foreignKey:"class_id"});
  }
  
  return Class;
};
