const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  const RoomAllocation = sequelize.define('room_allocation', {
    mandatory: {
      type: DataTypes.TINYINT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'room_allocation',
    timestamps: false
  });

  return RoomAllocation;
};
