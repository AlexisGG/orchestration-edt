const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  const Equipment = sequelize.define('equipment', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(256),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'equipment',
    timestamps: false
  });

  Equipment.associate = function(models)
  {
    // Many to One entre person et element
    Equipment.belongsTo(models.element, {foreignKey:"element_id"});
  }

  return Equipment;
};
