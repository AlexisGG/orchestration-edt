const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  const ElementType = sequelize.define('element_type', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(256),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'element_type',
    timestamps: false
  });

  // Associations
	ElementType.associate = function(models){
    // One to Many entre element_type et element
    ElementType.hasMany(models.element, {foreignKey:"element_type"});
    // One to Many entre element_type et filter
    ElementType.hasMany(models.filter, {foreignKey:"element_type_id"});
    // One to Many entre element_type et scope
    ElementType.hasMany(models.scope, {foreignKey:"group_by"});
	}

  return ElementType;
};
