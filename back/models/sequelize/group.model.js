const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  const Group = sequelize.define('group', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(256),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'group',
    timestamps: false
  });

  Group.associate = function(models)
  {
    // Many to Many entre group et class
    Group.belongsToMany(models.class, {through:"groupXclass", foreignKey:"group_id"});
    // Many to Many entre group et part
    Group.belongsToMany(models.part, {through:"group_registration", foreignKey:"group_id"});
    // Many to One entre group et element
    Group.belongsTo(models.element, {foreignKey:"element_id"});
  }

  return Group;
};
