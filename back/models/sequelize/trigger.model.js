const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  const Trigger = sequelize.define('trigger', {
    text: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    has_been_changed: {
      type: DataTypes.TINYINT,
      allowNull: false,
      defaultValue: 0
      /* true only and only if this object has been changed automatically and need a manual review  / safety check */
    },
    file : {
      type: DataTypes.STRING(256),
      allowNull : false
    },
    request : {
      type: DataTypes.STRING(256),
      allowNull : false
    }
  }, {
    sequelize,
    tableName: 'trigger',
    timestamps: false
  });

  Trigger.associate = function(models)
  {
    // One to Many entre trigger et rule
    Trigger.hasMany(models.rule, {foreignKey:"trigger_id", onDelete: "CASCADE"});
    // Many to One entre trigger et course
    Trigger.belongsTo(models.course, {foreignKey:"course_id"});
  }

  return Trigger;
};