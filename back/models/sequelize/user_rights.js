const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  const UserRights = sequelize.define('userRights', {
  }, {
    sequelize,
    tableName: 'userRights',
    timestamps: false
  });

  UserRights.associate = function(models)
  {
    UserRights.belongsTo(models.user)
    UserRights.belongsTo(models.course)
  }

  return UserRights;
};
