const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  const SlotSession = sequelize.define('slotXsession', {
    session: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    slot: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'slotXsession',
    timestamps: false
  });

  SlotSession.removeAttribute('id');
  SlotSession.associate = function(models){
    // Many to One entre room et element
    SlotSession.belongsTo(models.class, {foreignKey:"class_id"});
  }

  return SlotSession;
};
