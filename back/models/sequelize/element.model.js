const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  const Element = sequelize.define('element', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    }
  }, {
    sequelize,
    tableName: 'element',
    timestamps: false
  });

  // Associations
	Element.associate = function(models){
    // One to Many entre element er part
    Element.hasMany(models.part, {foreignKey:"element_id"});
    // One to Many entre element et course
    Element.hasMany(models.course, {foreignKey:"element_id"});
    // Many to One entre element et element_type
    Element.belongsTo(models.element_type, {foreignKey:"element_type", as:"type"});
    // One to Many entre element et room
    Element.hasMany(models.room, {foreignKey:"element_id"});
    // One to Many entre element et class
    Element.hasMany(models.class, {foreignKey:"element_id"});
    // One to Many entre element et group
    Element.hasMany(models.group, {foreignKey:"element_id"});
    // One to Many entre element et person
    Element.hasMany(models.person, {foreignKey:"element_id"});
    // One to Many entre element et equipment
    Element.hasMany(models.equipment, {foreignKey:"element_id"});
    // Many to Many entre element et filtre
    Element.belongsToMany(models.filter, {through:"filter_element", foreignKey:"element_id"});
    // Many to Many entre element et label
    Element.belongsToMany(models.label, {through:"elementXlabel", foreignKey:"element_id"});
	}

  return Element;
};
