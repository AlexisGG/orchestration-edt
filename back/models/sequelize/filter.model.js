const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  const Filter = sequelize.define('filter', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    including: {
      type: DataTypes.TINYINT,
      allowNull: true,
      defaultValue: 1
    },
    label: {
      type: DataTypes.TINYINT,
      allowNull: true,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'filter',
    timestamps: false
  });

  Filter.associate = function(models)
  {
    // Many to Many entre filter et element
    Filter.belongsToMany(models.element, {through:"filter_element", foreignKey:"filter_id"});
    // Many to Many entre filter et label
    Filter.belongsToMany(models.label, {through:"filter_label", foreignKey:"filter_id"});
    // Many to One entre filter et element_type
    Filter.belongsTo(models.element_type, {foreignKey:"element_type_id", as:"type"});
	// Many To One entre filter et scope
	Filter.belongsTo(models.scope, {foreignKey:"scope_id"});
  }

  return Filter;
};
