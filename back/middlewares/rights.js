const { course: CourseModel, user : UserModel } = require('../db/sequelize');

module.exports = {
    isAdmin: (req, res, next) =>{
        if(req.user.isAdmin){
            next();
        }else{
            res.status(403).send();
        }
    },
    onCourse : async (req, res, next) => {
        // Check if course exist
        req.course = await CourseModel.findByPk(req.params.course_id);
        if(req.course)
        {
            // Check if user has rights on this course
            req.user = await UserModel.findOne({where : {username : req.user.username }});
            if(req.user.isAdmin || await req.user.hasCourse(req.course))
                next();
            else
                res.sendStatus(403);
        }
        else
            res.sendStatus(404);
    }
}