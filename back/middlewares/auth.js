const jwt = require('jsonwebtoken');

process.deletedAPIToken = {};

module.exports = (req, res, next) => {
  const authHeader = req.headers['authorization']
  const token = authHeader && authHeader.split(' ')[1]

  // Si le token n'est plus valide car la route /user/disconnect à été appeler
  if( process.deletedAPIToken[token] ) {
	return res.sendStatus(401);
  }

  // Retirera tous les tokens supprimer dont la date d'expiration a été atteinte.
  (async () => {
	const now = + new Date();
	for(let [token,expirationDate] of Object.entries(process.deletedAPIToken)) {
		if(expirationDate < now) {
			delete process.deletedAPIToken[token];
		}
	}
  })();

  jwt.verify(token, process.env.NODE_TOKEN_SECRET, (err, user) => {

    if (err) return res.sendStatus(401);

    req.user = user;

    next();
  })
};