import { post, get, put } from "./utils.js";

export async function register(data) {
    return await post("users/register", data);
}

export async function login(data) {
    return await post("users/login", data);
}

export async function getUser() {
    return await get("users");
}

export async function updateUser(data) {
    return await put("users", data);
}

export async function logout() {
    return await get("users/disconnect");
}

export async function getAllUsers() {
    return await get("users/search/");
}
