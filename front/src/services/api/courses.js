import { get, put } from "./utils.js";

export async function getAllCourses() {
    return await get("/course/search");
}

export async function getCoursesByFormationId(data) {
    const url = "/course/search?formation=" + data.formationId;
    return await get(url);
}

export async function getCourseById(id) {
    return get("course/" + id);
}

export async function getCourse(name = null, formation_id = null) {
    let req = "course/search";
    let connector = "?";
    if (name) {
        req += connector + "name=" + name;
        connector = "&";
    }
    if (formation_id) {
        req += connector + "formation_id=" + formation_id;
        connector = "&";
    }

    return get(req);
}

export async function changePartById(idPart, data) {
    return await put("part/time_division/" + idPart, data);
}
