import {get, post, remove} from "./utils";

export async function saveTrigger(object) {
    return post("trigger", object);
}

export async function getTriggerList(courseID) {
    return get("trigger/all/" + courseID);
}

export async function deleteTrigger(id){
    return remove("trigger/" + id);
}
