import { post, get, remove } from "./utils.js";

export async function getAllRights(data) {
    return await get("/rights", data);
}

export async function addRight(data) {
    return await post("/rights", data);
}

export async function removeRight(data) {
    const url = "/rights/" + data.userId + "/" + data.courseId;
    return await remove(url);
}
