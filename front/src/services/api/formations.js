import { get } from "./utils.js";

export async function getAllFormations() {
    return await get("/formation/search");
}

export async function getFormation(name = null) {
    let req = "formation/search";
    if (name) {
        req += "?name=" + name;
    }
    return await get(req);
}
