const part = [
    "CM",
    "TD",
    "TP"
];

const days = [
    "Lundi",
    "Mardi",
    "Mercredi",
    "Jeudi",
    "Vendredi",
    "Samedi",
    "Dimanche"
];

const freq = [
    "Jours",
    "Semaine",
    "Mois"
]

export {part, days, freq};
export default {part, days, freq};