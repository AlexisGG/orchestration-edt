import Vue from "vue";
import VueRouter from "vue-router";

const Error404 = () => import("@/views/Error404/Error404.view.vue");
const Home = () => import("@/views/Home/Home.view.vue");
const Register = () => import("@/views/Register/Register.view.vue");
const Profile = () => import("@/views/Profile/Profile.view.vue");
const Login = () => import("@/views/Login/Login.view.vue");
const Admin = () => import("@/views/Admin/Admin.view.vue");
const TimeDivision = () => import("@/views/TimeDivision/TimeDivision.view.vue");

import store from "../store";
import { LOGOUT } from "../store/actions.type";

Vue.use(VueRouter);

const cantAccessIfAuthenticated = (to, from, next) => {
    if (!store.getters.authentication) {
        next();
        return;
    }
    next("/");
};

const ifAuthenticated = (to, from, next) => {
    if (store.getters.loginDate + 86400 * 1000 >= Date.now()) {
        next();
        return;
    } else {
        store.dispatch(LOGOUT);
        next("/login");
    }
};

const ifCourseIsSelected = (to, from, next) => {
    if (store.getters.course) {
        next();
        return;
    } else {
        store.dispatch(LOGOUT);
        next("/");
    }
};

const routes = [
    {
        path: "/",
        name: "Home",
        component: Home,
    },
    {
        path: "/login",
        name: "Login",
        component: Login,
        beforeEnter: cantAccessIfAuthenticated,
    },
    {
        path: "/register",
        name: "Register",
        component: Register,
        beforeEnter: cantAccessIfAuthenticated,
    },
    {
        path: "/profile",
        name: "Profile",
        component: Profile,
        beforeEnter: ifAuthenticated,
    },
    {
        path: "/admin",
        name: "Admin",
        component: Admin,
        beforeEnter: ifAuthenticated,
    },
    {
        path: "/time-division",
        name: "TimeDivision",
        component: TimeDivision,
        beforeEnter: ifAuthenticated && ifCourseIsSelected,
    },
    {
        path: "*",
        name: "404 Not Found",
        component: Error404,
    },
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes,
});

export default router;
