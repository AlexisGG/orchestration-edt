import Vue from "vue";
import Vuex from "vuex";
import actions from "./actions";
import mutations from "./mutations";
import getters from "./getters";
import state from "./state";
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

export const createStore = () =>
    new Vuex.Store({
        state,
        mutations,
        actions,
        getters,
        plugins: [createPersistedState()],
    });
export default createStore();
