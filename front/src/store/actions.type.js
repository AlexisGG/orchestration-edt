export const LOGIN = "LOGIN";
export const COURSE = "COURSE";
export const TRIGGER = "TRIGGER";
export const CONSTRAINT_LIST = "CONSTRAINT_LIST";
export const LOGOUT = "LOGOUT";
export const UPDATE_USER = "UPDATE_USER";
