const getters = {
    authentication(state) {
        return state.authentication;
    },
    user(state) {
        return state.user;
    },
    loginDate(state) {
        return state.loginDate;
    },
    formation(state) {
        return state.formation;
    },
    course(state) {
        return state.course;
    },
    trigger(state) {
        return state.trigger;
    },
    constraint_list(state) {
        return state.constraint_list;
    },
};
export default getters;
