import {
    LOGIN,
    LOGOUT,
    COURSE,
    TRIGGER,
    CONSTRAINT_LIST,
    UPDATE_USER,
} from "./actions.type";

import {
    SET_AUTHENTICATION,
    SET_USER,
    SET_LOGINDATE,
    SET_COURSE,
    SET_FORMATION,
    SET_TRIGGER,
    SET_CONSTRAINT_LIST,
} from "./mutations.type";

const actions = {
    [LOGIN]({ commit }, { token, user }) {
        commit(SET_AUTHENTICATION, token);
        commit(SET_USER, user);
        commit(SET_LOGINDATE, Date.now());
        commit(SET_FORMATION, null);
        commit(SET_COURSE, null);
        commit(SET_TRIGGER, null);
        commit(SET_CONSTRAINT_LIST, []);
    },
    [COURSE]({ commit }, { formation, course }) {
        commit(SET_FORMATION, formation);
        commit(SET_COURSE, course);
    },
    [TRIGGER]({ commit }, { trigger }) {
        commit(SET_TRIGGER, trigger);
    },
    [CONSTRAINT_LIST]({ commit }, { constraint_list }) {
        commit(SET_CONSTRAINT_LIST, constraint_list);
    },
    [LOGOUT]({ commit }) {
        commit(SET_AUTHENTICATION, null);
        commit(SET_USER, null);
        commit(SET_LOGINDATE, null);
        commit(SET_FORMATION, null);
        commit(SET_COURSE, null);
        commit(SET_TRIGGER, null);
        commit(SET_CONSTRAINT_LIST, []);
    },
    [UPDATE_USER]({ commit }, { user }) {
        commit(SET_USER, user);
    },
};
export default actions;
