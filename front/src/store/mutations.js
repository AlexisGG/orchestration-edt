import {
    SET_AUTHENTICATION,
    SET_USER,
    SET_LOGINDATE,
    SET_CONSTRAINT_LIST,
    SET_COURSE,
    SET_FORMATION,
    SET_TRIGGER,
} from "./mutations.type";

const mutations = {
    [SET_AUTHENTICATION](state, authentication) {
        state.authentication = authentication;
    },
    [SET_USER](state, user) {
        state.user = user;
    },
    [SET_LOGINDATE](state, date) {
        state.loginDate = date;
    },
    [SET_COURSE](state, course) {
        state.course = course;
    },
    [SET_FORMATION](state, formation) {
        state.formation = formation;
    },
    [SET_TRIGGER](state, trigger) {
        state.trigger = trigger;
    },
    [SET_CONSTRAINT_LIST](state, constraint_list) {
        state.constraint_list = constraint_list;
    },
};
export default mutations;
