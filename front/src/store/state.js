export default () => ({
    authentication: null,
    user: null,
    loginDate: null,
    formation: null,
    course: null,
    trigger: null,
    constraint_list: null,
});
