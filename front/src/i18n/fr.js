export default {
    APP_NAME: "USP Orchestration",

    // HEADER
    HEADER_SIGNIN: "Inscription",
    HEADER_LOGIN: "Connexion",
    HEADER_LOGOUT: "Se déconnecter",
    HEADER_PROFILE: "Mon profil",
    HEADER_ACCESSRIGHTS: "Droits d'édition",
    HEADER_LOGGEDIN: "Connecté·e",

    // HOME
    HOME_WELCOME: "Bienvenue sur USP Orchestration",
    HOME_SELECT_COURSE:
        "Veuillez sélectionner la formation puis la matière pour laquelle vous désirez saisir un vœu.",
    HOME_FORMATION: "Formation",
    HOME_COURSE: "Matière",
    HOME_ENTER_WISH_LABEL:
        "Recherchez un vœu en saisissant ci-dessous une phrase ou des mots-clés.",
    HOME_ENTER_WISH_PLACEHOLDER:
        "Exemples : Je voudrais un cours par semaine, Séquencement, Agenda, ...",
    HOME_SEARCH_RESULT:
        "Cliquez sur une case ci-dessous pour sélectionner le vœu que vous désirez ajouter.",
    HOME_NEED_HELP:
        "Si vous avez besoin d'aide, veuillez contacter votre administrateur système. (sysadmin@univ-angers.fr)",
    HOME_NO_EDIT_RIGHT: "Vous n'avez aucun droit d'édition.",
    HOME_TIME_DIVISION:
        "Vous pouvez modifier le découpage horaire de cette matière si vous le souhaitez.",

    // EDITION
    EDITION_RESET_TRIGGER: "Revenir à la recherche",
    EDITION_TITLE_ADD:
        "Éditez puis ajoutez ce nouveau vœu à la matière sélectionnée.",
    EDITION_TITLE_EDIT:
        "Éditez puis valider la modification de ce vœu pour la matière sélectionnée.",
    EDITION_WISH_TYPE: "Type de vœu : ",
    TRIGGER_RESET_BTN: "Réinitialiser",
    TRIGGER_ADD_BTN: "Ajouter ce vœu",
    TRIGGER_VALID_BTN: "Valider la modification",
    TRIGGER_DELETE_BTN: "Supprimer ce vœu",
    TRIGGER_SUCCESSFUL_CREATED: "Le vœu a bien été ajouté !",
    TRIGGER_ERROR: "Une erreur est survenue lors de l'ajout du vœu.",
    TRIGGER_DELETE_ERROR:
        "Une erreur est survenue lors de la suppression du vœu.",
    TRIGGER_DELETE_CARD: "Souhaitez-vous vraiment supprimer ce vœu ?",
    TRIGGER_CONFIRM_DELETE: "Oui, supprimer ce voeu",
    TRIGGER_CANCEL_DELETE: "Non, revenir à l'édition",

    // SIDE PANEL
    SP_EXISTING_CONSTRAINTS: "Vœux existants",
    SP_NO_COURSE_SELECTED: "Aucune matière sélectionnée",
    SP_WISH_VISUALISATION: "Visualisation des vœux",
    SP_NO_CONSTRAINTS: "Aucun vœu n'a été ajouté",
    SP_TIMEDIVISION: "Modifier le découpage horaire",

    // PAGES
    BTN_REDIRECT_HOME: "Aller à l'accueil",

    // REGISTER
    REGISTER_TITLE: "Inscription",
    REGISTER_ID: "Identifiant",
    REGISTER_PASSWORD: "Mot de passe",
    REGISTER_CONFIRM_PASSWORD: "Confirmation du mot de passe",
    REGISTER_CREATE_ACCOUNT: "Créer un compte",
    REGISTER_LOGIN_REDIRECTION_BUTTON: "J'ai déjà un compte",
    REGISTER_SUCCESS:
        "Votre compte a bien été créé !<br />Vous allez être redirigé vers la page de connexion.",
    REGISTER_USERNAME_ALREADY_TAKEN: "Cet identifiant n'est pas disponible.",
    REGISTER_ERROR_OCCURRED: "Une erreur est survenue.",
    REGISTER_RULE_ID_REQUIRED: "L'identifiant est requis.",
    REGISTER_RULE_ID_LENGTH:
        "L'identifiant doit faire entre 2 et 15 caractères.",
    REGISTER_RULE_PASSWORD: "Le mot de passe est requis.",
    REGISTER_RULE_CONFIRM_PASSWORD: "Les mots de passe ne correspondent pas.",

    // LOGIN
    LOGIN_TITLE: "Connexion",
    LOGIN_ID: "Identifiant",
    LOGIN_PASSWORD: "Mot de passe",
    LOGIN_LOGIN_BUTTON: "Se connecter",
    LOGIN_REGISTER_REDIRECTION_BUTTON: "Je n'ai pas encore de compte",
    LOGIN_CONNECTION_FAILED: "Identifiant ou mot de passe incorrect.",
    LOGIN_ERROR_OCCURRED: "Une erreur est survenue.",
    LOGIN_RULE_ID_REQUIRED: "L'identifiant est requis.",
    LOGIN_RULE_PASSWORD_REQUIRED: "Le mot de passe est requis.",

    // PROFILE
    PROFILE_SAVE_BUTTON: "Sauvegarder",
    PROFILE_RULE_ID_REQUIRED: "L'identifiant est requis.",
    PROFILE_RULE_ID_LENGTH:
        "L'identifiant doit faire entre 2 et 15 caractères.",
    PROFILE_PASSWORD: "Mot de passe",
    PROFILE_CONFIRM_PASSWORD: "Confirmer le mot de passe",
    PROFILE_RULE_PASSWORD: "Le mot de passe est requis",
    PROFILE_RULE_CONFIRM_PASSWORD: "Les mots de passe ne correspondent pas.",
    PROFILE_UPDATE_SUCCESS: "Votre compte a bien été mis à jour.",
    PROFILE_USERNAME_TAKEN: "Identifiant déjà pris",
    PROFILE_ERROR_OCCURED: "Une erreur est survenue",
    PROFILE_KEYWORD: "Profil",
    PROFILE_EDIT_INFOS: "Modifier mes informations",
    PROFILE_MY_RIGHTS: "Mes droits d'édition / de saisie de voeux",
    PROFILE_FORMATION: "Formation",
    PROFILE_COURSE: "Matière",

    // LOGOUT
    LOGOUT_BAD_REQUEST: "La requête a échoué.",
    LOGOUT_INTERNAL_SERVER_ERROR: "Une erreur serveur est survenue.",

    // ADMIN
    ADMIN_TITLE: "Gestion des droits d'édition",
    ADMIN_ADD_TITLE: "Création de droits d'édition",
    ADMIN_ADD_ADD_BUTTON: "Ajouter",
    ADMIN_ADD_USER: "Utilisateur",
    ADMIN_ADD_FORMATION: "Formation",
    ADMIN_ADD_COURSE: "Matière",
    ADMIN_ADD_USER_REQUIRED: "L'utilisateur est requis.",
    ADMIN_ADD_FORMATION_REQUIRED: "La formation est requise.",
    ADMIN_ADD_COURSE_REQUIRED: "La matière est requise.",
    ADMIN_ADD_SUCCESS: "Un nouveau droit d'édition a été ajouté.",
    ADMIN_ADD_UNAUTHORIZED:
        "Vous n'êtes pas autorisé à effectuer cette action.",
    ADMIN_ADD_DB_CONFLICT: "Ce droit d'édition existe déjà.",
    ADMIN_ADD_ERROR_OCCURED: "Une erreur est survenue.",

    ADMIN_REMOVE_TITLE: "Suppression de droits d'édition",
    ADMIN_REMOVE_USER: "Utilisateur",
    ADMIN_REMOVE_FORMATION: "Formation",
    ADMIN_REMOVE_COURSE: "Matière",
    ADMIN_REMOVE_USER_REQUIRED: "L'utilisateur est requis.",
    ADMIN_REMOVE_FORMATION_REQUIRED: "La formation est requise.",
    ADMIN_REMOVE_COURSE_REQUIRED: "La matière est requise.",
    ADMIN_REMOVE_SUCCESS: "La suppression a été effectuée.",
    ADMIN_REMOVE_NOTCONNECTED: "Vous n'êtes pas connecté.",
    ADMIN_REMOVE_UNAUTHORIZED:
        "Vous n'êtes pas autorisé à effectuer cette action.",
    ADMIN_REMOVE_BADREQUEST: "La requête a échoué.",
    ADMIN_REMOVE_NOTLINKED:
        "Cet utilisateur n'a pas de droit d'édition sur cette matière.",
    ADMIN_REMOVE_INTERNALSERVERERROR: "Une erreur serveur est survenue.",
    ADMIN_REMOVE_ERROR_OCCURED: "Une erreur est survenue.",

    // TIME DIVISION
    TIME_DIVISION_TITLE: "Découpage horaire",
    TIME_DIVISION_SELECTED_COURSE: "Matière sélectionnée : ",
    TIME_DIVISION_MANAGEMENT_TITLE: "Volumes horaires",
    TIME_DIVISION_SELECT_VOLUME: "Sélectionner un volume horaire",
    TIME_DIVISION_BUTTON_VALIDATE: "Valider ces volumes horaires",
    TIME_DIVISION_BUTTON_REINIT: "Réinitialiser",
    TIME_DIVISION_DIALOG_TITLE: "Attention",
    TIME_DIVISION_DIALOG_MSG1: `Vous êtes sur le point de valider ces volumes horaires.
        Les volumes horaires non selectionnés conserveront leur découpage initial et ne seront pas impactés.`,
    TIME_DIVISION_DIALOG_MSG2:
        "Veillez à vérifier les modifications apportées aux voeux liés au découpage horaire de cette matière.",
    TIME_DIVISION_DIALOG_BTN_REINIT: "Revenir à la saisie",
    TIME_DIVISION_DIALOG_BTN_VALIDATE: "Valider et continuer",
    TIME_DIVISION_TEXT1: "session(s) de",
    TIME_DIVISION_TEXT2: "minutes (soit",
    TIME_DIVISION_TEXT3: "créneau(x))",
    TIME_DVISION_ATLEASTONE: "Veuillez selectionner au moins un découpage.",

    TIME_DIVISION_SUCCESS: "Le découpage horaire a été mis à jour.",
    TIME_DIVISION_NOT_CONNECTED: "Vous devez être connecté.",
    TIME_DIVISION_CANT_ACCESS: "Vous n'avez pas le droit d'édition nécessaire.",
    TIME_DIVISION_BAD_REQUEST: "Une erreur est survenue.",
    TIME_DIVISION_INTERNAL_SERVOR_ERROR: "Une erreur serveur est survenue.",

    GROUP_DISTRIBUTION_TITLE_REPARTITION: "Répartition des groupes",
    GROUP_DISTRIBUTION_TYPE: "Type",
    GROUP_DISTRIBUTION_GROUPS: "Classes",
};
