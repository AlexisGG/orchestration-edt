export default {
    APP_NAME: "USP Orchestration",

    // HEADER
    HEADER_SIGNIN: "Sign in",
    HEADER_LOGIN: "Log in",
    HEADER_LOGOUT: "Log out",
    HEADER_PROFILE: "My profile",
    HEADER_ACCESSRIGHTS: "Edition Rights",
    HEADER_LOGGEDIN: "Logged in",

    // HOME
    HOME_WELCOME: "Welcome in USP Orchestration",
    HOME_SELECT_COURSE:
        "Please select a formation and a course for which you would like to make a wish",
    HOME_FORMATION: "Formation",
    HOME_COURSE: "Course",
    HOME_ENTER_WISH_LABEL:
        "Search for a wish by entering a phrase or keywords below.",
    HOME_ENTER_WISH_PLACEHOLDER:
        "E.G : I'd like a course per week, the lesson is before the practical work...",
    HOME_SEARCH_RESULT:
        "Click on a box below to select the wish you want to add.",
    HOME_NEED_HELP:
        "If you need help, please contact the system admin. (sysadmin@univ-angers.fr)",
    HOME_NO_EDIT_RIGHT: "You have no editing rights.",
    HOME_TIME_DIVISION:
        "You can modify the time division of this course if you wish.",

    // EDITION
    EDITION_RESET_TRIGGER: "Return to search",
    EDITION_TITLE_ADD: "Edit and add this new wish to the selected course.",
    EDITION_TITLE_EDIT:
        "Edit and validate the modification of this wish for the selected course.",
    EDITION_WISH_TYPE: "Wish type : ",
    TRIGGER_RESET_BTN: "Reset",
    TRIGGER_ADD_BTN: "Add this wish",
    TRIGGER_VALID_BTN: "Confirm the modification",
    TRIGGER_DELETE_BTN: "Delete this wish",
    TRIGGER_SUCCESSFUL_CREATED: "The wish has been succesfully added !",
    TRIGGER_ERROR: "An error occured while trying to save the wish.",
    TRIGGER_DELETE_ERROR: "An error occured while trying to delete the wish.",
    TRIGGER_DELETE_CARD: "Do you really want to delete this wish ?",
    TRIGGER_CONFIRM_DELETE: "Yes, please delete this wish",
    TRIGGER_CANCEL_DELETE: "No, go back to editing",

    // SIDE PANEL
    SP_EXISTING_CONSTRAINTS: "Existing wishes",
    SP_NO_COURSE_SELECTED: "No course selected",
    SP_WISH_VISUALISATION: "Wishes visualization",
    SP_NO_CONSTRAINTS: "No wishes have been added",
    SP_TIMEDIVISION: "Edit the time division",

    // PAGES
    BTN_REDIRECT_HOME: "Go to homepage",

    // REGISTER
    REGISTER_TITLE: "Register",
    REGISTER_ID: "Username",
    REGISTER_PASSWORD: "Password",
    REGISTER_CONFIRM_PASSWORD: "Confirm your password",
    REGISTER_CREATE_ACCOUNT: "Create account",
    REGISTER_LOGIN_REDIRECTION_BUTTON: "I already have an account",
    REGISTER_SUCCESS:
        "Your account has been successfully created !<br />You'll be redirected to the log in page.",
    REGISTER_USERNAME_ALREADY_TAKEN: "Username already taken.",
    REGISTER_ERROR_OCCURRED: "An error occurred.",
    REGISTER_RULE_ID_REQUIRED: "The username is required.",
    REGISTER_RULE_ID_LENGTH:
        "The username must contain between 2 and 15 characters.",
    REGISTER_RULE_PASSWORD: "A password is required.",
    REGISTER_RULE_CONFIRM_PASSWORD: "The passwords don't match.",

    // LOGIN
    LOGIN_TITLE: "Log in",
    LOGIN_ID: "Username",
    LOGIN_PASSWORD: "Password",
    LOGIN_LOGIN_BUTTON: "Login",
    LOGIN_REGISTER_REDIRECTION_BUTTON: "I don't have an account",
    LOGIN_CONNECTION_FAILED: "Wrong username or password.",
    LOGIN_ERROR_OCCURRED: "An error occurred.",
    LOGIN_RULE_ID_REQUIRED: "Username is required.",
    LOGIN_RULE_PASSWORD_REQUIRED: "Password is required.",

    // PROFILE
    PROFILE_SAVE_BUTTON: "Save",
    PROFILE_RULE_ID_REQUIRED: "The username is required.",
    PROFILE_RULE_ID_LENGTH:
        "The username must contain between 2 and 15 characters.",
    PROFILE_PASSWORD: "Password",
    PROFILE_CONFIRM_PASSWORD: "Confirm password",
    PROFILE_RULE_PASSWORD: "A password is required.",
    PROFILE_RULE_CONFIRM_PASSWORD: "The passwords don't match.",
    PROFILE_UPDATE_SUCCESS: "Your account has been updated",
    PROFILE_USERNAME_TAKEN: "Username already taken",
    PROFILE_ERROR_OCCURED: "An error occured",
    PROFILE_KEYWORD: "Profile",
    PROFILE_EDIT_INFOS: "Edit my informations",
    PROFILE_MY_RIGHTS: "My edition rights",
    PROFILE_FORMATION: "Formation",
    PROFILE_COURSE: "Course",

    // LOGOUT
    LOGOUT_BAD_REQUEST: "Bad request.",
    LOGOUT_INTERNAL_SERVER_ERROR: "Internal server error.",

    // ADMIN
    ADMIN_TITLE: "Edition rights management",
    ADMIN_ADD_TITLE: "Creation of edition rights",
    ADMIN_ADD_ADD_BUTTON: "Add",
    ADMIN_ADD_USER: "User",
    ADMIN_ADD_FORMATION: "Formation",
    ADMIN_ADD_COURSE: "Course",
    ADMIN_ADD_USER_REQUIRED: "User is required.",
    ADMIN_ADD_FORMATION_REQUIRED: "Formation is required.",
    ADMIN_ADD_COURSE_REQUIRED: "Course is required.",
    ADMIN_ADD_SUCCESS: "A new edition right has been created.",
    ADMIN_ADD_UNAUTHORIZED: "You are not authorized.",
    ADMIN_ADD_DB_CONFLICT: "This edition right already exists.",
    ADMIN_ADD_ERROR_OCCURED: "An error occured.",

    ADMIN_REMOVE_TITLE: "Deletion of edition rights",
    ADMIN_REMOVE_USER: "User",
    ADMIN_REMOVE_FORMATION: "Formation",
    ADMIN_REMOVE_COURSE: "Course",
    ADMIN_REMOVE_USER_REQUIRED: "User is required.",
    ADMIN_REMOVE_FORMATION_REQUIRED: "Formation is required.",
    ADMIN_REMOVE_COURSE_REQUIRED: "Course is required.",
    ADMIN_REMOVE_SUCCESS: "Deletion executed successfully.",
    ADMIN_REMOVE_NOTCONNECTED: "You are not connected.",
    ADMIN_REMOVE_UNAUTHORIZED: "You are not authorized.",
    ADMIN_REMOVE_BADREQUEST: "The request failed.",
    ADMIN_REMOVE_NOTLINKED: "This user has no editing right on this course.",
    ADMIN_REMOVE_INTERNALSERVERERROR: "Internal server error.",
    ADMIN_REMOVE_ERROR_OCCURED: "An error occured.",

    // TIME DIVISION
    TIME_DIVISION_TITLE: "Time division",
    TIME_DIVISION_SELECTED_COURSE: "Selected course : ",
    TIME_DIVISION_MANAGEMENT_TITLE: "Hourly volume",
    TIME_DIVISION_SELECT_VOLUME: "Select a hourly volume",

    TIME_DIVISION_BUTTON_VALIDATE: "Validate this hourly volumes",
    TIME_DIVISION_BUTTON_REINIT: "Reset",
    TIME_DIVISION_DIALOG_TITLE: "Warning",
    TIME_DIVISION_DIALOG_MSG1:
        "You are about to validate these hourly volumes. The hourly volumes not selected will keep their initial breakdown and will not be impacted.",
    TIME_DIVISION_DIALOG_MSG2:
        "Please check the modifications associated with the wishes linked to the time division of this course.",
    TIME_DIVISION_DIALOG_BTN_REINIT: "Revenir à la saisie",
    TIME_DIVISION_DIALOG_BTN_VALIDATE: "Validate and continue",
    TIME_DIVISION_TEXT1: "session(s) of",
    TIME_DIVISION_TEXT2: "minutes (i.e.",
    TIME_DIVISION_TEXT3: "slot(s))",
    TIME_DVISION_ATLEASTONE: "Please select at least one division",

    TIME_DIVISION_SUCCESS: "Hourly volume has been updated.",
    TIME_DIVISION_NOT_CONNECTED: "You must be logged in.",
    TIME_DIVISION_CANT_ACCESS: "You don't have the edition right.",
    TIME_DIVISION_BAD_REQUEST: "An error occured.",
    TIME_DIVISION_INTERNAL_SERVOR_ERROR: "Internal server error.",

    GROUP_DISTRIBUTION_TITLE_REPARTITION: "Group distribution",
    GROUP_DISTRIBUTION_TYPE: "Type",
    GROUP_DISTRIBUTION_GROUPS: "Groups",
};
