.PHONY: run stop clean front back update-back test-back front update-front

.env:
	@echo "Il manque le fichier .env, vous pouvez faire un copier coller depuis le fichier .env.example. Il est fortement conseillée de modifer les valeurs par défaut du .env"
	@exit 1

run: .env
	docker-compose up -d

stop: .env
	docker-compose down

clean: .env
	docker-compose down -v
	
back: .env
	docker-compose logs -f --tail 20 back

update-back: .env
	docker-compose exec back npm i

test-back: .env
	docker-compose exec back npm test

front: .env
	docker-compose logs -f --tail 20 front

update-front: .env
	docker-compose exec front npm i